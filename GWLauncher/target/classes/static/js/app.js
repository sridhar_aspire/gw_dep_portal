var app = angular.module('dep_portal', [ 'ngRoute', 'ui.bootstrap', 'dpdaterangepicker', 'angularUtils.directives.dirPagination']);
app.config(function($routeProvider, $httpProvider, paginationTemplateProvider) {
	$routeProvider.when('/login', {
		templateUrl : 'login.html',
		controller : 'loginController'
	}).when('/home', {
		templateUrl : 'home.html',
		controller : 'homeController'
	}).when('/stack', {
		templateUrl : 'stackcreate.html',
		controller : 'stackController'
	}).when('/stackmodify', {
		templateUrl : 'stackmodify.html',
		controller : 'stackModifyController'
	}).when('/stackmodify/:stackId', {
		templateUrl : 'stackmodify.html',
		controller : 'stackModifyController'
	}).when('/setting', {
		templateUrl : 'setting.html',
		controller : 'settingController'
	}).when('/logout', {
		templateUrl : 'login.html',
		controller : 'loginController'
 	}).otherwise('/login');
	$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
	paginationTemplateProvider.setPath('../dirPagination.tpl.html');
});
app.service('api-services', [ '$http', "$location", "$window", function($http, $location, $window) {
	return {
		setItem: function(key, value){
			$window.localStorage.setItem(key, angular.toJson(value));
		},
		getItem: function(key){
			return JSON.parse($window.localStorage.getItem(key));
		},
		getAuthUrl: function() {
			return $http.get("/open/login_url");
		},
		getUserDetails: function() {
//			var headers = credentials ? {
//				authorization : "Basic "
//						+ btoa(credentials.username + ":"
//								+ credentials.password)
//			} : {};
			return $http.get('user'
//					, {headers : headers}
					);
		},
		getSettings : function() {
			return $http.get("/setting/");
		},
		saveSettings : function(data) {
			return $http.put("/setting/", angular.toJson(data));
		},
		getUserList : function(pageNo) {
			return $http.get("/users?pageNo=" + pageNo);
		},
		getAvailableUserList : function(pageNo) {
			return $http.get("availableUsers?pageNo=" +pageNo);
		},
		getAddedUserList : function(pageNo, editId) {
			return $http.get("addedUsers?pageNo=" +pageNo+"&groupId="+editId);
		},
		getAllUsers : function() {
			return $http.get("/user_list");
		},
		getGroupList : function(pageNo) {
			return $http.get("/groups?pageNo=" + pageNo);
		},	
		getSharedGroupList : function(stackId, isShared) {
			return $http.get("/sharedGroup?stackId=" + stackId + "&isShared=" + isShared);
		},
		shareStackToGroup : function(grpId, stackId) {
			return $http.post("/sharedGroup?grpId=" + grpId + "&stackId=" + stackId);
		},
		removeStackFromGroup : function(grpId, stackId) {
			return $http.delete("/sharedGroup?grpId=" + grpId + "&stackId=" + stackId);
		},
		getGroup : function(groupId) {
			return $http.get("/groups/" + groupId);
		},
		getUser : function(userId) {
			return $http.get("/users/" + userId);
		},
//		saveUser : function(user) {
//			return $http.post("/users", angular.toJson(user));
//		},
//		updateUser : function(user, userId) {
//			return $http.put("/users/" + userId , angular.toJson(user));
//		},
		saveGroup : function(group) {
			return $http.post("/groups", angular.toJson(group));
		},
		updateGroupDetails : function(group) {
			return $http.post("/updateGroup" , angular.toJson(group));
		},
		getAddedUsers : function(groupId) {
			return $http.get("/added_user?groupId=" + groupId);
		},
		getTopUsersCostByGroup : function() {
			return $http.get("/user_cost_by_group/");
		},
		searchedUsers : function(serachtext) {
			return $http.get("/searchedUsers/" + serachtext);
		},
		getGroupDetails : function(groupId) {
			return $http.get("/groups/" + groupId);
		},
		deleteGroup : function(groupId) {
			return $http.delete("/groups/" + groupId);
		},
		/*addUserToGroup : function(groupName, userId) {
			return $http.post("/added_group?groupName=" + groupName + "&userId=" + userId);
		},*/
		getGroupByName : function(groupName) {
			return $http.get("/group_by_name/" + groupName);
		},
		addUserToGroup : function(group, userId) {
			return $http.post("/groups/" + userId , angular.toJson(group));
		},
		updateGroup : function(group, userId, isAdded) {
			return $http.post("/update_groups?userId=" + userId + "&isAdded=" + isAdded, angular.toJson(group));
		},
		saveGroup : function(group) {
			return $http.post("/saveGroup", angular.toJson(group))
		},
		EditGroup : function(group) {
			return $http.post("/EditGroup", angular.toJson(group))
		},
		assignStackToUser : function(stackId, userId) {
			return $http.get("/assign_stack_user?stackId=" + stackId + "&userId=" + userId);
		},
		shareStackToUser : function(stackId, userId) {
			return $http.post("/shared_user?stackId=" + stackId + "&userId=" + userId);
		},
		getSharedUsers : function(stackId) {
			return $http.get("/shared_user?stackId=" + stackId);
		},
		deleteSharedUser : function(stackId, userId) {
			return $http.delete("/shared_user?stackId=" + stackId + "&userId=" + userId);
		},
//		updatePwd : function(userId) {
//			return $http.put("/passwd/" + userId);
//		},
//		deleteUser : function(userId) {
//			return $http.delete("/users/" + userId);
//		},
		getRolesList : function() {
			return $http.get("/roles");
		},
		getApplicationMetadataList : function() {
			return $http.get("/application_meta_data");
		},
		getApplicationMetadata : function(appId) {
			return $http.get("/application_meta_data/" + appId);
		},
		getInstanceCostdata : function() {
			return $http.get("/instance_cost_data");
		},
		getStackList : function(pageNo) {
			return $http.get("/stacks?pageNo=" + pageNo);
		},
		getTerminatedStackList : function(pageNo) {
			return $http.get("/stacks?pageNo=" + pageNo + "&status=terminated");
		},
		getSharedStackList : function(pageNo) {
			return $http.get("/stacks?pageNo=" + pageNo + "&status=shared");
		},
		getStackDetails : function(stackId) {
			return $http.get("/stacks/" + stackId);
		},
		updateStackInstanceStatus : function(stackId) {
			return $http.get("/stack_status/" + stackId);
		},
		deleteStack : function(stackId) {
			return $http.delete("/stacks/" + stackId);
		},
		createStack : function(details) {
			return $http.post("/stacks", angular.toJson(details));
		},
		updateStack : function(stack) {
			return $http.put("/updateStacks", angular.toJson(stack));
		},
		sendEmail : function(mailOptions) {
			return $http.post('/notification', mailOptions);
		},
		terminateApplication: function(applicationId){
			return $http.delete("/applications/" + applicationId);
		},
		updateApplication : function(applicationId) {
			return $http.get("/applications/" + applicationId);
	    },
	    updateBuild : function(application) {
	    	return $http.post("/updateBuild", application);
	    },
	    stopApplication: function(applicationId){
			return $http.get("/applications/" + applicationId);
		},
		launchApp : function(stackId, application) {
			return $http.post("/applications?stack_id=" + stackId, application);
		},
		getAllResources : function(stackId, appId) {
			var url = "/resources/";
			if (stackId) {
				url += "?stack=" + stackId;
				if (appId) {
					url += "&app=" + appId;
				}
			}
			return $http.get(url);
		},
		getAllResourcesForStack : function(stackId) {
			return $http.get("/resources/" + stackId);
		},
		getRunningResourceCountByApp : function() {
			return $http.get("/running_resource_count_by_app/");
		},
		getTerminatedResourceCountByApp : function() {
			return $http.get("/terminated_resource_count_by_app/");
		},
		getResourceCountTrend : function(beginDate, endDate) {
			var url = "/resource_count_trend/";
			if (beginDate && endDate) {
				url += "?start_date="+beginDate+"&end_date="+endDate;
			}
			return $http.get(url);
			
		},
		getStackCountTrend : function(beginDate, endDate) {
			var url = "/stack_count_trend/";
			if (beginDate && endDate) {
				url += "?start_date="+beginDate+"&end_date="+endDate;
			}
			return $http.get(url);
		},
		getResourceSizeTrend : function(beginDate, endDate) {
			var url = "/resource_size_trend/";
			if (beginDate && endDate) {
				url += "?start_date="+beginDate+"&end_date="+endDate;
			}
			return $http.get(url);
		},
		getStackCountByUser : function() {
			return $http.get("/stack_count_by_user/");
		},
		getTopInstanceCostByUser : function() {
			return $http.get("/instance_cost_by_user/");
		},
		getInstanceCPUMonitoringData: function(instanceId) {
			return $http.get("/monitoring/instance_stats/cpu/" + instanceId);
		},
		getInstanceNetworkinMonitoringData: function(instanceId) {
			return $http.get("/monitoring/instance_stats/networkin/" + instanceId);
		},
		getTopInstanceCostByStack : function() {
			return $http.get("/instance_cost_by_stack/");
		},
		stopInstance: function(stackId, appId, instanceId) {
			var url = "/instance/" + instanceId + "/stop";
			if (stackId) {
				url += "?stack=" + stackId;
				if (appId) {
					url += "&app=" + appId;
				}
			}
			return $http.put(url);
		},
		startInstance: function(stackId, appId, instanceId) {
			var url = "/instance/" + instanceId + "/start";
			if (stackId) {
				url += "?stack=" + stackId;
				if (appId) {
					url += "&app=" + appId;
				}
			}
			return $http.put(url);
		},
		getInstanceSize: function() {
		    return $http.get("/getInstanceCostdata");
		},
		getUsersList: function() {
			return $http.get("/getUsersList");
		}
	}
}]);

app.service('ui-services', [ "$location","$rootScope", "api-services", function($location, $rootScope, apiservices) {
	return {
		authenticate : function(credentials, callback, path) {
			apiservices.getUserDetails(credentials).success(function(data) {
				if (data.name) {
					$rootScope.authenticated = true;
					$location.path(path);
				} else {
					$rootScope.authenticated = false;
				}
				callback && callback();
			}).error(function() {
				$rootScope.authenticated = false;
				$location.path("/login");
			})
		},
		
		populateStackListInfo : function(pageNo) {
			var self = this;
			apiservices.getStackList(pageNo).success(function(data) {
				$rootScope.totLiveStackCount = Object.keys(data)[0];
				$rootScope.stackList = Object.values(data)[0] != null ? Object.values(data)[0] : [];
				$rootScope.filteredStackList = self.getFormttedStackDate($rootScope.stackList);
				apiservices.setItem("stackList", $rootScope.stackList);
			}).error(function(err) {
				$rootScope.stackList=[];
				console.log(err.message);
			});
			apiservices.getTerminatedStackList(pageNo).success(function(data) {
				if (data) {
					$rootScope.totTerminatedStackCount = Object.keys(data)[0];
					$rootScope.terminatedStackList = Object.values(data)[0] != null ? Object.values(data)[0] : [];
					$rootScope.filteredTerminatedStackList = self.getFormttedStackDate($rootScope.terminatedStackList);
					apiservices.setItem("terminated_stackList", $rootScope.terminatedStackList);
				} else {
					$rootScope.terminatedStackList = [];
				}
			}).error(function(err) {
				$rootScope.terminatedStackList = [];
				console.log(err.message);
			});;
			
			apiservices.getSharedStackList(pageNo).success(function(data) {
				$rootScope.totSharedStackCount = Object.keys(data)[0];
				$rootScope.sharedStackList = Object.values(data)[0] != null ? Object.values(data)[0] : [];
				$rootScope.filteredSharedStackList = self.getFormttedStackDate($rootScope.sharedStackList);
				apiservices.setItem("shared_stackList", $rootScope.sharedStackList);
			}).error(function(err) {
				$rootScope.sharedStackList = [];
				console.log(err.message);
			});
		},
		getFormttedStackDate : function(stackList){
			for (var stackCount = 0; stackCount < stackList.length; stackCount++) {
				stackList[stackCount].createdDate = moment(stackList[stackCount].createdDate);
				stackList[stackCount].createdDate_day = moment(stackList[stackCount].createdDate).date();
				stackList[stackCount].createdDate_month = this.getMonthName(stackList[stackCount].createdDate.month()); 
			}
			return stackList;
		},
		getMonthName : function(monthNumber) {
			var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
			                  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			return monthNames[monthNumber];
		}
	}
}]);

app.filter('terminatedApp', function(){
	return function(terminatedAppList){
		return terminatedAppList.filter(function(app){return app.status == "TERMINATED";});
	}
});

app.filter('runningApp', function(){
	return function(runningAppAppList){
		return runningAppAppList.filter(function(app){return app.status != "TERMINATED";});
	}
});
