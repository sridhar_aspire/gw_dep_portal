/**
 * STACK DELETE CONTROLLER
 */

app.controller('stackDeleteController', ['scope', function($scope){
	$scope.modalTitle = "Terminate Stack";
	$scope.cancel = function () {
		$scope.terminateStackModalInstance.dismiss('cancel');
	};
		
	$scope.ok = function () {
		$scope.terminateStackModalInstance.close($scope.currentStack.id);
	};
}]);