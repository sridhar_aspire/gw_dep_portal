/**
 * 
 */
var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('createNewGroupCtrl', [ '$rootScope', 'scope', '$scope','api-services','ui-services', '$uibModalInstance', 'editId', '$timeout', function($rootScope, scope,$scope, apiservices, uiservices, $uibModalInstance, editId, $timeout) {
	$scope.isAddedSuccess = false;
	$scope.isAddedFail = false;
	/*$scope.loadAddedUsers = function() {
		apiservices.getAddedUsers($scope.group.name).success(function(added_user_list){
			$scope.added_user_list = added_user_list;
			apiservices.getAllUsers().success(function(userList){
				$scope.user_list = [];
				if ($scope.added_user_list == null || $scope.added_user_list.length == 0) {
					$scope.user_list = userList;
				} else {
					for (var userCount =0; userCount < userList.length; userCount++) {
						var userAdded = false;
						for (var addedUserCount=0; addedUserCount<$scope.added_user_list.length;addedUserCount++) {
							if (userList[userCount].id == $scope.added_user_list[addedUserCount].id) {
								userAdded = true;
							}
						}
						if (!userAdded) {
							$scope.user_list.push(userList[userCount]);
						}
					}
				}
			});
		});
	}*/
	if (!editId) {
		$scope.user = {};
		$scope.title = "ADD GROUP";
		apiservices.getAllUsers().success(function(userList){
			$scope.user_list = userList;
		});
		
	} else {
		$scope.title = "EDIT GROUP";
		apiservices.getGroup(editId).success(function(data) {
			$scope.group = data;
		});
	}
	
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	
	$scope.group= {
			name : "",
			users : [],
		};
	
	$scope.addUserToGroup = function(userId) {
		if ($scope.group.name == null || $scope.group.name == "" ) {
			$scope.message = "Group Name field can't be empty.";
			$scope.isAddedFail = true;
		} 
		else if ($scope.group.admin == null || $scope.group.admin == "" ) {
			$scope.message = "Group Admin Name field can't be empty.";
			$scope.isAddedFail = true;
		} else {
			apiservices.getGroupByName($scope.group.name).success(function(data){
			$scope.dbgroup=data;
			}).error( function(err) {
		   	$scope.message = "Please enter the Group Name and Group Admin Name";
			$scope.isAddedFail = true;
		    });
		if (!editId && !$scope.dbgroup && $scope.dbgroup==null) {
			apiservices.addUserToGroup($scope.group, userId).success(function(data) {
			$scope.group = data;
			$scope.isAddedSuccess = true;
			editId=$scope.group.id;
			scope.getLiveGroupsData(1);
			$scope.getAvailableUsersData(1);
			$scope.getAddedUsersData(1);
			$scope.message = "The user is successfully added to " + $scope.group.name + "";
	    }).error( function(err) {
	    	$scope.isAddedFail = true;
	    	$scope.message = "Group Name Already Exist";
	    });
		} else {
			apiservices.updateGroup($scope.group, userId, 1).success(function(data) {
			$scope.isAddedSuccess = true;
			$scope.message = "The user is successfully added to " + $scope.group.name + " And Group Details updated";
			$scope.getAvailableUsersData(1);
			$scope.getAddedUsersData(1);
			scope.getLiveGroupsData(1);
	    }).error( function(err) {
	    	$scope.isAddedFail = false;
	    	$scope.message = "Failed while addding";
	    });
		}
		}
	};
	
	$scope.removeAddedUser = function (userId) {
		apiservices.updateGroup($scope.group, userId, 0).success(function(data) {
			$scope.isAddedSuccess = true;
			editId=$scope.group.id;
			scope.getLiveGroupsData(1);
			$scope.getAvailableUsersData(1);
			$scope.getAddedUsersData(1);
			$scope.message = "User removed from"+ $scope.group.name +"";
	    }).error( function(err) {
	    	$scope.isAddedFail = true;
	    	$scope.message = "Failed while deleting User from "+ $scope.group.name +"";
	    });
	}
	
    $scope.search = function (email) {
        apiservices.searchedUsers(email).success(function(data){
        	$scope.filteredList = data;
    		
    		}).error( function(err) {
    	 
    	   });
    	}
	
	$scope.saveOrEditGroup = function(userId) {
		if ($scope.group.name == null || $scope.group.name == "" ) {
			$scope.message = "Group Name field can't be empty.";
			$scope.isAddedFail = true;
		} 
		else if ($scope.group.admin == null || $scope.group.admin == "" ) {
			$scope.message = "Group Admin Name field can't be empty.";
			$scope.isAddedFail = true;
		} else {
		apiservices.getGroupByName($scope.group.name).success(function(data){
			$scope.dbgroup=data;
			}).error( function(err) {
			$scope.isAddedFail = true;
		   	$scope.message = "Please enter the Group Name and Group Admin Name";
		    });
		if (!editId  && !$scope.dbgroup && $scope.dbgroup==null) {
			apiservices.saveGroup($scope.group).success(function(data) {
			$scope.group = data;
			editId=$scope.group.id;
			$scope.isAddedSuccess = true;
			scope.getLiveGroupsData(1);
			$scope.getAvailableUsersData(1);
			$scope.getAddedUsersData(1);
			$scope.message = "The user Group " + $scope.group.name + " created Successfully";
			$timeout(function(){
				$uibModalInstance.close(data);
			}, 2500)
			}).error( function(err) {
			$scope.isAddedFail = true;
	    	$scope.message = "Failed while Creating " + $scope.group.name + " , Please try with other name";
	    });
		} else {
			apiservices.updateGroupDetails($scope.group).success(function(data) {
			$scope.isAddedSuccess = true;
			scope.getLiveGroupsData(1);
			$scope.getAvailableUsersData(1);
			$scope.getAddedUsersData(1);
			$scope.message = "The user Group " + $scope.group.name + " updated successfully";
			$timeout(function(){
				$uibModalInstance.close(data);
			}, 2500)
	    }).error( function(err) {
	    	$scope.isAddedFail = true;
	    	$scope.message = "Failed while updating";
	    });
		}
		}
	};
	
	$scope.getAvailableUsersData = function(pageNo){
		var self = this;
		apiservices.getAvailableUserList(pageNo).success(function(data) {
			$rootScope.totAvailableCount = Object.keys(data)[0];
			$rootScope.availableUserList = Object.values(data)[0];
			$rootScope.available_user_list = $rootScope.availableUserList;
			
		}).error(function(err) {
			$rootScope.availableUserList=[];
			console.log(err.message);
		});
	}
	$scope.getAddedUsersData = function(pageNo){
		var self = this;
		if(editId==null){
			editId=0;
		}
		apiservices.getAddedUserList(pageNo, editId).success(function(data) {
			$rootScope.totAddedCount = Object.keys(data)[0];
			$rootScope.addedUserList = Object.values(data)[0];
			$rootScope.added_user_list = $rootScope.addedUserList;
			
		}).error(function(err) {
			$rootScope.addedUserList=[];
			console.log(err.message);
		});
	}
	$scope.getAvailableUsersData(1);
	$scope.getAddedUsersData(1);
}]);