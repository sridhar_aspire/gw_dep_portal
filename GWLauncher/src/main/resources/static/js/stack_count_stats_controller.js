/**
 * STACK COUNT STATISTICS CONTROLLER
 */
var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('stackCountStatisticsCtrl', [ '$rootScope', '$scope', 'api-services', '$uibModalInstance', function($rootScope, $scope, apiservices, $uibModalInstance) {
	
	// Configuration of the daterangepicker
	var dt = new Date();
	var fromDate = new Date();
	fromDate.setMonth(fromDate.getMonth() - 5);
    $scope.opt = {
        dateRangeSelectCb: onDateRangeSelect,
        showGrid: false,
        dateFormat: 'mm/dd/yyyy',
        endDateText:"To Date",
        beginDateText:"From Date",
        showGrid:false,
        initSelectedDateRange: {
            end: {
                year: dt.getFullYear(),
                month: dt.getMonth()+1,
                day: dt.getDate()
            },
            begin: {
                year: fromDate.getFullYear(),
                month: fromDate.getMonth()+1,
                day: fromDate.getDate()
            }
        }
    };
    
    function onDateRangeSelect(beginDate, endDate) {
    	if (moment(beginDate.formatted, 'MM/DD/YYYY').isValid() && moment(endDate.formatted, 'MM/DD/YYYY').isValid()) {
    		$scope.showGraph(beginDate.formatted, endDate.formatted);
    	}
    };
	
	$scope.showGraph = function(beginDate, endDate) {
		apiservices.getStackCountTrend(beginDate, endDate).success(function(trendData){
			$scope.resourceTrend = trendData;
			var labelsArray = Object.keys(trendData).reverse();
			var valuesArray = Object.values(trendData).reverse();
			$scope.fromMonth = labelsArray[0] + ' ' + moment().format("YYYY");
			$scope.toMonth = labelsArray[labelsArray.length - 1] + ' ' + moment().format("YYYY");
			var data = {
					labels: labelsArray,
					series: [valuesArray]
			};
			var options = {
					seriesBarDistance: 10,
					low : 0,
					high : Math.max.apply(null, valuesArray) < 10 ? 10 : Math.max.apply(null, valuesArray),
					axisX: {
						showGrid: false
					},
					axisY:{
						onlyInteger: true
					},
					height: "245px"
			};
			var responsiveOptions = [
			                         ['screen and (max-width: 640px)', {
			                        	 seriesBarDistance: 5,
			                        	 axisX: {
			                        		 labelInterpolationFnc: function (value) {
			                        			 return value[0];
			                        		 }
			                        	 }
			                         }]
			                         ];
			var chart = Chartist.Bar('#stackCountUsageStats', data, options, responsiveOptions);
			chart.on('draw', (ctx) => {
			      if (ctx.type === 'label' && ctx.axis.units.pos === 'x') {
			        const dX = ctx.width / 2 + (100 - ctx.width)
			        ctx.element.attr({ x: ctx.element.attr('x') - dX })
			      }
			})
		});
	};
	
	$uibModalInstance.rendered.then(function() {
		$scope.showGraph();
	});
	
	$scope.ok = function () {
		$uibModalInstance.close();
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}]);
