/**
 * COST REPORT MODAL CONTROLLER
 */

var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('costReportModalCtrl', [ '$rootScope', '$scope', 'api-services', '$uibModalInstance', 'selectedStack', function($rootScope, $scope, apiservices, $uibModalInstance, stack) {
	$scope.totalCost = 0;
	$scope.currentStack = stack;
	$scope.instanceCostData = apiservices.getItem("instance_cost_data");  
	$scope.currentStack.instance_count = 0;
	$scope.currentStack.applications.forEach(function(app){
		if(app.status != 'TERMINATED'){
			$scope.currentStack.instance_count += app.instCount;
		}
		app.instances.filter(function(res){
			res.cost = (1*0).toFixed(2);
			return res.resourceType == 'AWS::EC2::Instance' || res.resourceType == 'AWS::AutoScaling::AutoScalingGroup' || res.resourceType == 'AWS::ElasticLoadBalancingV2::LoadBalancer';
		}).forEach(function(res){
			var start_date = moment(res.createdDate);
			var end_date = res.terminatedDate == undefined ? moment() : moment(res.terminatedDate);
			var duration = moment.duration(end_date.diff(start_date));
			var hours = duration.asHours() - res.runningHours/3600;
			var inst_hourly_cost = 0;
			for (var i in $scope.instanceCostData) {
				  if ($scope.instanceCostData[i].size == res.instanceType || $scope.instanceCostData[i].type == res.instanceType) {
					  inst_hourly_cost = $scope.instanceCostData[i].cost;
				  }
				}
			var cost = (hours * inst_hourly_cost).toFixed(2);
			res.cost = cost;
			$scope.totalCost += (cost * 1);
		})
	})
	$scope.totalCost = $scope.totalCost.toFixed(2)

	$scope.ok = function () {
		$uibModalInstance.close();
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}]);