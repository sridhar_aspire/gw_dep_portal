/**
 * Navigation Controller 
 */
var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('navigation', [ '$rootScope', '$scope', 'api-services', 'ui-services', '$location','$q', function($rootScope, $scope, apiservices, uiservices, $location, $q) {
	$rootScope.isLiveStackTab = true;
	$scope.stackCount = 0;
	$scope.setLiveStackTab  = function(isLiveStackTab) {
		$rootScope.isLiveStackTab =  isLiveStackTab;
	}
	$scope.getLiveStackData = function(pageNo){
		$rootScope.pageNum = pageNo;
		apiservices.getStackList(pageNo).success(function(data){
			$rootScope.totLiveStackCount = Object.keys(data)[0];
			$rootScope.filteredStackList = uiservices.getFormttedStackDate(Object.values(data)[0] != null ? Object.values(data)[0] : []);
		});
	}
	
	$scope.getTerminatedStackData = function(pageNo){
		apiservices.getTerminatedStackList(pageNo).success(function(data){
			$rootScope.totTerminatedStackCount = Object.keys(data)[0];
			$rootScope.filteredTerminatedStackList = uiservices.getFormttedStackDate(Object.values(data)[0] != null ? Object.values(data)[0] : []);
		});
	}
	
	$scope.getSharedStackData = function(pageNo){
		apiservices.getSharedStackList(pageNo).success(function(data){
			$rootScope.totSharedStackCount = Object.keys(data)[0];
			$rootScope.filteredSharedStackList = uiservices.getFormttedStackDate(Object.values(data)[0] != null ? Object.values(data)[0] : []);
		});
	}
	$scope.setCurrentStack = function(stackId) {
		apiservices.getStackDetails(stackId).success(function(data) {
			$rootScope.currentStack = data;
			$scope.stackCount = data.length;
			apiservices.setItem("current_stack", $rootScope.currentStack);
			if($rootScope.pageNum == "" || $rootScope.pageNum == null){
			apiservices.setItem("pageNum", 1);
			}else
			{apiservices.setItem("pageNum", $rootScope.pageNum);}
			$location.path("stackmodify/" + $rootScope.currentStack.id);
		});
	}
	$scope.setCurrentGroup = function(groupId) {
		apiservices.getGroupDetails(groupId).success(function(data) {
			$rootScope.currentGroup = data;
			$scope.groupCount = data.length;
			apiservices.setItem("current_group", $rootScope.currentGroup);
			$location.path("groupmodify/" + $rootScope.currentGroup.id);
		});
	}
	$scope.showTerminatedStack = false;
	$scope.toggleTerminatedStack = function() {
		if (!$scope.showTerminatedStack) {
			$scope.showTerminatedStack = true;
		} else {
			$scope.showTerminatedStack = false;
		}
	}
	
    function onDateRangeSelect(beginDate, endDate) {
    	if (moment(beginDate.formatted, 'MM/DD/YYYY').isValid()) {
    		$scope.filteredStackList = $rootScope.stackList.filter(function(data){
        		return data.createdDate.isBefore(moment(endDate.formatted)) || data.createdDate.isAfter(moment(beginDate.formatted));
        	});
        	$scope.filteredTerminatedStackList = $rootScope.terminatedStackList.filter(function(data){
        		return data.createdDate.isBefore(moment(endDate.formatted)) || data.createdDate.isAfter(moment(beginDate.formatted));
        	});
    	}
    }

    // Configuration of the daterangepicker
    var dt = new Date();
	var fromDate = new Date();
	fromDate.setMonth(fromDate.getMonth() - 5);
    $scope.opt = {
        dateRangeSelectCb: onDateRangeSelect,
        showGrid: false,
        dateFormat: 'mm/dd/yyyy',
        endDateText:"To Date",
        beginDateText:"From Date",
        showGrid:false,
        initSelectedDateRange: {
            end: {
                year: dt.getFullYear(),
                month: dt.getMonth()+1,
                day: dt.getDate()
            },
            begin: {
                year: fromDate.getFullYear(),
                month: fromDate.getMonth()+1,
                day: fromDate.getDate()
            }
        }
    };
    
}]);