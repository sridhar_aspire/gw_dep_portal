/**
 * STACK CONTROLLER
 */
var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('stackController', ['$rootScope', '$scope', 'api-services', 'ui-services', '$location', function($rootScope, $scope, apiservices, uiservices, $location) {
	uiservices.populateStackListInfo(1);
	$scope.launchInProgress = false;
	$scope.tab = 1;
    $scope.setTab = function(newTab){
		$scope.showFieldError = false;
		var alphaNumericRegex = /^[a-zA-Z0-9]+$/i;
		var numericRegex = /^\d+$/;
		
		if ($scope.stackCreateForm.$invalid) {
			$scope.errorMessage = "Required field can't be empty.";
			$scope.showFieldError = true;
			window.scrollTo(1, 1);
		} else if (!alphaNumericRegex.test($scope.stack.name)) {
			$scope.errorMessage = "Please enter a valid Stack Name.";
			$scope.showFieldError = true;
			window.scrollTo(1, 1);
		} else if (!numericRegex.test($scope.stack.plannedUseDays) || $scope.stack.plannedUseDays < 10 || $scope.stack.plannedUseDays > 90) {
			$scope.errorMessage = "Please enter the value in numbers bewtween 10 and 90 for Planned use In Days.";
			$scope.showFieldError = true;
			window.scrollTo(1, 1);
		}else if($scope.stack.applications.filter(function(data) {return data.appId != null;}).length == 0){
			$scope.errorMessage = "Please select atleast one apllication to Launch";
			$scope.showFieldError = true;
			window.scrollTo(1, 1);
		} else {	
			$scope.showFieldError = false;
			window.scrollTo(1, 1);
			$scope.tab = newTab;
		}
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };
    
    $scope.goNext = function(i){
    	$scope.showFieldError = false;
      	$scope.setTab(i);
    }

	uiservices.authenticate(undefined, "", "/stack");
	$scope.instanceCountList = [1,2,3,4,5];
	$scope.envTypes = ["Standard", "Clustered"];
	apiservices.getApplicationMetadataList().success(function(data) {
		$scope.appList = data;
		apiservices.setItem("application_meta_data", data);
	}).error(function(err){
		console.log(err.message);
	});
		
	$scope.stack= {
		name : "",
		envType : false,
		plannedUseDays : "",
		autoShutdown : false,
		applications : [],
	};
	$scope.filterEnv =function() {
		if($scope.environmentType == "Clustered"){
			$scope.stack.envType = true;
		}
		else if($scope.environmentType =="Standard"){
			$scope.stack.envType = false;
		}
	}
    
	$scope.selectApp = function(index, appId, appSelected) {
		if(appSelected){
			$scope.stack.applications[index].appId = appId;
		}else{
			$scope.stack.applications[index].appId = null;
		}
	}
	$scope.saveStackDetails = function() {
		$scope.launchInProgress = true;
		var stackDetails = angular.copy($scope.stack);
		stackDetails.applications = stackDetails.applications.filter(function(data) {
					return data.appId != null;
				});
		if (stackDetails.name.trim() != "" && stackDetails.applications.length > 0) {
			apiservices.createStack(stackDetails).success(function(data) {
				$scope.launchInProgress = false;
				uiservices.populateStackListInfo(1);
				$rootScope.currentStack = data;
				apiservices.setItem("current_stack",$rootScope.currentStack);
				$location.path("/stackmodify");
		    }).error( function(err) {
		    	$scope.showFieldError = true;
		    	$scope.launchInProgress = false;
				$scope.errorMessage = err.message;
				uiservices.populateStackListInfo(1);
			});
		} else {
			console.log("Please provide required details");
		}
	}
} ]);