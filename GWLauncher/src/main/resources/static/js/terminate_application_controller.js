/**
 * TERMINATE APPLICATION CONTROLLER
 */

app.controller('terminateApplicationController', ['appId', 'appName', '$scope', function(appId, appName, $scope){
	$scope.modalTitle = "Terminate Application";
	$scope.appName = appName;
	$scope.cancel = function () {
		$scope.terminateApplicationModalInstance.dismiss('cancel');
	};
	$scope.ok = function () {
		$scope.terminateApplicationModalInstance.close(appId);
	};
}]);