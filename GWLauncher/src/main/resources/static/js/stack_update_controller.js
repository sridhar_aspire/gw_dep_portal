/**
 * 
 */

app.controller('stackUpdateController', ['scope', function($scope){
	$scope.modalTitle = "Update Stack";
	$scope.cancel = function () {
		$scope.updateStackModalInstance.dismiss('cancel');
	};
	$scope.ok = function () {
		$scope.updateStackModalInstance.close($scope.currentStack);
	};
}]);