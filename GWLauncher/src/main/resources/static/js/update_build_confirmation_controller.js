/**
 * UPDATE BUILD CONFIRMATION CONTROLLER
 */

app.controller('updateBuildConfirmationCtrl', ['$scope', 'selectedApp', function($scope, app){
	$scope.modalTitle = "Update Build";
	$scope.app = app;
	$scope.cancel = function () {
		$scope.updateModalInstance.dismiss('cancel');
	};
	$scope.ok = function () {
		$scope.updateModalInstance.close($scope.app);
	};
}]);