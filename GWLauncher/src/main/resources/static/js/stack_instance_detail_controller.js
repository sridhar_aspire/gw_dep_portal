/**
 * STACK INSTANCE CONTROLLER
 */

var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];


app.controller('stackInstanceDetailsCtrl', ['$scope', 'api-services', 
                                            function($scope, apiservices){
	$scope.modalTitle = "Stack Instance Details";
	$scope.legendFormat = "hh:mm a DD-MMM";
	$scope.loadInstances = function() {
		apiservices.getAllResources($scope.currentStack.id, $scope.currentAppId).success(function(data) {
			$scope.instance_list = data;
		});
	}
	$scope.loadInstances();
	
	$scope.loadInstanceMetrics = function(instanceId) {
		apiservices.getInstanceCPUMonitoringData(instanceId).success(function(data) {
			$scope.showCPUGraph(data);
		});
		apiservices.getInstanceNetworkinMonitoringData(instanceId).success(function(data) {
			$scope.showNetworkinGraph(data);
		});
	}
	
	$scope.hideGraph = function() {
		$scope.cpuData = null;
		$scope.networkData = null;
	}
	
	$scope.stopInstance = function(instanceId, index) {
		apiservices.stopInstance($scope.currentStack.id, $scope.currentAppId, instanceId).success(function(stopInstResp) {
			$scope.instance_list[index].status = "pending";
		}).error(function(err){
			console.log(err.message);
		});
	}
	
	$scope.startInstance = function(instanceId, index) {
		apiservices.startInstance($scope.currentStack.id, $scope.currentAppId, instanceId).success(function(startInstResp) {
			$scope.instance_list[index].status = "pending";
		}).error(function(err){
			console.log(err.message);
		});
	}
	
	$scope.showCPUGraph = function(cpuData) {
		$scope.cpuData = cpuData;
		var labelsArray = [];
		for (var dataCount=0;dataCount < $scope.cpuData.TIME.length; dataCount++) {
			if (dataCount%5==0) {
				var time = moment($scope.cpuData.TIME[dataCount]).format($scope.legendFormat);
				labelsArray.push(time);
			} else {
				labelsArray.push("")
			}
		}
		var valuesArray = [$scope.cpuData.MAXIMUM.reverse(), $scope.cpuData.AVERAGE.reverse()];
		var data = {
				labels: labelsArray,
				series: valuesArray
		};
		var options = {
				low : 0,
				axisX: {
					showGrid: true
				},
				axisY:{
					onlyInteger: true
				},
				height: "245px"
		};
		var responsiveOptions = [
		                         ['screen and (max-width: 640px)', {
		                        	 seriesBarDistance: 5,
		                        	 axisX: {
		                        		 labelInterpolationFnc: function (value) {
		                        			 return value[0];
		                        		 }
		                        	 }
		                         }]
		                         ];
		Chartist.Line('#instanceCPUMetrics', data, options);
	};
	
	$scope.showNetworkinGraph = function(networkData) {
		$scope.networkData = networkData;
		var labelsArray = [];
		for (var dataCount=0;dataCount < $scope.networkData.TIME.length; dataCount++) {
			if (dataCount%5==0) {
				var time = moment($scope.networkData.TIME[dataCount]).format($scope.legendFormat);
				labelsArray.push(time);
			} else {
				labelsArray.push("")
			}
		}
		var valuesArray = [$scope.networkData.MAXIMUM.reverse(), $scope.networkData.AVERAGE.reverse()];
		var data = {
				labels: labelsArray,
				series: valuesArray
		};
		var options = {
				low : 0,
				axisX: {
					showGrid: true
				},
				axisY:{
					onlyInteger: true
				},
				height: "245px"
		};
		var responsiveOptions = [
		                         ['screen and (max-width: 640px)', {
		                        	 seriesBarDistance: 5,
		                        	 axisX: {
		                        		 labelInterpolationFnc: function (value) {
		                        			 return value[0];
		                        		 }
		                        	 }
		                         }]
		                         ];
		Chartist.Line('#instanceNetworkinMetrics', data, options);
	};
	
	$scope.cancel = function () {
		$scope.instanceDetailsModal.dismiss('cancel');
	};
}]);