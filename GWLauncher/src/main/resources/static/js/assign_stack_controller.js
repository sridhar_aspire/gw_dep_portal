/**
 * ASSIGN STACK CONTROLLER
 */
var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('assignStackToUserCtrl', [ '$rootScope', '$scope', 'api-services', '$uibModalInstance','currentStack', '$timeout' ,  function($rootScope, $scope, apiservices, $uibModalInstance, currentStack, $timeout) {
	$scope.currentStack = currentStack;
	$scope.isAssignedSuccess = false;
	var loadUsers = function() {
		apiservices.getAllUsers().success(function(userList){
			$scope.user_list = userList;
			$scope.isAssigned = userList[0];
		});
	}
	var loadRoles = function() {
		apiservices.getRolesList().success(function(rolesList){
			$scope.roles_list = rolesList;
		});
	}

	loadUsers();
	loadRoles();
	$scope.title = "ASSIGN STACK : " + $scope.currentStack.name;
	$scope.assignStackToUser = function () {
		apiservices.assignStackToUser($scope.currentStack.id, $scope.isAssigned).success(function(data) {
			$scope.isAssignedSuccess = true;
			$scope.uName = $scope.user_list.filter(function(u){return u.id == $scope.isAssigned;})[0].userName;
			$scope.message = "The " + $scope.currentStack.name + " assigned successfully to " + $scope.uName;
			$timeout(function(){
				$uibModalInstance.close(data);
			}, 2500)
	    }).error( function(err) {
	    	$scope.message = "Stack Assigning failed";
	    	$timeout(function(){
				$uibModalInstance.close(data);
			}, 2500)
	    });
	}
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}]);