/**
 * SETTING CONTROLLER
 */

app.controller('settingController', [ '$rootScope', '$scope', 'api-services', 'ui-services', '$location', '$uibModal', function($rootScope, $scope, apiservices, uiservices, $location, $uibModal) {
	$scope.inProgress = false;
	uiservices.authenticate(undefined, "", "/setting");
	$scope.appList = apiservices.getItem("application_meta_data");
	if (!$scope.appList) {
		apiservices.getApplicationMetadataList().success(function(data) {
			$scope.appList = data;
			$scope.appList.forEach(function(app) {
				app.id = app.id.toUpperCase();
			});
			apiservices.setItem("application_meta_data", data);
		}).error(function(err){
			console.log(err.message);
		});
	}

	apiservices.getInstanceSize().success(function(data){
		$scope.instanceSizeList = data;
	}).error(function(err){
		console.log(err.message);
	});

	$scope.settings = null;
	apiservices.getSettings().success(function(settings_data) {
		$scope.settings = settings_data;
	}).error(function(err) {
		console.log(err.message);
	});

	$scope.saveSettings = function() {
		$scope.inProgress = true;
		apiservices.saveSettings($scope.settings).success(function() {
			$scope.inProgress = false;
			$scope.message = "Settings saved successfully !";
		});
	}
	
	$scope.cancelSettings = function() {
		$location.path("/home");
	
	}
}]);