/**
 * STACK COUNT STATISTICS CONTROLLER
 */
var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('stacksUnusedStatisticsReportModalCtrl', [ '$rootScope', '$scope', 'api-services', '$uibModalInstance', function($rootScope, $scope, apiservices, $uibModalInstance) {
	
	// Configuration of the daterangepicker
	
	$scope.res = apiservices.getItem("unusedInstanceList");
	
	$scope.ok = function () {
		$uibModalInstance.close();
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}]);
