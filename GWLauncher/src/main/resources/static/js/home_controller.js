/**
 * Home Controller
 */
var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('homeController', ['$rootScope', '$scope', '$uibModal', 'api-services', 'ui-services',"$window","$q", "$location", function($rootScope, $scope, $uibModal, apiservices, uiservices, $window, $q, $location) {
	uiservices.authenticate(undefined, "", "/home");
	$scope.populateStackList = function(pageNo){
		apiservices.getStackList(pageNo).success(function(data){
			$rootScope.totLiveStackCount = Object.keys(data)[0];
			$rootScope.stackList = Object.values(data)[0] != null ? Object.values(data)[0] : [];
		});
		apiservices.getTerminatedStackList(pageNo).success(function(data){
			$rootScope.totTerminatedStackCount = Object.keys(data)[0];
			$rootScope.terminatedStackList = Object.values(data)[0] != null ? Object.values(data)[0] : [];
		});
	}
	
	uiservices.populateStackListInfo(1);

	$rootScope.user_management_enabled = false;
	$scope.currentPage = 1;
	$scope.pageSize = 5;
	$scope.totUsersCount = 0;
	$scope.totGroupsCount = 0;
	
	$scope.appList = apiservices.getItem("application_meta_data");
	if (!$scope.appList) {
		apiservices.getApplicationMetadataList().success(function(data) {
			$scope.appList = data;
			$scope.appList.forEach(function(app) {
				app.id = app.id.toUpperCase();
			});
			apiservices.setItem("application_meta_data", data);
		}).error(function(err){
			console.log(err.message);
		});
	}
	
	$scope.loadUsers = function() {
		apiservices.getUserList().success(function(userList){
			$scope.user_list = userList;
		});
	}
	$scope.getLiveUsersData = function(pageNo){
		var self = this;
		apiservices.getUserList(pageNo).success(function(data) {
			$scope.totUsersCount = Object.keys(data)[0];
			$scope.userList = Object.values(data)[0];
			$scope.user_list = $scope.userList;
			/*$rootScope.totUsersCount = Object.keys(data)[0];
			$rootScope.userList = Object.values(data)[0];
			$rootScope.user_list = $rootScope.userList;
			apiservices.setItem("userList", $rootScope.userList);
			$scope.totUsersCount = $rootScope.totUsersCount;
			$scope.user_list = $rootScope.userList;*/
		}).error(function(err) {
			$rootScope.userList=[];
			console.log(err.message);
		});
	}
	$scope.getLiveGroupsData = function(pageNo){
		var self = this;
		apiservices.getGroupList(pageNo).success(function(groupList){
			$scope.totGroupsCount = Object.keys(groupList)[0];
			$scope.groupList = Object.values(groupList)[0];
			$rootScope.group_list = $scope.groupList;
			/*$rootScope.totGroupsCount = Object.keys(groupList)[0];
			$rootScope.groupList = Object.values(groupList)[0];
			$rootScope.group_list = $rootScope.groupList;
			apiservices.setItem("groupList", $rootScope.groupList);
			$scope.totGroupsCount = $rootScope.totGroupsCount;
			$scope.group_list = $rootScope.groupList;*/
		}).error(function(err) {
			$rootScope.groupList=[];
			console.log(err.message);
		});
	}
	$scope.stackStatusFilterList = ["ACTIVE", "TERMINATED"];
	$scope.getLiveUsersData(1);
	$scope.getLiveGroupsData(1);
		
	$scope.loadGroups = function() {
		apiservices.getGroupList().success(function(groupList){
			$scope.group_list = groupList;
		});
	}
	$scope.loadRoles = function() {
		apiservices.getRolesList().success(function(rolesList){
			$scope.roles_list = rolesList;
		});
	}

	$scope.loadPageContent = function() {
		$scope.totalCost = 0;
		$scope.instanceCount = 0;
		$scope.unusedInstanceCount = 0;
		apiservices.getInstanceCostdata().success(function(data) {
			$scope.instance = data;
			apiservices.setItem("instance_cost_data", $scope.instance);
			apiservices.getAllResources().success(function(resData) {
				$scope.instanceCount = resData.filter(function(res){
					return (res.resourceType == 'AWS::EC2::Instance' || res.resourceType == 'AWS::AutoScaling::AutoScalingGroup') && res.status != 'TERMINATED' && res.unused == false;
				}).length;
				
				$scope.unusedInstanceCount = resData.filter(function(res){
					return (res.resourceType == 'AWS::EC2::Instance' || res.resourceType == 'AWS::AutoScaling::AutoScalingGroup') && res.status != 'TERMINATED' && res.unused == true;
				}).length;
				
				$scope.unusedInstanceList = resData.filter(function(res){
					return (res.resourceType == 'AWS::EC2::Instance' || res.resourceType == 'AWS::AutoScaling::AutoScalingGroup') && res.status != 'TERMINATED' && res.unused == true;
				});
			
				apiservices.setItem("unusedInstanceList", $scope.unusedInstanceList);
				
				resData.filter(function(res){
					return res.resourceType == 'AWS::EC2::Instance' || res.resourceType == 'AWS::AutoScaling::AutoScalingGroup' || res.resourceType == 'AWS::ElasticLoadBalancingV2::LoadBalancer';
				}).forEach(function(res){
					var start_date = moment(res.createdDate);
					var end_date = res.terminatedDate == undefined ? moment() : moment(res.terminatedDate);
					var duration = moment.duration(end_date.diff(start_date));
					var inst_hourly_cost = 0;
					var hours = duration.asHours() - res.runningHours/3600;
					for (var i in $scope.instance) {
						  if ($scope.instance[i].size == res.instanceType || $scope.instance[i].type == res.instanceType) {
							  inst_hourly_cost = $scope.instance[i].cost;
						  }
						}
					var cost = (hours * inst_hourly_cost).toFixed(2);
					$scope.totalCost += (cost * 1);
				})
				$scope.totalCost = $scope.totalCost.toFixed(2)
			})
		});
	};

	apiservices.getUserDetails().success(function(user_details){
		$rootScope.user_details = user_details;
		if ($scope.user_details.role.permissions) {
			var permissions = $scope.user_details.role.permissions;
			permissions.forEach(function(permission) {
				if (permission.permissionName == "USER_CREATE") {
					$rootScope.user_management_enabled = true;
					$scope.showStackCountByUserGraph();
					$scope.getInstanceCostByUser();
					$scope.getInstanceCostByStack();
					$scope.getUserCostByGroup();
					/*$scope.loadUsers(1);
					$scope.loadGroups(1);*/
					$scope.loadRoles();
				}
			});
			$scope.loadPageContent();
			apiservices.getRunningResourceCountByApp().success(function(data){
				$scope.runningInstCountByApp = data;
			}).error(function(err){
				console.log(err.message);
			});
			
			apiservices.getTerminatedResourceCountByApp().success(function(data){
				$scope.terminatedInstCountByApp = data;
			}).error(function(err){
				console.log(err.message);
			});
		}
	});
	
	$scope.setCurrentStack = function(stackId) {
		apiservices.getStackDetails(stackId).success(function(data) {
			$rootScope.currentStack = data;
			$scope.stackCount = data.length;
			apiservices.setItem("current_stack", $rootScope.currentStack);
			$location.path("stackmodify/" + $rootScope.currentStack.id);
		});
	}

	$scope.addGroup = function() {
		$scope.groupCreateModalInstance = $uibModal.open({
		      templateUrl: '/contents/confirmationPopup/create_new_group.html',
		      controller: 'createNewGroupCtrl',
		      resolve: {
		          editId: function () {
		        	  return null;
		          },
				 scope: function () {
			       	  return $scope;
			         }
		      },
		      size:'lg'
		});
		$scope.groupCreateModalInstance.result.then(function(success) {
			loadGroups();
		});
	};
	
	$scope.editGroup = function(groupId) {
		$scope.ModalInstance = $uibModal.open({
		      templateUrl: '/contents/confirmationPopup/create_new_group.html',
		      controller: 'createNewGroupCtrl',
		      resolve: {
		          editId: function () {
		        	  return groupId;
		          },
				 scope: function () {
		       	  return $scope;
		         }
		      },
		      size:'lg'
		});
		$scope.ModalInstance.result.then(function(success) {
			loadGroups();
		});
	};
	$scope.deleteGroup = function(groupId) {
		apiservices.deleteGroup(groupId).success(function(data) {
			$scope.getLiveGroupsData(1);
		});
	}

	$scope.showCostReport = function(stackId) {
		var stackDetails = apiservices.getAllResourcesForStack(stackId).success(function(data) {
			var modalInstance = $uibModal.open({
			      templateUrl: '/contents/confirmationPopup/cost_report_modal.html',
			      controller: 'costReportModalCtrl',
			      size:'lg',
		    	  backdrop : 'static',
				  keyboard : true,
				  resolve : {
					selectedStack : function() {
						return data;
					}
				  }
			});
		});
	};
	
	$scope.showStackCostHistory = function() {
		var modalInstance = $uibModal.open({
		      templateUrl: '/contents/confirmationPopup/stacks_cost_history_report_modal.html',
		      controller: 'stacksCostHistoryReportModalCtrl',
		      size:'lg'
		});
	}
	$scope.showInstanceStatistics = function() {
		var modalInstance = $uibModal.open({
		      templateUrl: '/contents/confirmationPopup/stacks_cost_statistics_report_modal.html',
		      controller: 'stacksStatisticsReportModalCtrl',
		      size:'lg'
		});
	}
	$scope.showUnusedInstanceStatistics = function() {
		var modalInstance = $uibModal.open({
		      templateUrl: '/contents/confirmationPopup/stacks_cost_Unused_Statistics_report_modal.html',
		      controller: 'stacksUnusedStatisticsReportModalCtrl',
		      size:'lg'
		});
	}
	
	$scope.showStackCountStatistics = function() {
		var modalInstance = $uibModal.open({
		      templateUrl: '/contents/confirmationPopup/stacks_count_statistics_report_modal.html',
		      controller: 'stackCountStatisticsCtrl',
		      size:'lg'
		});
	}
	$scope.getLabels = function(stackCountData) {
		var keys = [];
		for(var k in stackCountData) keys.push(k);
		return keys;
	}
	$scope.getSeries = function(stackCountData) {
		var data = [];
		for(var k in stackCountData) data.push(stackCountData[k]);
		return data;
	}
	$scope.showStackCountByUserGraph = function() {
		apiservices.getStackCountByUser().success(function(stackCountData) {
			$scope.labelData = $scope.getLabels(stackCountData);
			$scope.seriesData = $scope.getSeries(stackCountData);
			var data = {
				labels: $scope.labelData,
				series:[$scope.seriesData]
			};
			var options = {
				seriesBarDistance: 4,
				low : 0,
				high: Math.max.apply(null, $scope.seriesData) < 10 ? 10 : Math.max.apply(null, $scope.seriesData),
				axisX: {
					showGrid: false
				},
				axisY:{
					onlyInteger: true
				},
				height: "245px"
			};
			var responsiveOptions = [
			                         ['screen and (max-width: 640px)', {
			                        	 seriesBarDistance: 5,
			                        	 axisX: {
			                        		 labelInterpolationFnc: function (value) {
			                        			 return value[0];
			                        		 }
			                        	 }
			                         }]
			                         ];
			var chart = Chartist.Bar('#usageStats', data, options, responsiveOptions);
			chart.on('draw', (ctx) => {
			      if (ctx.type === 'label' && ctx.axis.units.pos === 'x') {
			        const dX = ctx.width / 2 + (100 - ctx.width)
			        ctx.element.attr({ x: ctx.element.attr('x') - dX })
			      }
			})
		});
	};
	$scope.getInstanceCostByUser = function() {
		apiservices.getTopInstanceCostByUser().success(function(costData){
			var labelsArray = Object.keys(costData);
			var valuesArray = Object.values(costData).map(function(value){ return Number(value.toFixed(2))});
			var data = {
					labels: labelsArray,
					series: [valuesArray]
			};
			var options = {
					seriesBarDistance: 10,
					low: 0,
					high: Math.max.apply(null, valuesArray) < 10 ? 10 : Math.max.apply(null, valuesArray),
					axisX: {
						showGrid: false
					},
					axisY:{
						onlyInteger : true
					},
					height: "245px"
			};
			var responsiveOptions = [
                 ['screen and (max-width: 640px)', {
                	 seriesBarDistance: 5,
                	 axisX: {
                		 labelInterpolationFnc: function (value) {
                			 return value[0];
                		 }
                	 }
                 }]
                 ];
			var chart = Chartist.Bar('#usageStatses', data, options, responsiveOptions);
			chart.on('draw', (ctx) => {
			      if (ctx.type === 'label' && ctx.axis.units.pos === 'x') {
			        const dX = ctx.width / 2 + (100 - ctx.width)
			        ctx.element.attr({ x: ctx.element.attr('x') - dX })
			      }
			})
		}).error(function(err){
			console.log('error while getting user vs cost');
		})
	};
	$scope.getInstanceCostByStack = function() {
		apiservices.getTopInstanceCostByStack().success(function(costData){
			var labelsArray = Object.keys(costData);
			var valuesArray = Object.values(costData).map(function(value){ return Number(value.toFixed(2))});
			var data = {
					labels: labelsArray,
					series: [valuesArray]
			};
			var options = {
					seriesBarDistance: 10,
					low: 0,
					high: Math.max.apply(null, valuesArray) < 10 ? 10 : Math.max.apply(null, valuesArray),
					axisX: {
						showGrid: false
					},
					axisY:{
						onlyInteger : true
					},
					height: "245px"
			};
			var responsiveOptions = [
                 ['screen and (max-width: 640px)', {
                	 seriesBarDistance: 5,
                	 axisX: {
                		 labelInterpolationFnc: function (value) {
                			 return value[0];
                		 }
                	 }
                 }]
                 ];
			var chart = Chartist.Bar('#usageStacks', data, options, responsiveOptions);
			chart.on('draw', (ctx) => {
			      if (ctx.type === 'label' && ctx.axis.units.pos === 'x') {
			        const dX = ctx.width / 2 + (100 - ctx.width)
			        ctx.element.attr({ x: ctx.element.attr('x') - dX })
			      }
			})
		}).error(function(err){
			console.log('error while getting stack vs cost');
		})
	};
	
	$scope.getUserCostByGroup = function() {
		apiservices.getTopUsersCostByGroup().success(function(costData){
			var labelsArray = Object.keys(costData);
			var valuesArray = Object.values(costData).map(function(value){ return Number(value.toFixed(2))});
			var data = {
					labels: labelsArray,
					series: [valuesArray]
			};
			var options = {
					seriesBarDistance: 10,
					low: 0,
					high: Math.max.apply(null, valuesArray) < 10 ? 10 : Math.max.apply(null, valuesArray),
					axisX: {
						showGrid: false
					},
					axisY:{
						onlyInteger : true
					},
					height: "245px"
			};
			var responsiveOptions = [
                 ['screen and (max-width: 640px)', {
                	 seriesBarDistance: 5,
                	 axisX: {
                		 labelInterpolationFnc: function (value) {
                			 return value[0];
                		 }
                	 }
                 }]
                 ];
			var chart = Chartist.Bar('#groupCost', data, options, responsiveOptions);
			chart.on('draw', (ctx) => {
			      if (ctx.type === 'label' && ctx.axis.units.pos === 'x') {
			        const dX = ctx.width / 2 + (100 - ctx.width)
			        ctx.element.attr({ x: ctx.element.attr('x') - dX })
			      }
			})
		}).error(function(err){
			console.log('error while getting Group vs cost');
		})
	};
	
	
	
}]);