/**
 * Login Controller
 */

var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('loginController', ['$rootScope','$http', '$scope', '$location', 'api-services', 
                                   function($rootScope, $http, $scope, $location, apiservices) {
	$rootScope.showPostLoginContent = false;
	$scope.credentials = {};
	if(!$rootScope.authenticated){
		localStorage.clear();
	}
	if($location.path() == "/logout"){
		$http.get('/logout').success(function() {
			$rootScope.authenticated = false;
			$location.path("/logout");
		}).error(function(data) {
			$rootScope.authenticated = false;
		});
	}

	var authenticate = function(callback) {
		apiservices.getUserDetails().success(function(data) {
			if (data.userName) {
				$rootScope.authenticated = true;
				$rootScope.showPostLoginContent = true;
				$location.path("/home");
			} else {
				$rootScope.authenticated = false;
				$scope.redirectToLogin();
			}
			callback && callback();
		}).error(function() {
			$rootScope.authenticated = false;
			$scope.redirectToLogin();
			callback && callback();
		});
	}
	
	$scope.redirectToLogin = function() {
		apiservices.getAuthUrl().success(function(data) {
			window.location = data;
		});
	}
	
	authenticate();
	$scope.login = function() {
		$scope.progress = true;
		authenticate(function() {
			$scope.progress = false;
			if ($rootScope.authenticated) {
				$location.path("/home");
				$scope.error = false;
			} else {
//				$location.path("/login");
				$scope.error = true;
			}
		});
	};
}]);