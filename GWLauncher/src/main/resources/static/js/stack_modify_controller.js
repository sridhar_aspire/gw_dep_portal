/**
 * STACK MODIFY CONTROLLER
 */
var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('stackModifyController', [ '$rootScope', '$scope', 'api-services', 'ui-services', '$location', '$uibModal', function($rootScope, $scope, apiservices, uiservices, $location, $uibModal) {
	uiservices.authenticate(undefined, "", "/stackmodify");
	$scope.launchInProgress = [];
	$scope.disableAtLaunch = false;
	$scope.currentStack = apiservices.getItem("current_stack");
	$scope.instanceCountList = [1,2,3,4,5];
	$scope.pageNum = apiservices.getItem("pageNum");
	uiservices.populateStackListInfo($scope.pageNum);

	apiservices.getUserDetails().success(function(user_details){
		$scope.user_details = user_details;
		if ($scope.user_details.role.permissions) {
			var permissions = $scope.user_details.role.permissions;
			permissions.forEach(function(permission) {
				if (permission.permissionName == "ASSIGN_ROLE") {
					$scope.user_management_enabled = true;
				}
			});
		}
	});
	apiservices.getApplicationMetadataList().success(function(data) {
		$scope.appList = data.filter(function(appData){
			return $scope.currentStack.applications.map(function(d) {
					if(d.appId == appData.id){
						d.buildInfo = appData.buildInfo;
					}
					return d.appId
				}).indexOf(appData.id) == -1;
		});
	}).error(function(err){
		console.log(err.message);
	});
	
	$scope.runningAppInProgress = function(){
		for(var i = 0; i < $scope.currentStack.applications.length; i++) {
		   var app = $scope.currentStack.applications[i];
		   if(app.status != "TERMINATED" && ['INSTANCE_CREATION_STARTED', 'TERMINATION_STARTED'].indexOf(app.status) != -1){
			   $scope.disableAtLaunch = true;
			   break;
			}
		}
	}
	$scope.runningAppInProgress();
	$scope.assignStackToUser = function() {
		$scope.userCreateModalInstance = $uibModal.open({
		      templateUrl: '/contents/confirmationPopup/assign_stack_to_user.html',
		      scope: $scope,
		      size:'md',
		      controller: 'assignStackToUserCtrl',
		      backdrop : 'static',
			  keyboard : true,
		      resolve: {
		    	  currentStack: function () {
		        	  return $scope.currentStack;
		          }
		      },
		      size:'lg'
		});
		
		$scope.userCreateModalInstance.result.then(function(success) {
			console.log("Assigned");
		});
	};
	
	$scope.shareStackToUser = function() {
		$scope.userShareModalInstance = $uibModal.open({
		      templateUrl: '/contents/confirmationPopup/share_stack_to_user.html',
		      scope: $scope,
		      size:'md',
		      controller: 'shareStackToUserCtrl',
		      backdrop : 'static',
			  keyboard : true,
		      resolve: {
		    	  currentStack: function () {
		        	  return $scope.currentStack;
		          }
		      },
		      size:'lg'
		});
		
		$scope.userShareModalInstance.result.then(function(success) {
			console.log("Shared");
		});
	};
	
	$scope.viewInstances = function(appId) {
		$scope.currentAppId = appId;
		$scope.instanceDetailsModal = $uibModal.open({
		      templateUrl: '/contents/confirmationPopup/stack_instance_details.html',
		      scope: $scope,
		      controller: 'stackInstanceDetailsCtrl',
		      backdrop : 'static',
			  keyboard : true,
		      resolve: {
		    	  getScope: function () {
		        	  return $scope;
		          },
		      },
		      size:'lg'
		});
	};
	
	$scope.updateBuild = function(app) {
			$scope.updateModalInstance = $uibModal.open({
			      templateUrl: '/contents/confirmationPopup/updateBuildConfirmation.html',
			      scope: $scope,
			      size:'md',
			      controller:"updateBuildConfirmationCtrl",
			      backdrop : 'static',
				  keyboard : true,
				  resolve : {
					selectedApp : function() {
						return app;
					}
				  }
			});
			$scope.updateModalInstance.result.then(function(data) {
				apiservices.updateBuild(data).success(function(data){
					$location.path("/home");
				}).error(function(err){
					console.log(err.message);
				});
			});
		};
		
		$scope.updateStack = function() {
			$scope.updateStackModalInstance = $uibModal.open({
			      templateUrl: '/contents/confirmationPopup/stack_update_confirmation_popup.html',
			      scope: $scope,
			      size:'md',
			      controller:"stackUpdateController",
			      backdrop : 'static',
				  keyboard : true,
				  resolve : { 
					  scope : function(){
						  return $scope;
					  }
				  }
				 			});
			$scope.updateStackModalInstance.result.then(function(data) {
				apiservices.updateStack(data).success(function(data) {
					uiservices.populateStackListInfo(1);
					$rootScope.currentStack = data;
					apiservices.setItem("current_stack",$rootScope.currentStack);
					$location.path("/home");
				}).error(function(err){
					console.log(err.message);
				});
			});
		};
		$scope.terminateApplication = function(appId, appName) {
			$scope.terminateApplicationModalInstance = $uibModal.open({
			      templateUrl: '/contents/confirmationPopup/terminate_application_confirmation_popup.html',
			      scope: $scope,
			      size:'md',
			      controller:"terminateApplicationController",
			      backdrop : 'static',
				  keyboard : true,
				  resolve : {
					appId : function() {
						return appId;
					},
					appName : function() {
						return appName;
					}
				  }
			});
			$scope.terminateApplicationModalInstance.result.then(function(data) {
				$scope.disableAtLaunch = true;
				apiservices.terminateApplication(appId).success(function(data){
					$scope.disableAtLaunch = false;
					uiservices.populateStackListInfo(1);
					$rootScope.currentStack = data;
					apiservices.setItem("current_stack",$rootScope.currentStack);
					$location.path("/home");
				}).error(function(err){
					$scope.disableAtLaunch = false;
					console.log(err.message);
				});
			});
		};
		$scope.terminateStack = function() {
			$scope.terminateStackModalInstance = $uibModal.open({
			      templateUrl: '/contents/confirmationPopup/stack_delete_confirmation_popup.html',
			      scope: $scope,
			      size:'md',
			      controller:"stackDeleteController",
			      backdrop : 'static',
				  keyboard : true,
				  resolve : { 
					  scope : function(){
						  return $scope;
					  }
				  }
	 			});
			
			$scope.terminateStackModalInstance.result.then(function(data) {
				$scope.disableAtLaunch = true;
				apiservices.deleteStack(data).success(function(data) {
					$scope.disableAtLaunch = false;
					uiservices.populateStackListInfo(1);
					$rootScope.currentStack = data;
					apiservices.setItem("current_stack",$rootScope.currentStack);
					$location.path("/home");
				}).error(function(err){
					$scope.disableAtLaunch = false;
					console.log(err.message);
				});
			});
		};
	
	$scope.launchApp = function(app, index) {
		$scope.launchInProgress[index] = true;
		$scope.disableAtLaunch = true;
		apiservices.launchApp($scope.currentStack.id, app).success(function(data) {
			uiservices.populateStackListInfo(1);
			$scope.launchInProgress[index] = false;
			$scope.disableAtLaunch = false;
			$rootScope.currentStack = data;
			apiservices.setItem("current_stack",$rootScope.currentStack);
			$location.path("/home");
		}).error( function(err) {
	    	$scope.launchInProgress[index] = false;
	    	$scope.disableAtLaunch = false;
	    	uiservices.populateStackListInfo(1);
	    	console.log(err.message);
	    });
	}
} ]);