/**
 * 
 */
var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('createNewUserCtrl', [ '$rootScope', '$scope', 'api-services', '$uibModalInstance', 'editId', function($rootScope, $scope, apiservices, $uibModalInstance, editId) {
	apiservices.getRolesList().success(function(data) {
		$scope.roles = data;
	});
	if (!editId) {
		$scope.user = {};
		$scope.title = "ADD USER";
	} else {
		$scope.title = "EDIT USER";
		apiservices.getUser(editId).success(function(data) {
			$scope.user = data;
		});
	}
	
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}]);