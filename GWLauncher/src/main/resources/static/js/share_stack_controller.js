/**
 * SHARE_STACK_CONTROLLER
 */
var date_format = "DD-MMM-YYYY hh:mm";
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

app.controller('shareStackToUserCtrl', [ '$rootScope', '$scope', 'api-services', '$uibModalInstance','currentStack', '$timeout' ,  function($rootScope, $scope, apiservices, $uibModalInstance, currentStack, $timeout) {
	$scope.currentStack = currentStack;
	$scope.isSharedSuccess = false;
	var loadSharedUsers = function() {
		apiservices.getSharedUsers($scope.currentStack.id).success(function(shared_user_list){
			$scope.shared_user_list = shared_user_list;
			apiservices.getAllUsers().success(function(userList){
				$scope.user_list = [];
				if ($scope.shared_user_list == null || $scope.shared_user_list.length == 0) {
					$scope.user_list = userList;
				} else {
					for (var userCount =0; userCount < userList.length; userCount++) {
						var userShared = false;
						for (var sharedUserCount=0; sharedUserCount<$scope.shared_user_list.length;sharedUserCount++) {
							if (userList[userCount].id == $scope.shared_user_list[sharedUserCount].id) {
								userShared = true;
							}
						}
						if (!userShared) {
							$scope.user_list.push(userList[userCount]);
						}
					}
				}
			});
		});
	}
	$scope.loadSharedGroup = function(){
		apiservices.getSharedGroupList($scope.currentStack.id, true).success(function(data){
			$scope.shared_group_list = data;
		})
	}
	
	$scope.loadUnSharedGroup = function(){
		apiservices.getSharedGroupList($scope.currentStack.id, false).success(function(data){
			$scope.un_shared_group_list = data;
		})
	}

	loadSharedUsers();
	$scope.loadSharedGroup();
	$scope.loadUnSharedGroup();
	$scope.title = "SHARE STACK : " + $scope.currentStack.name;
	$scope.shareStackToUser = function (userId) {
		apiservices.shareStackToUser($scope.currentStack.id, userId).success(function(data) {
			$scope.isSharedSuccess = true;
			$scope.message = "The " + $scope.currentStack.name + " is successfully shared ";
			loadSharedUsers();
	    }).error( function(err) {
	    	$scope.message = "Failed while sharing";
	    });
	}
	$scope.removeSharedUser = function (userId) {
		apiservices.deleteSharedUser($scope.currentStack.id, userId).success(function(data) {
			$scope.isSharedSuccess = true;
			$scope.message = "Sharing removed";
			loadSharedUsers();
	    }).error( function(err) {
	    	$scope.message = "Failed while deleting share";
	    });
	}
	$scope.removeSharedGroup = function(grpId){
		apiservices.removeStackFromGroup(grpId, $scope.currentStack.id).success(function(data){
			$scope.loadSharedGroup();
			$scope.loadUnSharedGroup();
		})
	}
	$scope.shareStackToGroup = function(grpId){
		apiservices.shareStackToGroup(grpId, $scope.currentStack.id).success(function(data){
			$scope.loadSharedGroup();
			$scope.loadUnSharedGroup();
		})
	}
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
}]);