package com.gw.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gw.model.Setting;

@Transactional
public interface SettingRepository extends CrudRepository<Setting, String> {
	
	@Query("SELECT s FROM Setting s")
    public List<Setting> getAllSettings();
	
	@Query("SELECT s FROM Setting s where s.key = :appId")
	public Setting getDefaultInstanceSizeByAppId(@Param("appId") String appId);

}
