package com.gw.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gw.model.Group;

@Transactional
public interface GroupRepository extends CrudRepository<Group, Integer>{
	
	/*public static final String SELECT_USER_GROUP_BY_ID = "select g from Group g where g.id= :groupId and g.userId= :userId";*/
	//public static final String SELECT_GROUP_BY_ID = "select g from Group g where g.id= :groupId";
	
	public static final String  SELECT_SHARED_GROUP = "select g from Group g where g.id in (select distinct sg.id from Stack s join s.sharedGroup sg where s.id = :sId) group by g.id";
	public static final String  SELECT_UN_SHARED_GROUP = "select g from Group g where g.id not in (select distinct sg.id from Stack s join s.sharedGroup sg where s.id = :sId) group by g.id";

	@Query("SELECT g FROM Group g WHERE g.name = :name")
	public Group findByGroupName(@Param("name") String name);
	
	@Query("SELECT u.userName FROM User u")
	public List<String> getUsersList();
	
	/*@Query("SELECT u FROM User u where u.group_id = :groupId")
	public List<User> getAddedUsersForGroup(@Param("groupId") int groupId);*/
	
	@Query("SELECT g FROM Group g where g.terminatedDate is null")
    public List<Group> getAllGroups();
	
	@Query("SELECT g FROM Group g where g.terminatedDate is null")
    public Page<Group> getAllLiveGroups(Pageable pageable);
	
	/*@Query(SELECT_USER_GROUP_BY_ID)
	public Group getUserGroupById(@Param("userId") int userId, @Param("groupId") String groupId);*/
	
	/*@Query(SELECT_GROUP_BY_ID)
	public Group getGroupById(@Param("groupId") String groupId);*/
	
 
	
	/*@Query("SELECT ug FROM UserGroup u WHERE ug.email = :email")
	public UserGroup findByEmail(@Param("email") String email);*/
	
	@Query(SELECT_SHARED_GROUP)
	public List<Group> getAllSharedGroups(@Param("sId") String stackId);

	@Query(SELECT_UN_SHARED_GROUP)
	public List<Group> getAllUnSharedGroups(@Param("sId") String stackId);

}
