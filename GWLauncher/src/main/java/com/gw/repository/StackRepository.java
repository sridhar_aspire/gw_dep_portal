package com.gw.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gw.model.Stack;
import com.gw.model.User;

@Transactional
public interface StackRepository extends PagingAndSortingRepository<Stack, String> {
			
	public static final String SELECT_ALL_LIVE_STACKS = "SELECT s FROM Stack s, User u WHERE u.id = s.userId and s.status != 'TERMINATED' ORDER BY u.roleId DESC";
	public static final String SELECT_ALL_LIVE_STACKS_META_DATA = "SELECT s.id, s.name, s.status, s.userId, s.createdDate,u.userName FROM Stack s, User u WHERE u.id = s.userId and s.status != 'TERMINATED' ORDER BY u.roleId DESC";
	
	public static final String SELECT_ALL_TERMINATED_STACKS = "SELECT s FROM Stack s, User u WHERE u.id = s.userId and s.status = 'TERMINATED' ORDER BY u.roleId DESC";
	public static final String SELECT_ALL_TERMINATED_STACKS_META_DATA = "SELECT s.id, s.name, s.status, s.userId, s.createdDate,u.userName FROM Stack s, User u WHERE u.id = s.userId and s.status = 'TERMINATED' ORDER BY u.roleId DESC";
	
	public static final String SELECT_ALL_SHARED_STACKS = "SELECT s FROM Stack s left join s.sharedUsers u WHERE u.id = :userId AND s.status != 'TERMINATED'";
	public static final String SELECT_ALL_SHARED_STACKS_META_DATA = "SELECT s.id, s.name, s.status, s.userId, s.createdDate,u.userName,0 FROM Stack s join s.sharedUsers u WHERE u.id = :userId AND s.status != 'TERMINATED'";
	
	public static final String SELECT_STACK_BY_ID = "select s from Stack s, Application a where s.id = a.stackId and s.id= :stackId";
	public static final String SELECT_USER_STACK_BY_ID = "select s from Stack s, Application a where s.id = a.stackId and s.id= :stackId and s.userId= :userId";
	public static final String SELECT_STACK_RESOURCES_BY_ID = "select s from Stack s, Application a, Resource r where s.id = a.stackId and a.id = r.appId and s.id= :stackId ";
	public static final String SELECT_STACK_COUNT_BY_MONTH =  "select month(s.createdDate), count(s.awsStackid) as tc from Stack s where s.awsStackid is not null and s.status != 'TERMINATED' group by (month(s.createdDate)) order by(tc) desc";
	public static final String SELECT_STACK_COUNT_BETWEEN_BY_MONTH ="select month(s.createdDate),count(s.awsStackid) as tc from Stack s  where s.awsStackid is not null and s.createdDate between :startDate AND :endDate and s.status!='TERMINATED' group by month(s.createdDate) order by (tc) desc";
	
	public static final String SELECT_SHARED_USERS_FOR_STACK = "SELECT u FROM Stack s join s.sharedUsers u WHERE s.id = :stackId";
	
	public static final String SELECT_STACK_COUNT_PER_USER_BY_MONTH = "select month(s.createdDate), count(s.awsStackid) from Stack s, User u where u.id = s.userId and u.id = :uId and s.awsStackid is not null and s.status != 'TERMINATED' group by (month(s.createdDate))";;
	public static final String SELECT_STACK_COUNT_BETWEEN_PER_USER_BY_MONTH = "select month(s.createdDate), count(s.awsStackid) from Stack s, User u where u.id = s.userId and u.id = :uId and s.awsStackid is not null and s.status != 'TERMINATED' and s.createdDate between :startDate AND :endDate group by (month(s.createdDate))";
	public static final String SELECT_STACK_COUNT_PER_USER = " select count(s.awsStackid) from Stack s, User u where u.id = s.userId and u.id = :uId and s.awsStackid is not null and s.status != 'TERMINATED'";
	public static final String SELECT_ALL_SHARED_GROUP_STACKS_META_DATA = "SELECT s.id, s.name, s.status, s.userId, s.createdDate,g.admin,1 FROM Stack s join s.sharedGroup g WHERE g.id = :groupId AND s.status != 'TERMINATED'";
	
	
	@Query("SELECT s.name FROM Stack s where s.name = :stackName and s.status != 'TERMINATED'")
	public List<Stack> getStackNames(@Param("stackName")String stackName);
	
	@Query("SELECT s FROM Stack s, User u WHERE u.id = s.userId and u.id = :userId")
    public List<Stack> getAllMyStacks(@Param("userId") int userId);
	
	@Query("SELECT s FROM Stack s")
    public List<Stack> getAllStacks();
	
	@Query(SELECT_STACK_COUNT_PER_USER)
    public int getStackCountPerUser(@Param("uId") int uId);
	
	 @Query("SELECT s FROM Stack s, User u WHERE u.id = s.userId and u.id = :userId AND s.status != 'TERMINATED'")
    public Page<Stack> getMyLiveStacks(Pageable pageable, @Param("userId") int userId);
	
	@Query("SELECT s.id, s.name, s.status, s.userId, s.createdDate,u.userName FROM Stack s, User u WHERE u.id = s.userId and u.id = :userId AND s.status != 'TERMINATED'")
    public Page<Object[]> getMyLiveStackMetaData(Pageable pageable, @Param("userId") int userId);
	
	@Query("SELECT s FROM Stack s, User u WHERE u.id = s.userId and u.id = :userId AND s.status = 'TERMINATED'")
    public Page<Stack> getMyTerminatedStacks(Pageable pageable, @Param("userId") int userId);
	
	@Query("SELECT s.id, s.name, s.status, s.userId, s.createdDate,u.userName FROM Stack s, User u WHERE u.id = s.userId and u.id = :userId AND s.status = 'TERMINATED'")
    public Page<Object[]> getMyTerminatedStackMetaData(Pageable pageable, @Param("userId") int userId);
	
	@Query(SELECT_ALL_LIVE_STACKS)
    public List<Stack> getAllLiveStacks();
	
	@Query(SELECT_ALL_LIVE_STACKS_META_DATA)
	public Page<Object[]> getAllLiveStackMetaData(Pageable pageable);

	@Query(SELECT_ALL_TERMINATED_STACKS_META_DATA)
	public Page<Object[]> getAllTerminatedStackMetaData(Pageable pageable);
	
	@Query(SELECT_ALL_SHARED_STACKS_META_DATA)
	public Page<Object[]> getSharedStackMetaData(Pageable pageable, @Param("userId") int userId);
	
	@Query("SELECT u.userName, COUNT(s) FROM Stack s, User u WHERE u.id = s.userId and s.status != 'TERMINATED' GROUP BY u.id ORDER BY COUNT(s) DESC")
	public Page<Object[]> getTopStackCountByUser(Pageable pageRequest);

	@Query(SELECT_STACK_BY_ID)
	public Stack getStackById(@Param("stackId") String stackId);

	@Query(SELECT_USER_STACK_BY_ID)
	public Stack getUserStackById(@Param("userId") int userId, @Param("stackId") String stackId);
	
	@Override
	default Stack findOne(String id) {
		return getStackById(id);
	}

	@Query(SELECT_STACK_RESOURCES_BY_ID)
	public Stack getStackResourcesById(@Param("stackId") String stackId);
	
	@Query(SELECT_STACK_COUNT_BY_MONTH)
	public List<Object[]> getStackCountByMonth();

	@Query(SELECT_STACK_COUNT_BETWEEN_BY_MONTH)
	public List<Object[]> getStackCountByMonthBetween(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Query(SELECT_STACK_COUNT_PER_USER_BY_MONTH)
	public List<Object[]> getStackCountPerUserByMonth(@Param("uId") int userId);
	
	@Query(SELECT_STACK_COUNT_BETWEEN_PER_USER_BY_MONTH)
	public List<Object[]> getStackCountBetweenPerUserByMonth(@Param("uId") int userId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	@Query(SELECT_SHARED_USERS_FOR_STACK)
	public List<User> getSharedUsersForStack(@Param("stackId") String stackId);

	@Query(SELECT_ALL_SHARED_GROUP_STACKS_META_DATA)
	public Page<Object[]> getSharedGroupStackMetaData(Pageable pageRequest, @Param("groupId") int groupId);

}
