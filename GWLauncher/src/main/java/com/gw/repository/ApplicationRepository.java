package com.gw.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gw.model.Application;

@Transactional
public interface ApplicationRepository extends CrudRepository<Application, String> {

	public static final String SELECT_APPLICATION_BY_ID = "select a from Application a, Resource r where a.id = r.appId and a.id= :id";

	Page<Application> findAll(Pageable pageable);
	
	@Query("SELECT a FROM Application a WHERE a.stackId = :stackId")
	public List<Application> getStackApplications(@Param("stackId") String stackId);
	
	@Override
	default Application findOne(String id) {
		return getApplicationById(id);
	}

	@Query(SELECT_APPLICATION_BY_ID)
	public Application getApplicationById(@Param("id") String id);

}
