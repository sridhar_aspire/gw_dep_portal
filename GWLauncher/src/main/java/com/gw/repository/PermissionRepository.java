package com.gw.repository;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.gw.model.Permission;

@Transactional
public interface PermissionRepository extends CrudRepository<Permission, Long> {

}
