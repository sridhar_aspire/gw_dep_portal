package com.gw.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gw.model.User;

@Transactional
public interface UserRepository extends CrudRepository<User, Integer> {

	@Query("SELECT u FROM User u WHERE u.userName = :username")
	public User findByUserName(@Param("username") String username);
	
	@Query("SELECT u FROM User u WHERE u.email = :email")
	public User findByEmail(@Param("email") String email);
	
	@Query("SELECT u FROM User u WHERE u.terminatedDate IS NULL")
    public Page<User> getAllLiveUsers(Pageable pageable);
	
	@Query("SELECT u FROM User u where u.groupId = :groupId")
	public List<User> getAddedUsersForGroup(@Param("groupId") int groupId);
	
	@Query("SELECT u FROM User u where u.email like CONCAT('%',:serachtext,'%')")
	public List<User> findUserbySearch(@Param("serachtext") String serachtext);
	
	@Query("SELECT u FROM User u WHERE u.terminatedDate IS NULL")
	public List<User> getAllUsers();

	@Query("SELECT u FROM User u WHERE u.groupId = :grpId")
	public List<User> getUsersByGrpId(@Param("grpId") int grpId);
	
	@Query("SELECT u FROM User u WHERE u.terminatedDate IS NULL and u.groupId IS NULL")
    public Page<User> getAvailableUsers(Pageable pageable);
	
	@Query("SELECT u FROM User u WHERE u.terminatedDate IS NULL and u.groupId = :groupID")
    public Page<User> getAddedUsers(Pageable pageable, @Param("groupID") int groupID);
}
