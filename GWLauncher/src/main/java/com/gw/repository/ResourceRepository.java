package com.gw.repository;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gw.model.Resource;

@Transactional
public interface ResourceRepository extends CrudRepository<Resource, String> {
	
	public static final String SELECT_INSTANCE_COUNT_BY_MONTH =  "select month(r.createdDate), sum(r.instCount) from Resource r, Application a where r.appId = a.id  "
			+ " and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = 'AWS::EC2::Instance') and r.status != 'TERMINATED' group by (month(r.createdDate))";
	
	public static final String SELECT_INSTANCE_COUNT_BY_MONTH_BETWEEN =  "select month(r.createdDate), sum(r.instCount) from Resource r, Application a where r.appId = a.id  "
			+ " and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = 'AWS::EC2::Instance') and r.status != 'TERMINATED' and r.createdDate between :startDate AND :endDate group by (month(r.createdDate))";
	
	public static final String SELECT_INSTANCE_COUNT_PER_USER_BY_MONTH =  "select month(r.createdDate), sum(r.instCount) from Resource r, Application a, Stack s, User u where r.appId = a.id and"
			+ " a.stackId = s.id and s.userId = u.id and u.id = :uId and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = 'AWS::EC2::Instance') and r.status != 'TERMINATED' group by (month(r.createdDate))";
	
	public static final String SELECT_INSTANCE_COUNT_BETWEEN_PER_USER_BY_MONTH =  "select month(r.createdDate), sum(r.instCount) from Resource r, Application a, Stack s, User u where r.appId = a.id and"
			+ " a.stackId = s.id and s.userId = u.id and u.id = :uId and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = 'AWS::EC2::Instance') and r.status != 'TERMINATED' and r.createdDate between :startDate AND :endDate group by (month(r.createdDate))";
	
	public static final String SELECT_INSTANCE_COST_BY_USER = "select u.userName, (sum(r.instCount  * ((hour(timediff(case when r.terminatedDate is null then "
			+ ":now else r.terminatedDate end, r.createdDate)) + (minute(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/60) + "
			+ "(second(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/3600)) - (r.runningHours/3600)) *  (select cI.cost from ResourceCostMetaData "
			+ "cI where cI.size = r.instSize) )  + sum(1 * (hour(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate)) + "
			+ "(minute(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/60) + (second(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end "
			+ ", r.createdDate))/3600)) * (select cElb.cost from ResourceCostMetaData cElb where cElb.type = r.instanceType) )) as tc "
			+ "from Resource r, Application a, Stack s, User u where a.id = r.appId and s.id = a.stackId and u.id = s.userId and "
			+ "(r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = "
			+ "'AWS::ElasticLoadBalancingV2::LoadBalancer' or r.resourceType = 'AWS::EC2::Instance') group by (u.id) order by(tc) desc";
	
	public static final String SELECT_INSTANCE_COST_BY_STACK = "select s.name, (sum(r.instCount * ((hour(timediff(case when r.terminatedDate is null then "
			+ ":now else r.terminatedDate end, r.createdDate)) + (minute(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/60) + "
			+ "(second(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/3600)) - (r.runningHours/3600)) *  (select cI.cost from ResourceCostMetaData "
			+ "cI where cI.size = r.instSize) )  + sum(1 * (hour(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate)) + "
			+ "(minute(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/60) + (second(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end "
			+ ", r.createdDate))/3600)) * (select cElb.cost from ResourceCostMetaData cElb where cElb.type = r.instanceType) )) as stackCost "
			+ "from Resource r, Application a, Stack s where s.id = a.stackId and a.id = r.appId and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = "
			+ "'AWS::ElasticLoadBalancingV2::LoadBalancer' or r.resourceType = 'AWS::EC2::Instance') group By(s.name) order by stackCost desc";
	
	public static final String SELECT_RUNNING_INSTANCE_COUNT_BY_APPLICATION = "select a.appId, sum(r.instCount) from Resource r , Application a, Stack s, User u"
			+ " where a.id = r.appId and s.id = a.stackId and u.id = s.userId and r.status != 'TERMINATED' and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = 'AWS::EC2::Instance') group by(a.appId)";

	public static final String SELECT_MY_RUNNING_INSTANCE_COUNT_BY_APPLICATION = "select a.appId, sum(r.instCount) from Resource r , Application a, Stack s, User u"
			+ " where a.id = r.appId and s.id = a.stackId and u.id = s.userId and u.id = :uId and r.status != 'TERMINATED' and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = 'AWS::EC2::Instance') group by(a.appId)";
	
	public static final String SELECT_TERMINATED_INSTANCE_COUNT_BY_APPLICATION = "select a.appId, sum(r.instCount) from Resource r , Application a, Stack s, User u"
			+ " where a.id = r.appId and s.id = a.stackId and u.id = s.userId and r.status = 'TERMINATED' and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = 'AWS::EC2::Instance') group by(a.appId)";
	
	public static final String SELECT_MY_TERMINATED_INSTANCE_COUNT_BY_APPLICATION = "select a.appId, sum(r.instCount) from Resource r , Application a, Stack s, User u"
			+ " where a.id = r.appId and s.id = a.stackId and u.id = s.userId and u.id = :uId and r.status = 'TERMINATED' and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = 'AWS::EC2::Instance') group by(a.appId)";

	public static final String SELECT_INSTANCE_COST_BY_MONTH_BETWEEN="select month(r.createdDate),(sum(r.instCount * ((hour(timediff(case when r.terminatedDate is null then "
				+ ":now else r.terminatedDate end, r.createdDate)) + (minute(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/60) + "
				+ "(second(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/3600)) - (r.runningHours/3600)) *  (select cI.cost from ResourceCostMetaData "
				+ "cI where cI.size = r.instSize) )  + sum(1 * (hour(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate)) + "
				+ "(minute(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/60) + (second(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end "
				+ ", r.createdDate))/3600)) * (select cElb.cost from ResourceCostMetaData cElb where cElb.type = r.instanceType) )) "
				+ "from Resource r, Application a where a.id = r.appId and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = "
				+ "'AWS::ElasticLoadBalancingV2::LoadBalancer' or r.resourceType = 'AWS::EC2::Instance') and r.createdDate between :beginDate AND :endDate group by (month(r.createdDate))";
	
	public static final String SELECT_INSTANCE_COST_BY_MONTH= "select month(r.createdDate),(sum(r.instCount * ((hour(timediff(case when r.terminatedDate is null then "
		+ ":now else r.terminatedDate end, r.createdDate)) + (minute(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/60) + "
		+ "(second(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/3600)) - (r.runningHours/3600)) *  (select cI.cost from ResourceCostMetaData "
		+ "cI where cI.size = r.instSize) )  + sum(1 * (hour(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate)) + "
		+ "(minute(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/60) + (second(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end "
		+ ", r.createdDate))/3600)) * (select cElb.cost from ResourceCostMetaData cElb where cElb.type = r.instanceType) )) "
		+ "from Resource r, Application a where a.id = r.appId and (r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = "
		+ "'AWS::ElasticLoadBalancingV2::LoadBalancer' or r.resourceType = 'AWS::EC2::Instance')  group by (month(r.createdDate))";
	
	public static final String SELECT_GROUP_COST_BY_USER = "select g.name, (sum(r.instCount  * ((hour(timediff(case when r.terminatedDate is null then "
			+ ":now else r.terminatedDate end, r.createdDate)) + (minute(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/60) + "
			+ "(second(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/3600)) - (r.runningHours/3600)) *  (select cI.cost from ResourceCostMetaData "
			+ "cI where cI.size = r.instSize) )  + sum(1 * (hour(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate)) + "
			+ "(minute(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end, r.createdDate))/60) + (second(timediff(case when r.terminatedDate is null then :now else r.terminatedDate end "
			+ ", r.createdDate))/3600)) * (select cElb.cost from ResourceCostMetaData cElb where cElb.type = r.instanceType) )) as tc "
			+ "from Resource r, Application a, Stack s, User u, Group g where a.id = r.appId and s.id = a.stackId and u.id = s.userId and u.groupId = g.id and "
			+ "(r.resourceType = 'AWS::AutoScaling::AutoScalingGroup' or r.resourceType = "
			+ "'AWS::ElasticLoadBalancingV2::LoadBalancer' or r.resourceType = 'AWS::EC2::Instance') group by (g.id) order by(tc) desc";
	
	public static final String SELECT_INSTANCE_SIZE="select rcmd.size from ResourceCostMetaData rcmd where rcmd.type='EC2_INSTANCE'";
	
	public static final String SELECT_EC2_INSTANCE = "select r from Resource r, Application a, Stack s where s.id = a.stackId and a.id = r.appId "
			+ "and r.resourceType = 'AWS::EC2::Instance' and s.id = :stackId and a.id=:appId and r.instanceId = :instanceId";
	
		
	Page<Resource> findAll(Pageable pageable);
	
	@Query("SELECT r FROM Resource r WHERE r.appId = :appId")
    public List<Resource> getApplicationResources(@Param("appId") String appId);
	
	@Query(SELECT_MY_RUNNING_INSTANCE_COUNT_BY_APPLICATION)
    public  List<Object[]> getMyRunningResourceCountByAppName(@Param("uId") int uId);
    
    @Query(SELECT_MY_TERMINATED_INSTANCE_COUNT_BY_APPLICATION)
    public  List<Object[]> getMyTerminatedResourceCountByAppName(@Param("uId") int uId);
    
	@Query(SELECT_RUNNING_INSTANCE_COUNT_BY_APPLICATION)
    public  List<Object[]> getRunningResourceCountByAppName();
    
    @Query(SELECT_TERMINATED_INSTANCE_COUNT_BY_APPLICATION)
    public  List<Object[]> getTerminatedResourceCountByAppName();
	
	@Query("SELECT r FROM Resource r WHERE r.createdDate < :fromDate AND (r.terminatedDate > :toDate OR r.terminatedDate is null)")
    public List<Resource> getLiveResourcesBetween(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);
	
	@Query("SELECT r FROM Resource r, Application a, Stack s, User u  WHERE s.userId = u.id and a.stackId = s.id and r.appId = a.id and u.id = :userId")
    public List<Resource> getAllResourcesForUser(@Param("userId") int userId);
	
	@Query("SELECT r FROM Stack s, Application a, Resource r  WHERE s.id = :stackId and a.stackId = s.id and r.appId = a.id")
    public List<Resource> getAllResourcesForStack(@Param("stackId") String stackId);
	
	@Query("SELECT r FROM Stack s, Application a, Resource r  WHERE s.id = :stackId and a.stackId = s.id and r.appId = a.id and r.terminatedDate is not null")
    public List<Resource> getAllLiveResourcesForStack(@Param("stackId") String stackId);
	
	@Query(SELECT_INSTANCE_COUNT_BY_MONTH)
	public List<Object[]> getInstanceCountByMonth();
	
	@Query(SELECT_INSTANCE_COUNT_BY_MONTH_BETWEEN)
	public List<Object[]> getInstanceCountByMonthBetween(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	@Query(SELECT_INSTANCE_COUNT_PER_USER_BY_MONTH)
	public List<Object[]> getInstanceCountPerUserByMonth(@Param("uId") int userId);
	
	@Query(SELECT_INSTANCE_COUNT_BETWEEN_PER_USER_BY_MONTH)
	public List<Object[]> getInstanceCountBetweenPerUserByMonth(@Param("uId") int userId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	/*@Query(SELECT_INSTANCE_COST_BY_MONTH)
	public List<Object[]> getInstanceCostByMonth(@Param("now") Date now);*/

	@Query(SELECT_INSTANCE_COST_BY_USER)
	public List<Object[]> getTopInstanceCostByUser(@Param("now") Date now);

	@Query("SELECT r FROM Resource r, Application a, Stack s  WHERE a.stackId = s.id and r.appId = a.id")
	public List<Resource> getAllResources();

	/*@Query(SELECT_INSTANCE_COST_PER_USER_BY_MONTH)
	public List<Object[]> getInstanceCostPerUserByMonth(@Param("now") Date now, @Param("uId") int userId);*/
	
	
	@Query(SELECT_INSTANCE_COST_BY_MONTH_BETWEEN)
	public List<Objects[]> getInstanceCostByMonthBetween(@Param("beginDate") Date startDate, @Param("endDate") Date endDate,@Param("now") Date now);
	
	@Query(SELECT_INSTANCE_COST_BY_MONTH)
	public List<Objects[]> getInstanceCostByMonth(@Param("now") Date now);
	
	/*@Query(SELECT_INSTANCE_COST_PER_USER_BY_MONTH)
	public List<Object[]> getInstanceCostPerUserByMonth(@Param("beginDate") Date beginDate,@Param("endDate") Date endDate,@Param("now") Date now, @Param("uId") int 
userId);*/
	
	@Query(SELECT_INSTANCE_SIZE)
	public List<String> getInstanceSize();

	@Query(SELECT_EC2_INSTANCE)
	Resource getEC2Instance(@Param("stackId") String stackId, @Param("appId") String appId, @Param("instanceId") String instanceId);
	
	@Query(SELECT_INSTANCE_COST_BY_STACK)
	List<Object[]> getTopInstanceCostByStack(@Param("now") Date now);
	
	@Query(SELECT_GROUP_COST_BY_USER)
	List<Object[]> getTopUserCostByGroup(@Param("now") Date now);
}
