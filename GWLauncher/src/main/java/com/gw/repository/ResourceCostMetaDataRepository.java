package com.gw.repository;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.gw.model.ResourceCostMetaData;

@Transactional
public interface ResourceCostMetaDataRepository extends CrudRepository<ResourceCostMetaData, Long> {
	
	Page<ResourceCostMetaData> findAll(Pageable pageable);

}
