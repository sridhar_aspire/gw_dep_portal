package com.gw.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.gw.model.Role;

@Transactional
public interface RoleRepository extends CrudRepository<Role, Integer> {

	@Query("SELECT r FROM Role r WHERE r.name = 'USER'")
	Role getDefaultRole();
	
	@Query("SELECT r FROM Role r WHERE r.name = 'ADMIN'")
	Role getAdminRole();
	
}
