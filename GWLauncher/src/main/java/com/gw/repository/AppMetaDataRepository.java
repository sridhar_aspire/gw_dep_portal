package com.gw.repository;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.gw.model.AppMetaData;



@Transactional
public interface AppMetaDataRepository extends CrudRepository<AppMetaData, String> {
			
	Page<AppMetaData> findAll(Pageable pageable);
	
}
