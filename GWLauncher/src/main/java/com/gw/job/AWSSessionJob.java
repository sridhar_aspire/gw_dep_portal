package com.gw.job;

import org.springframework.stereotype.Component;

import com.gw.util.aws.AwsUtil;
import com.gw.util.aws.CloudWatchUtil;
import com.gw.util.aws.EC2Util;
import com.gw.util.aws.S3Util;
import com.gw.util.aws.StackUtil;

@Component
public class AWSSessionJob {

	//@Scheduled(cron = "0 */45 * * * *")
	public void renewAWSSession() {
		AwsUtil.getInstance().initSession();
		EC2Util.getInstance().init();
		StackUtil.getInstance().init();
		S3Util.getInstance().init();
		CloudWatchUtil.getInstance().init();
	}
}
