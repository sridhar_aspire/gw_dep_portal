package com.gw.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gw.model.AppMetaData;
import com.gw.repository.AppMetaDataRepository;
import com.gw.util.AppConstants;
import com.gw.util.jenkins.JenkinsUtility;

@Component
public class BuildJob {

    private static final Logger logger = Logger.getLogger(BuildJob.class.getName());
    
    @Autowired
	AppMetaDataRepository appMetaDataDao;
    
    @Scheduled(cron = "0 */30 * * * *")
    public void getLatestBuilds() {
    	Map<String, ArrayList<String>> buildMap = JenkinsUtility.getInstance().getBuilds(AppConstants.DEP_UCD);
    	logger.log(Level.INFO, "The builds across application " + buildMap.toString());
    	
    	HashMap<String, AppMetaData> appMetadataList = new HashMap<>();
		Iterator<AppMetaData> iter = appMetaDataDao.findAll().iterator();
		while(iter.hasNext()) {
			AppMetaData obj = iter.next();
			appMetadataList.put(obj.getId(), obj);
		}
    	for (String appName : buildMap.keySet()) {
    		AppMetaData appMetaData = appMetadataList.get(appName);
    		appMetaData.setBuildInfo(buildMap.get(appName));
    		appMetaDataDao.save(appMetaData);
		}
    }
}
