package com.gw.job;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.amazonaws.services.ec2.model.InstanceStateName;
import com.gw.controller.ServiceController;
import com.gw.model.Resource;
import com.gw.model.Stack;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.StackRepository;
import com.gw.repository.UserRepository;
import com.gw.util.aws.CloudWatchUtil;
import com.gw.workflow.StopInstance;

@Component
public class AutoStopJob {

	private static final Logger logger = Logger.getLogger(AutoStopJob.class.getName());

	private int INSTANCE_STATUS_CHECK_DURATION = 2;
	
	@Autowired
	StackRepository stackRepo;
	
	@Autowired
	ApplicationRepository appRepo;
	
	@Autowired
	ResourceRepository resourceRepo;
	
	@Autowired
	UserRepository userRepo;

	@Autowired
	ServiceController serviceController;

	@Scheduled(cron = "0 0 1 * * *")
	public void reportCurrentInstanceStatus() {
		List<Stack> stackList = new ArrayList<>();
		Iterator<Stack> iter = stackRepo.getAllLiveStacks().iterator();
		while(iter.hasNext()) {
			stackList.add(iter.next());
		}
		for (Stack stack : stackList) {
			Date currentDate = Calendar.getInstance().getTime();
			int actualUseDays = (int) (((currentDate.getTime()) - (stack.getUpdatedDate().getTime()))
					/ (1000 * 60 * 60 * 24));
			if (actualUseDays > INSTANCE_STATUS_CHECK_DURATION) {
				// getInstances
				List<Resource> resources = resourceRepo.getAllResourcesForStack(stack.getId());
				for (Resource resource : resources) {
					if (resource.getInstanceId() != null && !InstanceStateName.Stopped.toString().equals(resource.getStatus())) {
						boolean isUnused = CloudWatchUtil.getInstance().isInstanceUnused(resource.getInstanceId(), INSTANCE_STATUS_CHECK_DURATION);
						if (isUnused) {
							resource.setUnused(isUnused);
							StopInstance instance = new StopInstance(resource.getInstanceId(), resourceRepo, stack.getId(), resource.getAppId());
							instance.run();
							logger.log(Level.INFO, "EC2 Instance (" +resource.getInstanceId()+ ") Stopped ");
						}
					}
				}
			}
		}
	}
}
