package com.gw.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gw.controller.ServiceController;
import com.gw.dao.StackDAO;
import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.model.status.ApplicationStatus;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.StackRepository;
import com.gw.repository.UserRepository;
import com.gw.workflow.AutoShutDownReminder;
import com.gw.workflow.TerminateStackWorkFlow;

@Component
public class AutoShutDownJob {

	private static final Logger logger = Logger.getLogger(AutoShutDownJob.class.getName());

	@Autowired
	StackRepository stackRepo;
	
	@Autowired
	ApplicationRepository appRepo;
	
	@Autowired
	ResourceRepository resourceRepo;
	
	@Autowired
	UserRepository userRepo;

	@Autowired
	ServiceController serviceController;
	
	private static TerminateStackWorkFlow terminateStackWorkFlow = new TerminateStackWorkFlow();
	private static StackDAO stackDAO = new StackDAO();

	@Scheduled(cron = "0 0 0 1 * *")
	public void reportCurrentInstanceStatus() {
		List<Stack> stackList = new ArrayList<>();
		Iterator<Stack> iter = stackRepo.getAllLiveStacks().iterator();
		while(iter.hasNext()) {
			stackList.add(iter.next());
		}
		for (Stack stack : stackList) {
			if (stack.isAutoShutdown()) {
				Date currentDate = new Date();
				int actualUseDays = (int) (((currentDate.getTime()) - (stack.getCreatedDate().getTime()))
						/ (1000 * 60 * 60 * 24));
				if (actualUseDays > stack.getPlannedUseDays()) {
					stackDAO.updateStackStatus(stackRepo, appRepo, resourceRepo, stack, ApplicationStatus.TERMINATION_STARTED.getStatus(), null);
					terminateStackWorkFlow.initWorkFlow(stack, stackRepo, appRepo, resourceRepo, userRepo.findOne(stack.getUserId()), true).startWorkflow();
					logger.log(Level.INFO, "The stack to be deleted :" + stack.getName());
				} else if (stack.getPlannedUseDays() - actualUseDays <= 5) {
					try {
						AutoShutDownReminder auto = new AutoShutDownReminder();

						int userId = stack.getUserId();
						User user = userRepo.findOne(userId);
						// To Do - Send the user details
						auto.initWorkFlow(stack, user).startWorkflow();
						logger.log(Level.INFO,
								"Number of days stack is operational: " + (stack.getPlannedUseDays() - actualUseDays));
					} catch (Exception e) {
						logger.log(Level.INFO, "Auto shut down Reminder not sent for :" + stack.getName());
						e.printStackTrace();
					}
				}
			}
		}
	}
}
