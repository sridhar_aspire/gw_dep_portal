package com.gw.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gw.exception.InvalidOperationException;
import com.gw.exception.NotAuthorizedException;

@RestController
@RequestMapping("/open")
public class OpenServiceController extends BaseController {
	
	@RequestMapping(value = "/login_url", method = RequestMethod.GET, produces = "application/text")
    public String getLoginUrl(HttpServletRequest request) throws NotAuthorizedException, InvalidOperationException{
		String authURL = getRedirectURL(request);
		return authURL;
	}
}


