package com.gw.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gw.auth.Permissions;
import com.gw.exception.DuplicateDataFoundException;
import com.gw.exception.InvalidOperationException;
import com.gw.exception.NotAuthorizedException;
import com.gw.model.Group;
import com.gw.model.Role;
import com.gw.model.Stack;
import com.gw.model.User;


@RestController
public class UserController extends BaseController {
	
	private static ArrayList<Role> roleList = new ArrayList<Role>();
	private static Logger logger = Logger.getLogger(UserController.class.getName());
	
	@RequestMapping(value = "/user", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<User> user(Principal user) throws NotAuthorizedException {
		logger.log(Level.INFO, "GET : /user");
		User user1 = getCurrentUserDetails();
		return new ResponseEntity<User>(user1, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Boolean> user() throws NotAuthorizedException {
		logger.log(Level.INFO, "GET : /logout");
		boolean invalidated = invalidateSession();
		return new ResponseEntity<Boolean>(new Boolean(invalidated), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/roles", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Role>> getAllRoles() throws NotAuthorizedException {
		logger.log(Level.INFO, "GET : /roles");
		enforcePermission(Permissions.ROLE_LISTING);
		if(roleList.isEmpty()){
			Iterator<Role> iter = roleRepo.findAll().iterator();
			while (iter.hasNext()) {
				Role role = iter.next();
				role.setPermissions(null);
				roleList.add(role);
			}
		}
		return new ResponseEntity<List<Role>>(roleList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/assign_stack_user", method = RequestMethod.GET, produces = "application/json")
	public boolean assignStackToUser(@RequestParam("stackId") String stackId, @RequestParam("userId") int userId) throws NotAuthorizedException, DuplicateDataFoundException {
		logger.log(Level.INFO, "GET : /assign_stack_user");
		enforcePermission(Permissions.MY_STACK_LISTING);
		Stack stack = stackRepo.findOne(stackId);
		if (stack != null) {
			stack.setUserId(userId);
			stackRepo.save(stack);
			return true;
		}
		return false;
	}
	
	@RequestMapping(value = "/shared_user", method = RequestMethod.GET, produces = "application/json")
	public List<User> getSharedUsersForStack(@RequestParam("stackId") String stackId) throws NotAuthorizedException {
		logger.log(Level.INFO, "GET : /shared_user");
		enforcePermission(Permissions.MY_STACK_LISTING);
//		User currentUser = getCurrentUserDetails();
//		boolean hasPermissionToShare = false;
//		Stack stack = null;
//		if (hasPermission(Permissions.ALL_STACK_LISTING)) {
//			stack = stackRepo.getStackById(stackId);
//		} else {
//			stack = stackRepo.getUserStackById(currentUser.getId(), stackId);
//		}
//		if (stack != null) {
//			hasPermissionToShare = true;
//		} else {
//			stack = stackRepo.getStackById(stackId);
//			List<User> sharedUsers = stackRepo.getSharedUsersForStack(stack.getId());
//			if (sharedUsers != null) {
//				for (User sharedUser : sharedUsers) {
//					if (currentUser.getId() == sharedUser.getId()) {
//						hasPermissionToShare = true;
//					}
//				}
//			}
//		}
//		if (!hasPermissionToShare) {
//			throw new NotAuthorizedException("You do not have access to this stack");
//		} else {
			List<User> sharedUsers = stackRepo.getSharedUsersForStack(stackId);
			return sharedUsers;
//		}
	}
	
	@RequestMapping(value = "/shared_user", method = RequestMethod.POST, produces = "application/json")
	public boolean shareStackToUser(@RequestParam("stackId") String stackId, @RequestParam("userId") int userId) throws NotAuthorizedException, DuplicateDataFoundException, InvalidOperationException {
		logger.log(Level.INFO, "POST : /shared_user");
		enforcePermission(Permissions.MY_STACK_LISTING);
		User currentUser = getCurrentUserDetails();
		boolean hasPermissionToShare = false;
		Stack stack = stackRepo.getUserStackById(currentUser.getId(), stackId);
		if (stack != null) {
			hasPermissionToShare = true;
		} else {
			stack = stackRepo.getStackById(stackId);
			Set<User> sharedUsers = stack.getSharedUsers();
			for (User sharedUser : sharedUsers) {
				if (currentUser.getId() == sharedUser.getId()) {
					hasPermissionToShare = true;
				}
			}
		}
		if (!hasPermissionToShare) {
			throw new NotAuthorizedException("You do not have access to this stack");
		} else {
			Set<User> sharedUsers = stack.getSharedUsers();
			for (User sharedUser : sharedUsers) {
				if (userId == sharedUser.getId()) {
					throw new InvalidOperationException("The stack is already shared to the specified user");
				}
			}
			User user = userRepo.findOne(userId);
			sharedUsers.add(user);
			stackRepo.save(stack);
			return true;
		}
	}
	
	@RequestMapping(value = "/shared_user", method = RequestMethod.DELETE, produces = "application/json")
	public boolean deleteSharedUserFromStack(@RequestParam("stackId") String stackId, @RequestParam("userId") int userId) throws NotAuthorizedException, DuplicateDataFoundException, InvalidOperationException {
		logger.log(Level.INFO, "DELETE : /shared_user");
		enforcePermission(Permissions.MY_STACK_LISTING);
		User currentUser = getCurrentUserDetails();
		boolean hasPermissionToShare = false;
		Stack stack = stackRepo.getUserStackById(currentUser.getId(), stackId);
		if (stack != null) {
			hasPermissionToShare = true;
		} else {
			stack = stackRepo.getStackById(stackId);
			Set<User> sharedUsers = stack.getSharedUsers();
			for (User sharedUser : sharedUsers) {
				if (currentUser.getId() == sharedUser.getId()) {
					hasPermissionToShare = true;
				}
			}
		}
		if (!hasPermissionToShare) {
			throw new NotAuthorizedException("No access to this stack");
		} else {
			Set<User> sharedUsers = stack.getSharedUsers();
			for (User sharedUser : sharedUsers) {
				if (userId == sharedUser.getId()) {
					sharedUsers.remove(sharedUser);
					stackRepo.save(stack);
					return true;
				}
			}
		}
		return false;
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET, produces = "application/json")
	public HashMap<Long, List<User> > getAllUser(@RequestParam(value = "pageNo") int pageNo) throws NotAuthorizedException  {
		logger.log(Level.INFO, "GET : /users");
		enforcePermission(Permissions.USER_LISTING);
		List<User> userList = null;
    	long userCount = 0;
    	HashMap<Long, List<User>> object = new HashMap<Long, List<User>>();
		 Page<User> user = userRepo.getAllLiveUsers(new PageRequest(pageNo-1, 5));
			userCount = user.getTotalElements();
			userList = user.getContent();
			object.put(userCount, userList);
			return object;
	}
	
	@RequestMapping(value = "/users/{userId}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<User> getUserById(@PathVariable("userId") int userId) throws NotAuthorizedException  {
		logger.log(Level.INFO, "GET : /users/{userId}");
		enforcePermission(Permissions.USER_LISTING);
		User user = userRepo.findOne(userId);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/groups/{userId}", method = RequestMethod.POST, produces = "application/json")
	public Group createGroup(@RequestBody Group group, @PathVariable int userId) throws NotAuthorizedException, DuplicateDataFoundException {
		logger.log(Level.INFO, "POST : /groups");
		enforcePermission(Permissions.USER_CREATE);
		Group groupByGroupName = groupRepo.findByGroupName(group.getName());
		if (groupByGroupName != null) {
			throw new DuplicateDataFoundException("Group already exists for this groupname");
		}
		group.setCreatedDate(new Date());
		group.setUpdatedDate(new Date());
		group.setCreatedBy(getCurrentUserDetails().getUserName());
		groupRepo.save(group);
		User user = userRepo.findOne(userId);
		if(user!= null){
			user.setGroup(group);
			userRepo.save(user);
			}
		
		return groupRepo.findOne(group.getId());
	}
	
	@RequestMapping(value = "/update_groups", method = RequestMethod.POST, produces = "application/json")
	public boolean updateGroup(@RequestBody Group group, @RequestParam("userId") int userId, @RequestParam("isAdded") int isAdded) throws NotAuthorizedException, DuplicateDataFoundException {
		logger.log(Level.INFO, "POST : /update_groups");
		enforcePermission(Permissions.USER_CREATE);
		Group dbGroup = groupRepo.findOne(group.getId());
		dbGroup.setName(group.getName());
		dbGroup.setAdmin(group.getAdmin());
		group.setUpdatedDate(new Date());
		group.setCreatedBy(getCurrentUserDetails().getUserName());
		groupRepo.save(group);
		User user = userRepo.findOne(userId);
		if(isAdded == 1){
			user.setGroup(group);
			userRepo.save(user);
		} else{
			user.setGroup(null);
			userRepo.save(user);
		}
		return true;
	}
	
	@RequestMapping(value = "/saveGroup", method = RequestMethod.POST, produces = "application/json")
	public Group saveGroup(@RequestBody Group group) throws NotAuthorizedException, DuplicateDataFoundException {
		logger.log(Level.INFO, "POST : /saveGroup");
		enforcePermission(Permissions.USER_CREATE);
		Group groupByGroupName = groupRepo.findByGroupName(group.getName());
		if (groupByGroupName != null) {
			throw new DuplicateDataFoundException("Group already exists for this groupname");
		}
		group.setCreatedDate(new Date());
		group.setUpdatedDate(new Date());
		group.setCreatedBy(getCurrentUserDetails().getUserName());
		groupRepo.save(group);
		
		return groupRepo.findOne(group.getId());
	}
	
	@RequestMapping(value = "/updateGroup", method = RequestMethod.POST, produces = "application/json")
	public Group updateGroup(@RequestBody Group group) throws NotAuthorizedException, DuplicateDataFoundException {
		logger.log(Level.INFO, "POST : /updateGroup");
		enforcePermission(Permissions.USER_CREATE);
		Group dbGroup = groupRepo.findOne(group.getId());
		dbGroup.setName(group.getName());
		dbGroup.setAdmin(group.getAdmin());
		group.setUpdatedDate(new Date());
		group.setCreatedBy(getCurrentUserDetails().getUserName());
		groupRepo.save(group);
		
		return groupRepo.findOne(group.getId());
	}
	
	@RequestMapping(value = "/added_user", method = RequestMethod.GET, produces = "application/json")
	public List<User> getAddedUsersForGroup(@RequestParam("groupId") String groupName) throws NotAuthorizedException {
		logger.log(Level.INFO, "GET : /added_user");
		enforcePermission(Permissions.USER_LISTING);
		Group group = groupRepo.findByGroupName(groupName);
		List<User> addedUsers = userRepo.getAddedUsersForGroup(group.getId());
		System.out.println(addedUsers);
		return addedUsers ;	
			
	}
	
	/*@RequestMapping(value = "/groups", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Group>> getAllGroups() throws NotAuthorizedException  {
		logger.log(Level.INFO, "GET : /groups");
		enforcePermission(Permissions.USER_LISTING);
		List<Group> groupList = (List<Group>) groupRepo.getAllGroups();
		return new ResponseEntity<List<Group>>(groupList, HttpStatus.OK);
	}*/
	
	@RequestMapping(value = "/groups", method = RequestMethod.GET, produces = "application/json")
	public HashMap<Long, List<Group> > getAllGroups(@RequestParam(value = "pageNo") int pageNo) throws NotAuthorizedException  {
		logger.log(Level.INFO, "GET : /groups");
		enforcePermission(Permissions.USER_LISTING);
		List<Group> groupList = null;
    	long groupCount = 0;
    	HashMap<Long, List<Group>> object = new HashMap<Long, List<Group>>();
		 Page<Group> group = groupRepo.getAllLiveGroups(new PageRequest(pageNo-1, 5));
			groupCount = group.getTotalElements();
			groupList = group.getContent();
			object.put(groupCount, groupList);
			return object;
	}
	
	@RequestMapping(value = "/sharedGroup", method = RequestMethod.GET, produces = "application/json")
	public List<Group> getAllSharedGroup(@RequestParam(value = "stackId") String stackId, @RequestParam(value = "isShared") boolean isShared) throws NotAuthorizedException  {
		enforcePermission(Permissions.USER_LISTING);
		List<Group> sharedGrpList;
		if(isShared){
			sharedGrpList = groupRepo.getAllSharedGroups(stackId);
		}else{
			sharedGrpList = groupRepo.getAllUnSharedGroups(stackId);
		}
		return sharedGrpList;
	}
	
	@RequestMapping(value = "/sharedGroup", method = RequestMethod.POST, produces = "application/json")
	public boolean shareStackToGroup(@RequestParam(value = "stackId") String stackId, @RequestParam(value = "grpId") int grpId) throws NotAuthorizedException  {
		enforcePermission(Permissions.USER_LISTING);
		Stack stack = stackRepo.getStackById(stackId);
		stack.getSharedGroup().add(groupRepo.findOne(grpId));
		stackRepo.save(stack);
		return true;
	}
	
	@RequestMapping(value = "/sharedGroup", method = RequestMethod.DELETE, produces = "application/json")
	public boolean removeStackFromGroup(@RequestParam(value = "stackId") String stackId, @RequestParam(value = "grpId") int grpId) throws NotAuthorizedException  {
		enforcePermission(Permissions.USER_LISTING);
		Stack stack = stackRepo.getStackById(stackId);
		stack.getSharedGroup().remove(groupRepo.findOne(grpId));
		stackRepo.save(stack);
		return true;
	}
	
	 @RequestMapping(value = "/groups/{groupId}", method = RequestMethod.GET, produces = "application/json")
		public ResponseEntity<Group> getGroupById(@PathVariable("groupId") int groupId) throws NotAuthorizedException  {
			logger.log(Level.INFO, "GET : /groups/{groupId}");
			enforcePermission(Permissions.USER_LISTING);
			Group group = groupRepo.findOne(groupId);
			return new ResponseEntity<Group>(group, HttpStatus.OK);
	}
	 
	 @RequestMapping(value = "/groups/{groupId}", method = RequestMethod.DELETE, produces = "application/json")
		public boolean deleteUserGroup(@PathVariable("groupId") int groupId) throws NotAuthorizedException, InvalidOperationException {
			logger.log(Level.INFO, "DELETE : /users/{userId}");
			enforcePermission(Permissions.USER_CREATE);
			if (groupId == getCurrentUserDetails().getId()) {
				throw new InvalidOperationException("Cannot delete yourself");
			}
			Group group = groupRepo.findOne(groupId);
			group.setTerminatedDate(new Date());
			groupRepo.save(group);
			return true;
	}
	
	 @RequestMapping(value = "/searchedUsers/{serachtext}", method = RequestMethod.GET, produces = "application/json")
		public ResponseEntity<List<User>> searchedUsers(@PathVariable("serachtext") String serachtext) throws NotAuthorizedException  {
			logger.log(Level.INFO, "GET : /searchedUsers/{serachtext}");
			enforcePermission(Permissions.USER_LISTING);
			List<User> user = (List<User>) userRepo.findUserbySearch(serachtext);
			return new ResponseEntity<List<User>>(user, HttpStatus.OK);
	}
	 @RequestMapping(value = "/user_list", method = RequestMethod.GET, produces = "application/json")
		public ResponseEntity<List<User>> getUserList() throws NotAuthorizedException  {
			logger.log(Level.INFO, "GET : /user_list");
			enforcePermission(Permissions.MY_USER_LISTING);
			List<User> userList = (List<User>) userRepo.getAllUsers();
			return new ResponseEntity<List<User>>(userList, HttpStatus.OK);
		}
	 @RequestMapping(value = "/group_by_name/{groupName}", method = RequestMethod.GET, produces = "application/json")
		public Group getGroupByName(@PathVariable("groupName") String groupName) throws NotAuthorizedException  {
			logger.log(Level.INFO, "GET : /group_by_name");
			enforcePermission(Permissions.USER_LISTING);
			Group group = groupRepo.findByGroupName(groupName);
			return group ;
		}
	 @RequestMapping(value = "/availableUsers", method = RequestMethod.GET, produces = "application/json")
		public HashMap<Long, List<User> > getAvailableUsers(@RequestParam(value = "pageNo") int pageNo) throws NotAuthorizedException  {
			logger.log(Level.INFO, "GET : /availableUsers");
			enforcePermission(Permissions.USER_LISTING);
			List<User> userList = null;
	    	long userCount = 0;
	    	HashMap<Long, List<User>> object = new HashMap<Long, List<User>>();
			 Page<User> user = userRepo.getAvailableUsers(new PageRequest(pageNo-1, 5));
				userCount = user.getTotalElements();
				userList = user.getContent();
				object.put(userCount, userList);
				return object;
		}
	 @RequestMapping(value = "/addedUsers", method = RequestMethod.GET, produces = "application/json")
		public HashMap<Long, List<User> > getAddedUsers(@RequestParam(value = "pageNo") int pageNo, @RequestParam(value = "groupId") int groupId) throws NotAuthorizedException  {
			logger.log(Level.INFO, "GET : /addedUsers");
			enforcePermission(Permissions.USER_LISTING);
			List<User> userList = null;
	    	long userCount = 0;
	    	HashMap<Long, List<User>> object = new HashMap<Long, List<User>>();
	    	if(groupId>0){
			    Page<User> user = userRepo.getAddedUsers(new PageRequest(pageNo-1, 5), groupId);
				userCount = user.getTotalElements();
				userList = user.getContent();
				object.put(userCount, userList);
	    	}
				return object;
		}
	 
}
