package com.gw.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.parser.ParseException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.cloudformation.model.StackStatus;
import com.gw.auth.Permissions;
import com.gw.dao.ResourceDAO;
import com.gw.dao.StackDAO;
import com.gw.exception.DuplicateDataFoundException;
import com.gw.exception.InvalidOperationException;
import com.gw.exception.NoDataFoundException;
import com.gw.exception.NotAuthorizedException;
import com.gw.exception.ServerException;
import com.gw.model.AppMetaData;
import com.gw.model.Application;
import com.gw.model.Resource;
import com.gw.model.ResourceCostMetaData;
import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.model.status.ApplicationStatus;
import com.gw.util.ValidationUtils;
import com.gw.util.aws.StackUtil;
import com.gw.workflow.LaunchAppWorkflow;
import com.gw.workflow.LaunchStackWorkFlow;
import com.gw.workflow.TerminateAppWorkFlow;
import com.gw.workflow.TerminateStackWorkFlow;
import com.gw.workflow.UpdateBuildWorkflow;
import com.gw.workflow.UpdateInstanceStatusWorkFlow;


@RestController
public class ServiceController extends BaseController {

	private static List<ResourceCostMetaData> instanceCostData = null;
	private static Logger logger = Logger.getLogger(ServiceController.class.getName());
	
    @RequestMapping(value = "/application_meta_data", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity<AppMetaData[]> getApplicationMetaDataList() throws NoDataFoundException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /application_meta_data");
    	enforcePermission(Permissions.METADATA_LISTING);
    	List<AppMetaData> appMetadataList = new ArrayList<>();
		Iterator<AppMetaData> iter = appMetaDataRepo.findAll().iterator();
		while(iter.hasNext()) {
			AppMetaData obj = iter.next();
			appMetadataList.add(obj);
		}
    	return new ResponseEntity<AppMetaData[]>(appMetadataList.toArray(new AppMetaData[appMetadataList.size()]), HttpStatus.OK);
    }

    @RequestMapping(value = "/application_meta_data/{appId}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity<AppMetaData> getApplicationMetaData(@PathVariable("appId") String appId) throws NoDataFoundException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /application_meta_data/" + appId);
    	enforcePermission(Permissions.METADATA_LISTING);
    	Iterator<AppMetaData> iter = appMetaDataRepo.findAll().iterator();
		while(iter.hasNext()) {
			AppMetaData appMetaData = iter.next();
			if (appMetaData.getId().equals(appId)) {
				return new ResponseEntity<AppMetaData>(appMetaData, HttpStatus.OK);
			}
		}
		throw new NoDataFoundException("Application id not found."); 
    }
    
    @RequestMapping(value = "/instance_cost_data", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity<List<ResourceCostMetaData>> getInstanceCostMetaData() throws NoDataFoundException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /instance_cost_data");
    	enforcePermission(Permissions.METADATA_LISTING);
    	if (instanceCostData == null) {
    		instanceCostData = new ArrayList<>();
    		Iterator<ResourceCostMetaData> iter = resourceCostMetaDataDao.findAll().iterator();
    		while(iter.hasNext()) {
    			ResourceCostMetaData obj = iter.next();
    			instanceCostData.add(obj);
    		}
    	}
    	return new ResponseEntity<List<ResourceCostMetaData>>(instanceCostData, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/stacks", method = RequestMethod.POST, consumes = "application/json")
    public Stack createStack(@RequestBody Stack stack) throws IOException, ParseException, ServerException, NotAuthorizedException, DuplicateDataFoundException {
    	logger.log(Level.INFO, "POST : /stacks");
    	enforcePermission(Permissions.STACK_CREATE);
		User user = getCurrentUserDetails();
    	try{
    	if(ValidationUtils.validateStackParams(stackRepo, roleRepo, settingRepo, stack, user)){
    		stack.setUserId(user.getId());
			stack.setCreatedDate(Calendar.getInstance().getTime());
			stack.setUpdatedDate(Calendar.getInstance().getTime());
    		stack.setStatus(StackStatus.CREATE_IN_PROGRESS.toString());
    		stackRepo.save(stack);
    		stack.getApplications().forEach(app -> {
    			app.setStackId(stack.getId());
    			app.setStatus(ApplicationStatus.INSTANCE_CREATION_STARTED.getStatus());
    			app.setCreatedDate(Calendar.getInstance().getTime());
    			app.setUpdatedDate(Calendar.getInstance().getTime());
    			appRepo.save(app);
    		});
    		LaunchStackWorkFlow launchStackWorkflow = new LaunchStackWorkFlow();
    		launchStackWorkflow.initWorkFlow(stack, appMetaDataRepo, stackRepo, appRepo, resourceRepo, settingRepo, user).startWorkflow();
    	}
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		throw ex;
    	}
		return stack;
    }
    
    @RequestMapping(value = "/stacks/{stackId}", method = RequestMethod.DELETE, produces = "application/json")
    public boolean deleteStack(@PathVariable("stackId") String stackId) throws IOException, ParseException, InvalidOperationException, NotAuthorizedException {
    	logger.log(Level.INFO, "DELETE : /stacks/" + stackId);
    	enforcePermission(Permissions.MY_STACK_TERMINATE);
		try {
			StackDAO stackDao = new StackDAO();
			Stack stack = stackDao.getStackDetail(stackId, stackRepo);
			if (!stack.getStatus().equals("TERMINATED")) {
				User user = getCurrentUserDetails();
				if (stack.getUserId() != user.getId()) {
					enforcePermission(Permissions.ALL_STACK_TERMINATE);
				}
				stackDao.updateStackStatus(stackRepo, appRepo, resourceRepo, stack,
						ApplicationStatus.TERMINATION_STARTED.getStatus(), null);
				TerminateStackWorkFlow terminateStackWorkFlow = new TerminateStackWorkFlow();
				terminateStackWorkFlow.initWorkFlow(stack, stackRepo, appRepo, resourceRepo, getCurrentUserDetails(), false)
						.startWorkflow();
			} else {
				throw new InvalidOperationException("Stack already terminated");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
    	return true;
    }
    
    @RequestMapping(value = "/stacks", method = RequestMethod.GET, produces = "application/json")
    public HashMap<Long, List<Stack> > getStackList(@RequestParam(required = false) String status, @RequestParam(value = "pageNo") int pageNo) throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /stacks?" + status);
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	List<Stack> stackList = null;
    	long stackCount = 0;
    	HashMap<Long, List<Stack>> object = new HashMap<Long, List<Stack>>();
    	User user = getCurrentUserDetails();
    	if (status == null) {
    		if(hasPermission(Permissions.ALL_STACK_LISTING)){
    			Page<Object[]> stackpage = stackRepo.getAllLiveStackMetaData(new PageRequest(pageNo - 1 , 5));
    			stackCount = stackpage.getTotalElements();
    			stackList = convertToStackList(stackpage.getContent());
    		}else{
    			Page<Object[]> stackpage = stackRepo.getMyLiveStackMetaData(new PageRequest(pageNo - 1 , 5), user.getId());
    			stackCount = stackpage.getTotalElements();
    			stackList = convertToStackList(stackpage.getContent());
    		}
    	} else if (status.equalsIgnoreCase("terminated")) {
    		if(hasPermission(Permissions.ALL_STACK_LISTING)){
    			Page<Object[]> stackpage = stackRepo.getAllTerminatedStackMetaData(new PageRequest(pageNo - 1 , 5));
    			stackCount = stackpage.getTotalElements();
    			stackList = convertToStackList(stackpage.getContent());
    		}else{
    			Page<Object[]> stackpage = stackRepo.getMyTerminatedStackMetaData(new PageRequest(pageNo - 1 , 5), user.getId());
    			stackCount = stackpage.getTotalElements();
    			stackList = convertToStackList(stackpage.getContent());
    		}
    	} else if (status.equalsIgnoreCase("shared")) {
    		Page<Object[]> stackpage = stackRepo.getSharedStackMetaData(new PageRequest(pageNo - 1 , 5), user.getId());
    		long stackGrpPageCount = 0;
    		stackList = convertToStackList(stackpage.getContent());
    		User gUser = userRepo.findOne(user.getId());
    		if(gUser.getGroupId() != null){
        		Page<Object[]> stackGrpPage = stackRepo.getSharedGroupStackMetaData(new PageRequest(pageNo - 1 , 5), gUser.getGroupId());
        		stackList.addAll(convertToStackList(stackGrpPage.getContent()));
        		stackGrpPageCount = stackGrpPage.getTotalElements();
    		}
    		stackCount = stackpage.getTotalElements() + stackGrpPageCount;
    	}
    	object.put(stackCount, stackList);
		return object;
    }
    
    private List<Stack> convertToStackList(List<Object[]> data) {
    	List<Stack> stackList = new ArrayList<Stack>();
    	if (data != null && data.size() > 0) {
			for (Object[] dataObj: data) {
				Stack stack = new Stack();
				stack.setId(dataObj[0].toString());
				stack.setName(dataObj[1].toString());
				stack.setStatus(dataObj[2].toString());
				stack.setUserId((Integer) dataObj[3]);
				stack.setCreatedDate((Date) dataObj[4]);
				stack.setUsername(dataObj[5].toString());
				if(dataObj.length > 6){
					stack.setSharedToGroup((int)dataObj[6]);
				}
				stackList.add(stack);
			}
		}
		return stackList;
	}

	@RequestMapping(value = "/stacks/{stackId}", method = RequestMethod.GET, produces = "application/json")
    public Stack getStackDetails(@PathVariable("stackId") String stackId) throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /stacks/"+stackId);
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	StackDAO stackDao = new StackDAO();
    	Stack stack = stackDao.getStackDetail(stackId, stackRepo);
    	return stack;
    }
    
    @RequestMapping(value = "/applications", method = RequestMethod.POST, produces = "application/json")
    public Stack launchApplication(@RequestParam("stack_id") String stackId, @RequestBody Application application) throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /applications?stack_id="+stackId);
    	enforcePermission(Permissions.MY_STACK_MODIFY);
    	User user = getCurrentUserDetails();
    	Stack stack = stackRepo.findOne(stackId);
    	try {
    		if(stack.getUserId() != user.getId()){
            	enforcePermission(Permissions.ALL_STACK_MODIFY);
        	}
	    	application.setStackId(stackId);
	    	application.setCreatedDate(Calendar.getInstance().getTime());
	    	application.setUpdatedDate(Calendar.getInstance().getTime());
	    	application.setStatus(ApplicationStatus.INSTANCE_CREATION_STARTED.getStatus());
	    	appRepo.save(application);
	    	stack.setUpdatedDate(Calendar.getInstance().getTime());
	    	stack.setStatus(StackStatus.UPDATE_IN_PROGRESS.toString());
	    	stackRepo.save(stack);
	    	if(stack.getAwsStackid() != null){
	    		LaunchAppWorkflow launchAppWorkFlow = new LaunchAppWorkflow();
	    		launchAppWorkFlow.initWorkFlow(stack, application, appMetaDataRepo, stackRepo, appRepo, resourceRepo, settingRepo, user).startWorkflow();
	    	}else{
	    		//TODO If stack creation failed at initial time itself
	    		/*List<Application> tmpList = stack.getApplications();
	    		stack.getApplications().removeAll(tmpList);
	    		stack.getApplications().add(application);
	    		LaunchStackWorkFlow launchStackWorkflow = new LaunchStackWorkFlow();
	    		launchStackWorkflow.initWorkFlow(stack, appMetaDataRepo, stackRepo, appRepo, resourceRepo, user).startWorkflow();*/
	    	}
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		throw ex;
    	}
    	return stack;
    }
    
    @RequestMapping(value = "/applications/{appId}", method = RequestMethod.DELETE, produces = "application/json")
    public Stack deleteApplication(@PathVariable("appId") String appId) throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "DELETE : /applications/"+appId);
    	User user = getCurrentUserDetails();
    	Application application = appRepo.findOne(appId);
    	Stack stack = stackRepo.findOne(application.getStackId());
    	try {
    		if(stack.getUserId() != user.getId()){
            	enforcePermission(Permissions.ALL_STACK_MODIFY);
        	}else{
            	enforcePermission(Permissions.MY_STACK_MODIFY);
        	}
    		/*application.getInstances().forEach(res ->{
    			res.setStatus(ApplicationStatus.TERMINATION_STARTED.toString());
    			res.setUpdatedDate(Calendar.getInstance().getTime());
    			resourceRepo.save(res);
    		});*/
	    	application.setUpdatedDate(Calendar.getInstance().getTime());
	    	application.setStatus(ApplicationStatus.TERMINATION_STARTED.getStatus());
	    	appRepo.save(application);
	    	stack.setUpdatedDate(Calendar.getInstance().getTime());
	    	stack.setStatus(StackStatus.UPDATE_IN_PROGRESS.toString());
	    	stackRepo.save(stack);
    		TerminateAppWorkFlow terminateAppWorkFlow = new TerminateAppWorkFlow();
    		terminateAppWorkFlow.initWorkFlow(stack, application, stackRepo, appRepo, resourceRepo).startWorkflow();
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		throw ex;
    	}
    	return stack;
    }
    
    // TODO : Check and delete this API
    @RequestMapping(value = "/stack_status/{stackId}", method = RequestMethod.GET, produces = "application/json")
    @Deprecated
    public boolean getApplicationLaunchStatus(@PathVariable("stackId") String stackId) throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /stack_status/"+stackId);
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	try {
    		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    		StackDAO stackDao = new StackDAO();
	    	Stack stack = stackDao.getStackDetail(stackId, stackRepo);
	    	User user = getCurrentUserDetails();
    		if (stack.getUserId() != user.getId()) {
    			enforcePermission(Permissions.ALL_STACK_LISTING);
    		}
	    	UpdateInstanceStatusWorkFlow instanceStatusWorkFlow = new UpdateInstanceStatusWorkFlow();
	    	instanceStatusWorkFlow.initWorkFlow(stack, appMetaDataRepo, stackRepo, appRepo, resourceRepo, userDetails.getUsername()).startWorkflow();
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		throw ex;
    	}
    	return true;
    }

    @RequestMapping(value = "/resources", method = RequestMethod.GET, produces = "application/json")
    public List<Resource> getResourceList(
    		@RequestParam(value="stack", required=false) String stackId, 
    		@RequestParam(value="app", required=false) String appId) throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /resources?stack="+stackId+"&app="+appId);
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	List<Resource> resourceList;
    	if (stackId == null && appId == null) {
	    	if(hasPermission(Permissions.COST_GROUP_BY_USER)){
	    		resourceList = resourceRepo.getAllResources();
	    	}else{
	    		resourceList = resourceRepo.getAllResourcesForUser(getCurrentUserDetails().getId());
	    	}
    	} else {
    		resourceList = resourceRepo.getAllResourcesForStack(stackId);
    		resourceList = StackUtil.filterInstances(resourceList);
    		if (appId != null) {
    			List<Resource> filteredResourceList = new ArrayList<>();
    			for (Resource res : resourceList) {
    				if (res.getAppId().equals(appId)) {
    					filteredResourceList.add(res);
    				}
    			}
    			return filteredResourceList;
    		}
    	}
		return resourceList;
    }
    
    @RequestMapping(value = "/resources/{stackId}", method = RequestMethod.GET, produces = "application/json")
    public Stack getStackResourceList(@PathVariable("stackId") String stackId) throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /resources/" + stackId);
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	Stack resourceList = stackRepo.getStackResourcesById(stackId);
		return resourceList;
    }
    
    @RequestMapping(value="/updateBuild", method = RequestMethod.POST, consumes = "application/json")
    public boolean updateBuild(@RequestBody Application application)throws IOException, ParseException, NotAuthorizedException{
    	logger.log(Level.INFO, "POST : /updateBuild");
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	Stack stack = stackRepo.findOne(application.getStackId());
    	UpdateBuildWorkflow ubw = new UpdateBuildWorkflow();
    	ubw.initWorkflow(appRepo, appMetaDataRepo, application, stack);
    	ubw.startWorkflow();
    	return true;
    }

    @RequestMapping(value="/updateStacks", method = RequestMethod.PUT, consumes = "application/json")
    public boolean updateStack(@RequestBody Stack stack)throws IOException, ParseException, NotAuthorizedException{
    	logger.log(Level.INFO, "PUT : /updateStacks");
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	stack.setUpdatedDate(new Date());
    	stackRepo.save(stack);
    	return true;
    }
    
    @RequestMapping(value = "/stack_count_by_user", method = RequestMethod.GET, produces = "application/json")
    public LinkedHashMap<String, Long> getStackCountByUser() throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /stack_count_by_user");
    	enforcePermission(Permissions.ALL_STACK_LISTING);
    	List<Object[]> results = stackRepo.getTopStackCountByUser(new PageRequest(0, 10)).getContent();
    	LinkedHashMap<String, Long> stackCountByUser = new LinkedHashMap<>();
    	for (int i = 0; i < results.size(); i++) {
    		Object[] arr = results.get(i);
    		String user = (String) arr[0];
    		Long count = (Long) arr[1];
    		stackCountByUser.put(user, count);
    	}
		return stackCountByUser;
    }
    
    @RequestMapping(value = "/instance_cost_by_user", method = RequestMethod.GET, produces = "application/json")
     public LinkedHashMap<String, Double> getTopInstanceCostByUser() throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /instance_count_by_user");
    	enforcePermission(Permissions.ALL_STACK_LISTING);
     	List<Object[]> results = resourceRepo.getTopInstanceCostByUser(Calendar.getInstance().getTime());
     	results = results.size() > 10 ? results.subList(0, 9) : results;
     	LinkedHashMap<String, Double> instCostByUser = new LinkedHashMap<>();
     	for (int i = 0; i < results.size(); i++) {
     		Object[] arr = results.get(i);
     		String user = (String) arr[0];
     		Double cost = (Double) arr[1];
     		instCostByUser.put(user, cost);
     	}
 		return instCostByUser;
     }
    
    @RequestMapping(value = "/running_resource_count_by_app", method = RequestMethod.GET, produces = "application/json")
    public LinkedHashMap<String, Long> getRunningResourceCountByApp() throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /running_resource_count_by_app");
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	ResourceDAO resourceDao = new ResourceDAO();
    	LinkedHashMap<String, Long> resCountByApp;
    	if(hasPermission(Permissions.ALL_STACK_LISTING)){
    		resCountByApp = resourceDao.getRunningResourceCountByApp(resourceRepo, 0);
    	}else{
    		resCountByApp = resourceDao.getRunningResourceCountByApp(resourceRepo, getCurrentUserDetails().getId());
    	}
		return resCountByApp;
    }
    
    @RequestMapping(value = "/terminated_resource_count_by_app", method = RequestMethod.GET, produces = "application/json")
    public LinkedHashMap<String, Long> getTerminatedResourceCountByApp() throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /terminated_resource_count_by_app");
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	ResourceDAO resourceDao = new ResourceDAO();
    	LinkedHashMap<String, Long> resCountByApp ;
    	if(hasPermission(Permissions.ALL_STACK_LISTING)){
    		resCountByApp = resourceDao.getTerminatedResourceCountByApp(resourceRepo, 0);
    	}else{
    		resCountByApp = resourceDao.getTerminatedResourceCountByApp(resourceRepo, getCurrentUserDetails().getId());
    	}
		return resCountByApp;
    }
    
    @RequestMapping(value = "/resource_count_trend", method = RequestMethod.GET, produces = "application/json")
    public LinkedHashMap<String, Long> getResourceCountTrend(@RequestParam(value="start_date", required=false) String startDate, @RequestParam(value="end_date", required=false) String endDate) throws IOException, ParseException, NotAuthorizedException, java.text.ParseException {
    	logger.log(Level.INFO, "GET : /resource_count_trend");
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	ResourceDAO resourceDAO = new ResourceDAO();
    	LinkedHashMap<String, Long> resCount;
    	if (startDate != null && endDate != null) {
        	Date beginDate = dtFormat.parse(startDate);
        	Date lastDate = dtFormat.parse(endDate);
        	if(hasPermission(Permissions.ALL_STACK_LISTING)){
	    		resCount = resourceDAO.getResourceTrend(resourceRepo, 0,  beginDate, lastDate);
	    	}else{
	    		resCount = resourceDAO.getResourceTrend(resourceRepo, getCurrentUserDetails().getId(),  beginDate, lastDate);
	    	}
    	} else {
	    	if(hasPermission(Permissions.ALL_STACK_LISTING)){
	    		resCount = resourceDAO.getResourceTrend(resourceRepo, 0, null, null);
	    	}else{
	    		resCount = resourceDAO.getResourceTrend(resourceRepo, getCurrentUserDetails().getId(), null, null);
	    	}
    	}
		return resCount;
    }
    
    @RequestMapping(value = "/stack_count_trend", method = RequestMethod.GET, produces = "application/json")
    public LinkedHashMap<String, Long> getStackCountTrend(@RequestParam(value="start_date", required=false) String startDate, @RequestParam(value="end_date", required=false) String endDate) throws IOException, ParseException, NotAuthorizedException, java.text.ParseException {
    	logger.log(Level.INFO, "GET : /stack_count_trend");
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	StackDAO stackDAO = new StackDAO();
    	LinkedHashMap<String, Long> stackCount;
    	if (startDate != null && endDate != null) {
        	Date beginDate = dtFormat.parse(startDate);
        	Date lastDate = dtFormat.parse(endDate);
        	if(hasPermission(Permissions.ALL_STACK_LISTING)){
	    		stackCount = stackDAO.getStackTrend(stackRepo, 0, beginDate, lastDate);
	    	}else{
	    		stackCount = stackDAO.getStackTrend(stackRepo, getCurrentUserDetails().getId(), beginDate, lastDate);
	    	}
    	} else {
	    	if(hasPermission(Permissions.ALL_STACK_LISTING)){
	    		stackCount = stackDAO.getStackTrend(stackRepo, 0, null, null);
	    	}else{
	    		stackCount = stackDAO.getStackTrend(stackRepo, getCurrentUserDetails().getId(), null, null);
	    	}
    	}
		return stackCount;
    }
    
    @RequestMapping(value = "/resource_size_trend", method = RequestMethod.GET, produces = "application/json")
    public LinkedHashMap<String, Double> getResourceSizeTrend(@RequestParam(value="start_date", required=false) String startDate, @RequestParam(value="end_date", required=false) String endDate) throws IOException, ParseException, NotAuthorizedException, java.text.ParseException {
    	logger.log(Level.INFO, "GET : /resource_size_trend");
    	enforcePermission(Permissions.MY_STACK_LISTING);
    	ResourceDAO resourceDao = new ResourceDAO();
    	LinkedHashMap<String, Double> resSizeByApp;
    	if (startDate != null && endDate != null) {
        	Date beginDate = dtFormat.parse(startDate);
        	Date lastDate = dtFormat.parse(endDate);
	    	if(hasPermission(Permissions.COST_GROUP_BY_USER)){
	    		resSizeByApp = resourceDao.getResourceSizeTrend(resourceRepo, resourceCostMetaDataDao, 0, beginDate, lastDate);
	    	}else{
	    		resSizeByApp = resourceDao.getResourceSizeTrend(resourceRepo, resourceCostMetaDataDao, getCurrentUserDetails().getId(), beginDate, lastDate);
	    	}
	    } 
    	else {
	    	if(hasPermission(Permissions.COST_GROUP_BY_USER)){
	    		System.out.println("Default Bar not Getting1");
	    		resSizeByApp = resourceDao.getResourceSizeTrend(resourceRepo, resourceCostMetaDataDao,0, null, null);
	    	}else{
	    		resSizeByApp = resourceDao.getResourceSizeTrend(resourceRepo, resourceCostMetaDataDao,getCurrentUserDetails().getId(), null, null);
	    	}
	    }
		return resSizeByApp;
    }  
    
    @RequestMapping(value = "/getInstanceCostdata", method = RequestMethod.GET, produces = "application/json")
    public List<String> getInstanceCostdata() throws IOException, ParseException, NotAuthorizedException {
    	logger.log(Level.INFO, "GET : /getInstanceCostdata");
    	enforcePermission(Permissions.MY_STACK_LISTING);
		return resourceRepo.getInstanceSize();
    }
    
    @RequestMapping(value = "/instance_cost_by_stack", method = RequestMethod.GET, produces = "application/json")
    public LinkedHashMap<String, Double> getTopInstanceCostByStack() throws IOException, ParseException, NotAuthorizedException {
    	enforcePermission(Permissions.ALL_STACK_LISTING);
    	List<Object[]> results = resourceRepo.getTopInstanceCostByStack(Calendar.getInstance().getTime());
    	results = results.size() > 10 ? results.subList(0, 9) : results;
    	LinkedHashMap<String, Double> instCostByStack = new LinkedHashMap<>();
    	for (int i = 0; i < results.size(); i++) {
    		Object[] arr = results.get(i);
    		String stack = (String) arr[0];
    		Double cost = (Double) arr[1];
    		instCostByStack.put(stack, cost);
    	}
    	return instCostByStack;
    }
    
    @RequestMapping(value = "/user_cost_by_group", method = RequestMethod.GET, produces = "application/json")
    public LinkedHashMap<String, Double> getTopUsersCostByGroup() throws IOException, ParseException, NotAuthorizedException {
    	enforcePermission(Permissions.COST_GROUP_BY_USER);
    	List<Object[]> results = resourceRepo.getTopUserCostByGroup(Calendar.getInstance().getTime());
    	results = results.size() > 10 ? results.subList(0, 9) : results;
    	LinkedHashMap<String, Double> costByGroup = new LinkedHashMap<>();
    	for (int i = 0; i < results.size(); i++) {
    		Object[] arr = results.get(i);
    		String group = (String) arr[0];
    		Double cost = (Double) arr[1];
    		costByGroup.put(group, cost);
    	}
    	return costByGroup;
    }
}
