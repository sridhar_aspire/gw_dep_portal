package com.gw.controller;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gw.auth.Permissions;
import com.gw.exception.NotAuthorizedException;
import com.gw.util.aws.CloudWatchUtil;
import com.gw.util.aws.metrics.MetricName;

@RestController
@RequestMapping("/monitoring")
public class MonitoringController extends BaseController {
	
	private static Logger logger = Logger.getLogger(MonitoringController.class.getName());

	@RequestMapping(value = "/instance_stats/cpu/{instanceId}", method = RequestMethod.GET, produces = "application/json")
    public HashMap<String, List<Double>> getInstanceCPUStats(@PathVariable("instanceId") String instanceId) throws NotAuthorizedException {
		logger.log(Level.INFO, "GET : /instance_stats/cpu/{instanceId}");
		enforcePermission(Permissions.MY_STACK_TERMINATE);
		HashMap<String, List<Double>> metricData = CloudWatchUtil.getInstance().getMetrics(instanceId, MetricName.CPU_UTILIZATION, 2);
		return metricData;
	}
	
	@RequestMapping(value = "/instance_stats/networkin/{instanceId}", method = RequestMethod.GET, produces = "application/json")
    public HashMap<String, List<Double>> getInstanceNetworkStats(@PathVariable("instanceId") String instanceId) throws NotAuthorizedException {
		logger.log(Level.INFO, "GET : /instance_stats/networkin/{instanceId}");
		enforcePermission(Permissions.MY_STACK_TERMINATE);
		HashMap<String, List<Double>> metricData = CloudWatchUtil.getInstance().getMetrics(instanceId, MetricName.NETWORK_IN, 2);
		return metricData;
	}
}
