package com.gw.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gw.auth.Permissions;
import com.gw.exception.NotAuthorizedException;
import com.gw.model.Setting;

@RestController
public class SettingController extends BaseController {

	@RequestMapping(value = "/setting/", method = RequestMethod.GET, produces = "application/json")
    public List<Setting> getAllSettings() throws NotAuthorizedException {
		enforcePermission(Permissions.EDIT_SETTING);
		List<Setting> settings = settingRepo.getAllSettings();
		return settings;
	}
	
	@RequestMapping(value = "/setting/", method = RequestMethod.PUT, produces = "application/json")
    public boolean saveSettings(@RequestBody List<Setting> settings) throws NotAuthorizedException {
		enforcePermission(Permissions.EDIT_SETTING);
		boolean updateSuccess = true;
		try {
			for (Setting setting: settings) {
				settingRepo.save(setting);
				updateSuccess = true;
			}
		} catch (Exception e) {
			updateSuccess = false;
		}
		return updateSuccess;
	}
}
