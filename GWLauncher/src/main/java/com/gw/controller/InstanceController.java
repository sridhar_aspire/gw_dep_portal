package com.gw.controller;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.ec2.model.InstanceStateName;
import com.gw.auth.Permissions;
import com.gw.exception.InvalidOperationException;
import com.gw.exception.NotAuthorizedException;
import com.gw.model.Resource;
import com.gw.util.aws.EC2Util;
import com.gw.workflow.StartInstance;
import com.gw.workflow.StopInstance;

@RestController
@RequestMapping("/instance")
public class InstanceController extends BaseController {

	private static Logger logger = Logger.getLogger(InstanceController.class.getName());
	
	@RequestMapping(value = "/{instanceId}/stop", method = RequestMethod.PUT, produces = "application/json")
    public boolean stopInstance(@PathVariable("instanceId") String instanceId,
    		@RequestParam(value="stack", required=true) String stackId, 
    		@RequestParam(value="app", required=true) String appId) throws NotAuthorizedException, InvalidOperationException{
		// TODO Check if instance belongs to the current user. Else if the user is an admin
		logger.log(Level.INFO, "PUT : /{instanceId}/stop");
		enforcePermission(Permissions.MY_STACK_MODIFY);
		if(stackRepo.findOne(stackId).getUserId() != getCurrentUserDetails().getId()){
			enforcePermission(Permissions.ALL_STACK_MODIFY);
		}
		com.amazonaws.services.ec2.model.Instance instance = EC2Util.getInstance().getInstanceDetailFromEC2(instanceId);
		if(InstanceStateName.Running.toString().equals(instance.getState().getName())){
			Resource resource = resourceRepo.getEC2Instance(stackId, appId, instanceId);
			resource.setStatus(InstanceStateName.Pending.toString());
			resource.setUpdatedDate(Calendar.getInstance().getTime());
			resourceRepo.save(resource);
			new StopInstance(instanceId, resourceRepo, stackId, appId).startTask();;
		}else{
			new InvalidOperationException("Instance Already in " + instance.getState().getName());
		}
		return true;
	}

	@RequestMapping(value = "/{instanceId}/start", method = RequestMethod.PUT, produces = "application/json")
    public boolean startInstance(@PathVariable("instanceId") String instanceId,
    		@RequestParam(value="stack", required=true) String stackId, 
    		@RequestParam(value="app", required=true) String appId) throws NotAuthorizedException,InvalidOperationException {
		// TODO Check if instance belongs to the current user. Else if the user is an admin
		logger.log(Level.INFO, "PUT : /{instanceId}/start");	
		enforcePermission(Permissions.MY_STACK_MODIFY);
		if(stackRepo.findOne(stackId).getUserId() != getCurrentUserDetails().getId()){
			enforcePermission(Permissions.ALL_STACK_MODIFY);
		}
		com.amazonaws.services.ec2.model.Instance instance = EC2Util.getInstance().getInstanceDetailFromEC2(instanceId);
		if(InstanceStateName.Stopped.toString().equals(instance.getState().getName())){
			Resource resource = resourceRepo.getEC2Instance(stackId, appId, instanceId);
			resource.setStatus(InstanceStateName.Pending.toString());
			//TODO Need to handle the running hours better
			resource.setRunningHours(resource.getRunningHours() + ((Calendar.getInstance().getTimeInMillis() - resource.getUpdatedDate().getTime())/1000));
			resource.setUpdatedDate(Calendar.getInstance().getTime());
			resourceRepo.save(resource);
			new StartInstance(instanceId, resourceRepo, stackId, appId).startTask();;
		}else{
			new InvalidOperationException("Instance Already in " + instance.getState().getName());
		}
		return true;
	}
	
}


