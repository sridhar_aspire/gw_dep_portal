package com.gw.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.jooq.tools.json.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.RequestContextHolder;

import com.gw.auth.AuthConstants;
import com.gw.auth.Permissions;
import com.gw.exception.DuplicateDataFoundException;
import com.gw.exception.ErrorResponse;
import com.gw.exception.InvalidOperationException;
import com.gw.exception.NoDataFoundException;
import com.gw.exception.NotAuthorizedException;
import com.gw.exception.ServerException;
import com.gw.model.Permission;
import com.gw.model.User;
import com.gw.repository.AppMetaDataRepository;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.GroupRepository;
import com.gw.repository.ResourceCostMetaDataRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.RoleRepository;
import com.gw.repository.SettingRepository;
import com.gw.repository.StackRepository;
import com.gw.repository.UserRepository;
import com.gw.util.PropertiesReader;
import com.gw.validator.UserValidator;

public class BaseController {
	
	@Autowired
	protected GroupRepository groupRepo;
	
	@Autowired 
	protected ResourceCostMetaDataRepository resourceCostMetaDataDao;
	
	@Autowired
	protected AppMetaDataRepository appMetaDataRepo;
	
	@Autowired
	protected StackRepository stackRepo;
	
	@Autowired
	protected ApplicationRepository appRepo;
	
	@Autowired
	protected ResourceRepository resourceRepo;
	
	@Autowired
	protected RoleRepository roleRepo;
	
	@Autowired
	protected UserRepository userRepo;
	
	@Autowired
	protected SettingRepository settingRepo;

    @Autowired
    protected UserValidator userValidator;
	
    protected static String datePattern = "MM/dd/yyyy";
	protected static SimpleDateFormat dtFormat = new SimpleDateFormat(datePattern);
	
	protected User getCurrentUserDetails() throws NotAuthorizedException {
		User user = (User) RequestContextHolder.currentRequestAttributes().getAttribute(AuthConstants.USER_INFO_SESSION_KEY, 1);
		if(user == null){
			throw new NotAuthorizedException();
		}
		return user;
	}
	
	protected boolean invalidateSession() throws NotAuthorizedException {
		RequestContextHolder.currentRequestAttributes().removeAttribute(AuthConstants.USER_INFO_SESSION_KEY, 1);
		RequestContextHolder.resetRequestAttributes();
		return true;
	}
	
	/**
	 * @param permission
	 * @return true if permission available for the user
	 * @throws NotAuthorizedException
	 */
	protected boolean enforcePermission(Permissions perm) throws NotAuthorizedException {
		User user = getCurrentUserDetails();
		Set<Permission> permissionList = user.getRole().getPermissions();
		for (Permission permission: permissionList) {
			if (permission.getPermissionName().equals(perm.getPermission())) {
				return true;
			}
		}
		throw new NotAuthorizedException("Current User does not have permissions to perform this action");
	}
	
	protected boolean hasPermission(Permissions perm) throws NotAuthorizedException {
		User user = getCurrentUserDetails();
		Set<Permission> permissionList = user.getRole().getPermissions();
		for (Permission permission: permissionList) {
			if (permission.getPermissionName().equals(perm.getPermission())) {
				return true;
			}
		}
		return false;
	}
	
	public static String getRedirectURL(HttpServletRequest request) {
		String authURL = PropertiesReader.getInstance().getValue("auth_url");
		authURL += "/registration/login.htm";
		String redirectUrl = request.getRequestURL().toString();
		redirectUrl = redirectUrl.replace(request.getRequestURI(), "");
		authURL += "?t=" + redirectUrl + "?x%3D%7B0%7D%26t%3D/";
		return authURL;
	}
	
	public static String getAuthServiceURL(HttpServletRequest request) {
		String authURL = PropertiesReader.getInstance().getValue("auth_url");
		return authURL;
	}
	
	public static String getUserInfoURL(HttpServletRequest request) {
		String authURL = PropertiesReader.getInstance().getValue("auth_url");
		authURL += "/registration/getUserDetails?_=" + Calendar.getInstance().getTimeInMillis();
		return authURL;
	}
	
	@ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(NoDataFoundException ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.NOT_FOUND.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(DuplicateDataFoundException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(DuplicateDataFoundException ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(IOException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(IOException ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(ParseException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(ParseException ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(java.text.ParseException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(java.text.ParseException ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(InvalidOperationException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(InvalidOperationException ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(ServerException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(ServerException ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
	@ExceptionHandler(NotAuthorizedException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(NotAuthorizedException ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.FORBIDDEN.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.FORBIDDEN);
    }
	
}
