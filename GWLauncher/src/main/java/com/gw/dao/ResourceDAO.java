package com.gw.dao;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.gw.model.Resource;
import com.gw.repository.ResourceCostMetaDataRepository;
import com.gw.repository.ResourceRepository;
import com.gw.util.AppConstants;
import com.gw.util.StringUtil;

@Component
@Service
public class ResourceDAO {

	private Logger logger = Logger.getLogger(ResourceDAO.class.getName());
	
	public LinkedHashMap<String, Long> getRunningResourceCountByApp(ResourceRepository resourceRepo, int uId) {
		List<Object[]> results ;
		if(uId == 0){
			results = resourceRepo.getRunningResourceCountByAppName();
		}else{
			results = resourceRepo.getMyRunningResourceCountByAppName(uId);
		}
		LinkedHashMap<String, Long> resultsMap = new LinkedHashMap<String, Long>();
		LinkedHashMap<String, Long> resCountByApp = new LinkedHashMap<>();
    	for (int i = 0; i < results.size(); i++) {
    		Object[] arr = results.get(i);
    		String appId = (String) arr[0];
    		Long instCount = (Long) arr[1];
    		resultsMap.put(appId, instCount);
    	}
    	Arrays.asList(AppConstants.APP_NAMES).forEach(app -> {
    		resCountByApp.put(app, resultsMap.containsKey(app) ?  (Long)resultsMap.get(app) : 0);
    	});
		return resCountByApp;
	}
	
	public LinkedHashMap<String, Long> getTerminatedResourceCountByApp(ResourceRepository resourceRepo, int uId) {
		List<Object[]> results;
		if(uId == 0){
			results = resourceRepo.getTerminatedResourceCountByAppName();
		}else{
			results = resourceRepo.getMyTerminatedResourceCountByAppName(uId);
		}
		LinkedHashMap<String, Long> resultsMap = new LinkedHashMap<String, Long>();
		LinkedHashMap<String, Long> resCountByApp = new LinkedHashMap<>();
    	for (int i = 0; i < results.size(); i++) {
    		Object[] arr = results.get(i);
    		String appId = (String) arr[0];
    		Long instCount = (Long) arr[1];
    		resultsMap.put(appId, instCount);
    	}
    	Arrays.asList(AppConstants.APP_NAMES).forEach(app -> {
    		resCountByApp.put(app, resultsMap.containsKey(app) ?  (Long)resultsMap.get(app) : 0);
    	});
		return resCountByApp;	
	}
	public LinkedHashMap<String, Long> getResourceTrend(ResourceRepository resourceRepo, int userId, Date beginDate, Date endDate) {
    	LinkedHashMap<String, Long> instanceCountMap = new LinkedHashMap<>();
    	List<Object[]> resCountListByApp;
    	if(userId == 0){
    		if (beginDate != null && endDate != null) {
    			resCountListByApp = resourceRepo.getInstanceCountByMonthBetween(beginDate, endDate);
    		} else {
    			resCountListByApp = resourceRepo.getInstanceCountByMonth();
    		}
    	}else{
    		if (beginDate != null && endDate != null) {
    			resCountListByApp = resourceRepo.getInstanceCountBetweenPerUserByMonth(userId, beginDate, endDate);
    		} else {
    			resCountListByApp = resourceRepo.getInstanceCountPerUserByMonth(userId);
    		}
    	}
    	LinkedHashMap<Integer, Long> resCountByApp = new LinkedHashMap<>();
    	for (int i = 0; i < resCountListByApp.size(); i++) {
    		Object[] arr = resCountListByApp.get(i);
    		Integer month = (Integer) arr[0];
    		Long count = (Long) arr[1];
    		resCountByApp.put(month - 1 , count);
    	}
    	
    	Calendar aCalendar = Calendar.getInstance();
    	if (beginDate == null || endDate == null) {
    		for (int i=0; i<6; i++) {
    			instanceCountMap.put(AppConstants.MONTH_NAMES[aCalendar.get(Calendar.MONTH)], 
        				resCountByApp.get(aCalendar.get(Calendar.MONTH)) == null ? 0 : (Long) resCountByApp.get(aCalendar.get(Calendar.MONTH)));
        		aCalendar.add(Calendar.MONTH, -1);
	    	}
    	} else {
    		aCalendar.setTime(endDate);
	    	int monthsBetween = StringUtil.getMonthsBetween(endDate, beginDate) + 1;
	    	for (int i=0; i<monthsBetween; i++) {
	    		instanceCountMap.put(AppConstants.MONTH_NAMES[aCalendar.get(Calendar.MONTH)], 
	    				resCountByApp.get(aCalendar.get(Calendar.MONTH)) == null ? 0 : (Long) resCountByApp.get(aCalendar.get(Calendar.MONTH)));
	    		aCalendar.add(Calendar.MONTH, -1);
	    	}
    	}
    	return instanceCountMap;
	}
	
	
	public List<Resource> getResourcesBetween(Date firstDate, Date lastDate, ResourceRepository resourceRepo) {
		logger.log(Level.INFO, "Start of getResourcesBetween");
		List<Resource> resourceList = resourceRepo.getLiveResourcesBetween(firstDate, lastDate);
		logger.log(Level.INFO, "End of getResourcesBetween");
		return resourceList;
	}

	public LinkedHashMap<String, Double> getResourceSizeTrend(ResourceRepository resourceRepo, ResourceCostMetaDataRepository resourceCostMetaDataRepo, int userId,Date beginDate,Date endDate) {
    	LinkedHashMap<String, Double> instanceCostMap = new LinkedHashMap<>();
    	List<Objects[]> resCostListByApp ;
    	if (beginDate != null && endDate != null) {
			resCostListByApp = resourceRepo.getInstanceCostByMonthBetween(beginDate, endDate,Calendar.getInstance().getTime());
		} else {
			resCostListByApp = resourceRepo.getInstanceCostByMonth(Calendar.getInstance().getTime());
		}   	   	 	
    	LinkedHashMap<Integer, Double> resCostByApp = new LinkedHashMap<>();
    	for (int i = 0; i < resCostListByApp.size(); i++) {
    		Object[] arr = resCostListByApp.get(i);
    		Integer month = (Integer) arr[0];
    		Double count = (Double) arr[1];
    		resCostByApp.put(month - 1 , count);
    	}
    	Calendar aCalendar1 = Calendar.getInstance();
    	if (beginDate == null || endDate == null) {
    		for (int i=0; i<6; i++) {
    			instanceCostMap.put(AppConstants.MONTH_NAMES[aCalendar1.get(Calendar.MONTH)], 
        				(Double) (resCostByApp.get(aCalendar1.get(Calendar.MONTH)) == null ? 0 :(Double) resCostByApp.get(aCalendar1.get(Calendar.MONTH))));
    			aCalendar1.add(Calendar.MONTH, -1);
	    	}
    	} else {
    		aCalendar1.setTime(endDate);
	    	int monthsBetween = StringUtil.getMonthsBetween(endDate, beginDate) + 1;
	    	for (int i=0; i<monthsBetween; i++) {
	    		instanceCostMap.put(AppConstants.MONTH_NAMES[aCalendar1.get(Calendar.MONTH)], 
	    				(Double) (resCostByApp.get(aCalendar1.get(Calendar.MONTH)) == null ? 0 : (Double) resCostByApp.get(aCalendar1.get(Calendar.MONTH))));
	    		aCalendar1.add(Calendar.MONTH, -1);
	    	}
    	}
    	return instanceCostMap;
		}	
}
