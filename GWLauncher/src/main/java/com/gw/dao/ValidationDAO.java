package com.gw.dao;

import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.repository.SettingRepository;
import com.gw.repository.StackRepository;
import com.gw.util.AppConstants;
import com.gw.util.aws.S3Util;
import com.gw.util.aws.StackUtil;

@Component
@Service
public class ValidationDAO {
	
	private static ValidationDAO validationDAO = new ValidationDAO();
	private Logger logger = Logger.getLogger(ValidationDAO.class.getName());
	
	public static ValidationDAO getInstance(){
		return validationDAO;
	}

	public boolean isStackExist(StackRepository stackRepo, String stackName) {
		List<Stack> stackList = stackRepo.getStackNames(stackName);
		if (stackList == null || stackList.size() == 0) {
			return false;
		}
		return true;
	}

	public boolean isValidTemplate(Stack stack) {
		try {
			File currentDirectory = new File(".");
			logger.log(Level.SEVERE, "CURRENT PATH : " + currentDirectory.getAbsolutePath());
			if(stack.getEnvType()){
				String cfContent = S3Util.getInstance().updateCFTemplate(AppConstants.CF_FILE_NAME);
				StackUtil.getInstance().validateCFTemplate(new JSONObject(cfContent));
			}else{
				String cfContent = S3Util.getInstance().updateCFTemplate(AppConstants.CF_SINGLE_INS_FILE_NAME);
				StackUtil.getInstance().validateCFTemplate(new JSONObject(cfContent));
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} 
	}

	public boolean getStackLimit(User user, StackRepository stackRepo, SettingRepository settingRepo) {
		if(stackRepo.getStackCountPerUser(user.getId()) > Integer.valueOf(settingRepo.findOne(AppConstants.LIVE_STACK_LIMIT_PER_USER).getValue())){
			return true;
		}
		return false;
	}

//	public List<Application> isDnsExist(ApplicationRepository appRepo, List<String> dnsList) {
//		List<Application> appList = appRepo.findByDnsSuffix(dnsList);
//		return appList;
//	}
//	
//	public List<Application> isDepGrpExist(ApplicationRepository appRepo, List<String> depGrpList) {
//		List<Application> appList = appRepo.findByDeploymentGroup(depGrpList);
//		return appList;
//	}
//	
//	public List<Application> isInstExist(ApplicationRepository appRepo, List<String> instList) {
//		List<Application> appList = appRepo.findByInstanceName(instList);
//		return appList;
//	}
}
