package com.gw.dao;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.gw.model.Application;
import com.gw.repository.ApplicationRepository;

@Component
@Service
public class ApplicationDAO {

	private Logger logger = Logger.getLogger(ApplicationDAO.class.getName());

	public boolean saveApplication(ApplicationRepository appRepo, Application application){
		logger.log(Level.INFO, "Start of saveApplication");
		appRepo.save(application);
		logger.log(Level.INFO, "End of saveApplication");
		return true;
	}
	
}
