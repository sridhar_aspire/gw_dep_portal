package com.gw.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.gw.model.Stack;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.StackRepository;
import com.gw.util.AppConstants;
import com.gw.util.StringUtil;

@Component
@Service
public class StackDAO {

	private Logger logger = Logger.getLogger(StackDAO.class.getName());

/*	public boolean createStack(StackRepository stackRepo, ApplicationRepository appRepo, UserRepository userRepo, Stack stack, int userId){
		logger.log(Level.INFO, "Start of createObject");
		stack.setUser(createUser);
		stackRepo.save(stack);
		if (stack.getApplications() != null && stack.getApplications().size() > 0) {
			for (Application app: stack.getApplications()) {
				appRepo.save(app);
			}
		}
		logger.log(Level.INFO, "End of createObject");
		return true;
	}*/

	public boolean updateStackStatus(StackRepository stackRepo, ApplicationRepository appRepo, ResourceRepository resourceRepo, Stack stack, String status, Date terminatedDate) {
		logger.log(Level.INFO, "Start of updateStack");
		stack.setStatus(status);
		stack.setTerminatedDate(terminatedDate);
		stack.setUpdatedDate(Calendar.getInstance().getTime());
		stack.getApplications().forEach(app ->{ 
			app.setStatus(status);
			app.setUpdatedDate(Calendar.getInstance().getTime());
			app.setTerminatedDate(terminatedDate);
			if(terminatedDate != null){
				app.getInstances().forEach(resource -> {
					resource.setStatus(status);
					resource.setUpdatedDate(Calendar.getInstance().getTime());
					resource.setTerminatedDate(terminatedDate);
					resourceRepo.save(resource);
				});
			}
			appRepo.save(app);
		});
		//TODO use single query to update with join
		stackRepo.save(stack);
		logger.log(Level.INFO, "End of updateStack");
		return true;
	}
	
	public Stack getStackDetail(String stackId, StackRepository stackRepo) {
		logger.log(Level.INFO, "Start of getStackDetail");
		Stack stack = stackRepo.findOne(stackId);
    	return stack;
	}

	public LinkedHashMap<String, Long> getStackTrend(StackRepository stackRepo, int userId, Date beginDate, Date endDate) {
    	LinkedHashMap<String, Long> stackCountMap = new LinkedHashMap<>();
    	List<Object[]> stackCountByMonth;
    	if(userId == 0){
    		if (beginDate != null && endDate != null) {
    			stackCountByMonth = stackRepo.getStackCountByMonthBetween(beginDate, endDate);
    		} else {
    			stackCountByMonth = stackRepo.getStackCountByMonth();
    		}
    	} else {
    		if (beginDate != null && endDate != null) {
    			stackCountByMonth = stackRepo.getStackCountBetweenPerUserByMonth(userId, beginDate, endDate);
    		} else {
    			stackCountByMonth = stackRepo.getStackCountPerUserByMonth(userId);
    		}
    	}
    	LinkedHashMap<Integer, Long> stackCount = new LinkedHashMap<>();
    	for (int i = 0; i < stackCountByMonth.size(); i++) {
    		Object[] arr = stackCountByMonth.get(i);
    		Integer month = (Integer) arr[0];
    		Long count = (Long) arr[1];
    		stackCount.put(month - 1 , count);
    	}
    	Calendar aCalendar = Calendar.getInstance();
    	if (beginDate == null || endDate == null) {
    		for (int i=0; i<6; i++) {
	    		stackCountMap.put(AppConstants.MONTH_NAMES[aCalendar.get(Calendar.MONTH)], 
	    				stackCount.get(aCalendar.get(Calendar.MONTH)) == null ? 0 : (Long) stackCount.get(aCalendar.get(Calendar.MONTH)));
	    		aCalendar.add(Calendar.MONTH, -1);
	    	}
    	} else {
    		aCalendar.setTime(endDate);
	    	int monthsBetween = StringUtil.getMonthsBetween(endDate, beginDate) + 1;
	    	for (int i=0; i<monthsBetween; i++) {
	    		stackCountMap.put(AppConstants.MONTH_NAMES[aCalendar.get(Calendar.MONTH)], 
	    				stackCount.get(aCalendar.get(Calendar.MONTH)) == null ? 0 : (Long) stackCount.get(aCalendar.get(Calendar.MONTH)));
	    		aCalendar.add(Calendar.MONTH, -1);
	    	}
    	}
    	return stackCountMap;
	}

	public LinkedHashMap<String, Long> getUnusedStackTrend(StackRepository stackRepo, int userId, Date beginDate, Date endDate) {
    	LinkedHashMap<String, Long> unusedStackCountMap = new LinkedHashMap<>();
    	List<Object[]> stackCountByMonth;
    	if(userId == 0){
    		if (beginDate != null && endDate != null) {
    			stackCountByMonth = stackRepo.getStackCountByMonthBetween(beginDate, endDate);
    		} else {
    			stackCountByMonth = stackRepo.getStackCountByMonth();
    		}
    	} else {
    		if (beginDate != null && endDate != null) {
    			stackCountByMonth = stackRepo.getStackCountBetweenPerUserByMonth(userId, beginDate, endDate);
    		} else {
    			stackCountByMonth = stackRepo.getStackCountPerUserByMonth(userId);
    		}
    	}
    	LinkedHashMap<Integer, Long> stackCount = new LinkedHashMap<>();
    	for (int i = 0; i < stackCountByMonth.size(); i++) {
    		Object[] arr = stackCountByMonth.get(i);
    		Integer month = (Integer) arr[0];
    		Long count = (Long) arr[1];
    		stackCount.put(month - 1 , count);
    	}
    	Calendar aCalendar = Calendar.getInstance();
    	if (beginDate == null || endDate == null) {
    		for (int i=0; i<6; i++) {
    			unusedStackCountMap.put(AppConstants.MONTH_NAMES[aCalendar.get(Calendar.MONTH)], 
	    				stackCount.get(aCalendar.get(Calendar.MONTH)) == null ? 0 : (Long) stackCount.get(aCalendar.get(Calendar.MONTH)));
	    		aCalendar.add(Calendar.MONTH, -1);
	    	}
    	} else {
    		aCalendar.setTime(endDate);
	    	int monthsBetween = StringUtil.getMonthsBetween(endDate, beginDate) + 1;
	    	for (int i=0; i<monthsBetween; i++) {
	    		unusedStackCountMap.put(AppConstants.MONTH_NAMES[aCalendar.get(Calendar.MONTH)], 
	    				stackCount.get(aCalendar.get(Calendar.MONTH)) == null ? 0 : (Long) stackCount.get(aCalendar.get(Calendar.MONTH)));
	    		aCalendar.add(Calendar.MONTH, -1);
	    	}
    	}
    	return unusedStackCountMap;
	}
	
}
