package com.gw.util;

import java.util.Comparator;
import java.util.HashMap;

import com.gw.model.Application;


public class ApplicationComparator implements Comparator<Application> {

	private static HashMap<String, Integer> applicationOrderMap = new HashMap<>();
	
	static {
		applicationOrderMap.put("ab", 1);
		applicationOrderMap.put("bc", 2);
		applicationOrderMap.put("cc", 3);
		applicationOrderMap.put("pc", 4);
	}
	
	@Override
	public int compare(Application o1, Application o2) {
		return applicationOrderMap.get(o1.getAppId()).compareTo(applicationOrderMap.get(o2.getAppId()));
	}

	
}
