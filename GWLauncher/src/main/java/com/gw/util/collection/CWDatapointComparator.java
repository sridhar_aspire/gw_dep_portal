package com.gw.util.collection;

import java.util.Comparator;

import com.amazonaws.services.cloudwatch.model.Datapoint;

public class CWDatapointComparator implements Comparator<Datapoint> {

	@Override
	public int compare(Datapoint o1, Datapoint o2) {
		return o1.getTimestamp().compareTo(o2.getTimestamp());
	}
	
	

}
