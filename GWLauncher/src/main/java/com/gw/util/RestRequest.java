package com.gw.util;

import java.io.UnsupportedEncodingException;

import org.apache.commons.httpclient.methods.StringRequestEntity;

public class RestRequest {
	
	private String body = null;
	private String contentType = null;
	private String charset = "UTF-8";
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	
	public StringRequestEntity getRequestEntity() throws UnsupportedEncodingException {
		 return new StringRequestEntity(getBody(), getContentType(), getCharset());
	}

}
