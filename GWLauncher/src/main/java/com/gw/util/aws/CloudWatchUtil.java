package com.gw.util.aws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.cloudwatch.model.Datapoint;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsRequest;
import com.amazonaws.services.cloudwatch.model.GetMetricStatisticsResult;
import com.gw.util.aws.metrics.AggregationType;
import com.gw.util.aws.metrics.MetricName;
import com.gw.util.collection.CWDatapointComparator;

@Component
public class CloudWatchUtil {
	
	private AmazonCloudWatchClient amazonCWClient;

	private static CloudWatchUtil instance = new CloudWatchUtil();

	private CloudWatchUtil() {
		init();
	}
	
	public static CloudWatchUtil getInstance() {
		return instance;
	}

	public void init(){
		if(AwsUtil.getInstance().getAWSCredential() != null){
			amazonCWClient = new AmazonCloudWatchClient(AwsUtil.getInstance().getAWSCredential()).withRegion(AwsUtil.getInstance().getRegion());
		}
	}
	
	public static AmazonCloudWatchClient getCWClient() {
		instance.init();
		return getInstance().amazonCWClient;
	}
	
	private static double usageThreshold = 10.0;
	
	public boolean isInstanceUnused(String instanceId, int nbrDays) {
		final long hrs = 1000 * 60 * 60 * 24 * nbrDays;
        final int oneHour = 60 * 60;
        GetMetricStatisticsRequest request = new GetMetricStatisticsRequest()
            .withStartTime(new Date(new Date().getTime()- hrs))
            .withNamespace("AWS/EC2")
            .withPeriod(oneHour)
            .withDimensions(new Dimension().withName("InstanceId").withValue(instanceId))
            .withMetricName("CPUUtilization")
            .withStatistics("Average", "Maximum")
            .withEndTime(new Date());
        GetMetricStatisticsResult result = getCWClient().getMetricStatistics(request);
        Double averageMax = 0.0;
        Double sumMax = 0.0;
        Double maxMax = 0.0;
        List<Datapoint> dataPoints = result.getDatapoints();
        Collections.sort(dataPoints, new CWDatapointComparator());
        for (final Datapoint dataPoint : dataPoints) {
            if (dataPoint.getMaximum() > maxMax) {
            	maxMax = dataPoint.getMaximum();
            }
            sumMax += dataPoint.getMaximum();
        }
        averageMax = sumMax/result.getDatapoints().size();
        if (averageMax < usageThreshold && maxMax < usageThreshold) {
        	return true;
        }
        return false;
	}
	
	public HashMap<String, List<Double>> getMetrics(String instanceId, MetricName metricName, int nbrDays) {
		final long hrs = 1000 * 60 * 60 * 24 * nbrDays;
        final int oneHour = 60 * 60;
        GetMetricStatisticsRequest request = new GetMetricStatisticsRequest()
            .withStartTime(new Date(new Date().getTime()- hrs))
            .withNamespace("AWS/EC2")
            .withPeriod(oneHour)
            .withDimensions(new Dimension().withName("InstanceId").withValue(instanceId))
            .withMetricName(metricName.getMetricName())
            .withStatistics("Average", "Maximum")
            .withEndTime(new Date());
        GetMetricStatisticsResult result = getCWClient().getMetricStatistics(request);
        List<Double> averageDPList = new ArrayList<Double>();
        List<Double> maxDPList = new ArrayList<>();
        List<Double> minDPList = new ArrayList<>();
        List<Double> dpDates=new ArrayList<>();
        List<Datapoint> dataPoints = result.getDatapoints();
        Collections.sort(dataPoints, new CWDatapointComparator());
        for (final Datapoint dataPoint : dataPoints) {
        	averageDPList.add(dataPoint.getAverage());
        	maxDPList.add(dataPoint.getMaximum());
        	minDPList.add(dataPoint.getMinimum());
        	dpDates.add(new Long(dataPoint.getTimestamp().getTime()).doubleValue());
        }
        HashMap<String, List<Double>> dataPointsMap = new HashMap<>();
        dataPointsMap.put(AggregationType.AVERAGE.getAggregationType(), averageDPList);
        dataPointsMap.put(AggregationType.MAXIMUM.getAggregationType(), maxDPList);
        dataPointsMap.put(AggregationType.MINIMUM.getAggregationType(), minDPList);
        dataPointsMap.put("TIME", dpDates);
        return dataPointsMap;
	}
}
