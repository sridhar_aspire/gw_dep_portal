package com.gw.util.aws;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import com.gw.util.AppConstants;
import com.gw.util.PropertiesReader;
import com.gw.util.StringUtil;

public class AwsUtil {
	
	private static final String SECRET_KEY_PROP_NAME = "secretKey";
	private static final String ACCESS_KEY_PROP_NAME = "accessKey";
	
	private BasicSessionCredentials awsCredentials = null;
	private static Regions AWSRegion = Regions.US_WEST_2;
	
	private static final String CROSS_ORGIN_ROLE = "arn:aws:iam::313761101697:role/sys-cloudops-launcher-" + AWSRegion.getName();
	private static Logger logger = Logger.getLogger(AwsUtil.class.getName());
	private static AwsUtil instance = new AwsUtil();
	
	private static Date sessionTokenIssueTime = Calendar.getInstance().getTime(); 
	
	public AwsUtil() {
		initSession();
	}

	public void initSession() {
		if (PropertiesReader.getInstance().getValue(ACCESS_KEY_PROP_NAME) != null
				&& PropertiesReader.getInstance().getValue(SECRET_KEY_PROP_NAME) != null) {
			AWSCredentials credentials = new BasicAWSCredentials(PropertiesReader.getInstance().getValue(ACCESS_KEY_PROP_NAME),
					PropertiesReader.getInstance().getValue(SECRET_KEY_PROP_NAME));
			AWSSecurityTokenServiceClient client = new AWSSecurityTokenServiceClient(credentials);
			AssumeRoleResult assumeResult = client.assumeRole(new AssumeRoleRequest().withRoleArn(CROSS_ORGIN_ROLE)
					.withDurationSeconds(3600).withRoleSessionName("DEP"));
			awsCredentials =  new BasicSessionCredentials(assumeResult.getCredentials().getAccessKeyId(),
			                    assumeResult.getCredentials().getSecretAccessKey(), assumeResult.getCredentials().getSessionToken());
			logger.log(Level.INFO, "New session started ");
			sessionTokenIssueTime = Calendar.getInstance().getTime();
		}
	}
	
	public static AwsUtil getInstance() {
		if (StringUtil.getMinutesBetween(Calendar.getInstance().getTime(), sessionTokenIssueTime) > AppConstants.SESSION_RENEWAL_TIME) {
			instance.initSession();
		}
		return instance;
	}

	public AWSCredentials getAWSCredential() {
		if (StringUtil.getMinutesBetween(Calendar.getInstance().getTime(), sessionTokenIssueTime) > AppConstants.SESSION_RENEWAL_TIME) {
			instance.initSession();
		}
		return awsCredentials;
	}
	
	public Regions getRegion(){
		if (StringUtil.getMinutesBetween(Calendar.getInstance().getTime(), sessionTokenIssueTime) > AppConstants.SESSION_RENEWAL_TIME) {
			instance.initSession();
		}
		return AWSRegion;
	}
}
