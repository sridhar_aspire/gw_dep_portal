package com.gw.util.aws;

import java.io.StringWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.stereotype.Component;

import com.gw.model.Stack;
import com.gw.util.PropertiesReader;

@Component
public class AmazonSESUtil {
	private static String From = PropertiesReader.getInstance().getValue("senderAddress");
	//private static String To = PropertiesReader.getInstance().getValue("receiverAddress");
	private static String ServerName = PropertiesReader.getInstance().getValue("smtpServerName");
	private static String UserName = PropertiesReader.getInstance().getValue("smtpUserName");
	private static String Password = PropertiesReader.getInstance().getValue("smtpPassword");
	private static VelocityEngine velocityEngine = new VelocityEngine();
	private static final Logger LOGGER = Logger.getLogger(AmazonSESUtil.class.getName());
	/*
	 * Calls the VM file to implement the email template for SES
	 */
	public static String getBody(Stack stack) {
		Properties p = new Properties();
		p.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		p.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		velocityEngine.init(p);
		VelocityContext velocityContext = new VelocityContext();
		velocityContext.put("stack", stack);
		Template template = velocityEngine.getTemplate("templates/SESNotificationMessage.vm");
		StringWriter stringWriter = new StringWriter();
		template.merge(velocityContext, stringWriter);
		return stringWriter.toString();
	}

	public boolean sendEmail(String body, String toEmail, String subject) {
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtp.port", PropertiesReader.getInstance().getValue("smtpPort"));
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.starttls.required", "true");
		Session session = Session.getDefaultInstance(props);
		try {
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(From));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
			msg.setSubject(subject);
			msg.setText(body);
			msg.setContent(body, "text/html; charset=utf-8");
			Transport transport = session.getTransport();
			LOGGER.log(Level.INFO, "Before connect to : " + ServerName);
			transport.connect(ServerName, UserName, Password);
			LOGGER.log(Level.INFO, "After connect to : " + ServerName);
			transport.sendMessage(msg, msg.getAllRecipients());
			System.out.println("Email sent!");
			return true;
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "The email was not sent: " + ex.getMessage());
			return false;
		}
	}

}