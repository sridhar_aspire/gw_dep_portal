package com.gw.util.aws.metrics;

public enum MetricName {

	CPU_UTILIZATION("CPUUtilization"), 
	NETWORK_IN("NetworkIn");
	
	private String metricName;

	private MetricName(String metricName) {
		this.metricName = metricName;
	}

	public String getMetricName() {
		return metricName;
	}
}
