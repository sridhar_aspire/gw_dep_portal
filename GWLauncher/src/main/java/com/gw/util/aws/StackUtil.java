package com.gw.util.aws;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.amazonaws.services.cloudformation.AmazonCloudFormationClient;
import com.amazonaws.services.cloudformation.model.CreateStackRequest;
import com.amazonaws.services.cloudformation.model.DeleteStackRequest;
import com.amazonaws.services.cloudformation.model.DescribeStackResourcesRequest;
import com.amazonaws.services.cloudformation.model.DescribeStackResourcesResult;
import com.amazonaws.services.cloudformation.model.DescribeStacksRequest;
import com.amazonaws.services.cloudformation.model.DescribeStacksResult;
import com.amazonaws.services.cloudformation.model.GetTemplateRequest;
import com.amazonaws.services.cloudformation.model.Parameter;
import com.amazonaws.services.cloudformation.model.Stack;
import com.amazonaws.services.cloudformation.model.StackResource;
import com.amazonaws.services.cloudformation.model.Tag;
import com.amazonaws.services.cloudformation.model.UpdateStackRequest;
import com.amazonaws.services.cloudformation.model.ValidateTemplateRequest;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancer;
import com.gw.model.Application;
import com.gw.model.Resource;
import com.gw.model.status.StackResourceType;
import com.gw.util.AppConstants;

@Component
public class StackUtil {
	
	private AmazonCloudFormationClient amazonCfClient;
	
	private static StackUtil instance = new StackUtil();
	
	private StackUtil(){
		init();
	}
	
	public void init() {
		if(AwsUtil.getInstance().getAWSCredential() != null){
			amazonCfClient = new AmazonCloudFormationClient(AwsUtil.getInstance().getAWSCredential()).withRegion(AwsUtil.getInstance().getRegion());
		}
	}
	
	public static AmazonCloudFormationClient getCFClient(){
		instance.init();
		return instance.amazonCfClient;
	}
	
	public static StackUtil getInstance() {
		return instance;
	}
	
	public static List<Resource> filterInstances(List<Resource> resourceList) {
		List<Resource> instances = resourceList.stream().filter(r->r.getResourceType().equals(StackResourceType.AUTO_SCALING_GROUP.toString()) || 
				r.getResourceType().equals(StackResourceType.INSTANCE.toString())).collect(Collectors.toList());
		return instances;
	}

	public static List<Resource> filterLoadBalancers(List<Resource> resourceList) {
		List<Resource> instances = resourceList.stream().filter(r->r.getResourceType().equals(StackResourceType.LOAD_BALANCER.toString())).collect(Collectors.toList());
		return instances;
	}
	
	public static boolean isResourceAnInstance(StackResource resource) {
		return (StackResourceType.AUTO_SCALING_GROUP.toString().equals(resource.getResourceType()));
	}
	
	public static boolean isResourceALoadBalancer(StackResource resource) {
		return (StackResourceType.LOAD_BALANCER.toString().equals(resource.getResourceType()));
	}
	
	public String createStack(String cfTemplate, String stackName, HashMap<String, String> stackParams){
		ArrayList<Tag> tagList = new ArrayList<Tag>(Arrays.asList(new Tag().withKey("Project Id").withValue(AppConstants.PROJECT_ID), 
				new Tag().withKey("Sub Project").withValue(AppConstants.SUB_PROJECT)));
		CreateStackRequest createStackRequest = new CreateStackRequest().withStackName(stackName)
				.withTemplateBody(cfTemplate).withTags(tagList).withParameters(getParameterList(stackParams));
		return	amazonCfClient.createStack(createStackRequest).getStackId();
	}
	
	private List<Parameter> getParameterList(HashMap<String, String> stackParams) {
		List<Parameter> stackParamsList = new ArrayList<Parameter>();
		stackParams.keySet().forEach(p ->{
			stackParamsList.add(new Parameter().withParameterKey(p).withParameterValue(stackParams.get(p)));
		});
		return stackParamsList;
	}

	public double getEstimatedMonthlyCost(){
		return 0.0;
	}
	
	
	public boolean deleteStack(String stackName){
		DeleteStackRequest deleteStackRequest = new DeleteStackRequest().withStackName(stackName);
		getCFClient().deleteStack(deleteStackRequest);
		return true;
	}
	
	public List<Stack> getStackDetails(String stackName) {
		DescribeStacksRequest describeStackReq = new DescribeStacksRequest().withStackName(stackName);
		DescribeStacksResult result = getCFClient().describeStacks(describeStackReq);
		List<Stack> stacks = result.getStacks();
		return stacks;
	}
	
	public List<StackResource> getStackInstances(String stackName) {
		DescribeStackResourcesRequest describeStackResourcesReq = new DescribeStackResourcesRequest();
		describeStackResourcesReq.withStackName(stackName);
		DescribeStackResourcesResult result = getCFClient().describeStackResources(describeStackResourcesReq);
		return result.getStackResources();
	}

	public void validateCFTemplate(JSONObject cfTemplate) {
		ValidateTemplateRequest tempReq = new ValidateTemplateRequest().withTemplateBody(cfTemplate.toString());
		getCFClient().validateTemplate(tempReq);
	}

	public void updateStack(String cfTemplate, String stackName, HashMap<String, String> stackParams) {
		UpdateStackRequest updateStackRequest = new UpdateStackRequest().withStackName(stackName).withTemplateBody(cfTemplate)
				.withParameters(getParameterList(stackParams));
		getCFClient().updateStack(updateStackRequest);
	}

	public String getExistingCFTemplate(String stackName) {
		GetTemplateRequest tempReq = new GetTemplateRequest().withStackName(stackName);
		return getCFClient().getTemplate(tempReq).getTemplateBody();
	}

	public synchronized void mapResourceWithApp(Application app, List<StackResource> resources) {
		List<Resource> resourceList = new ArrayList<>();
		for (StackResource resource : resources) {
			if (isResourceAnInstance(resource)) {
				List<Resource> resourceListForResource = getInstanceDetailsFromEC2(resource);
				int instanceSize = resourceListForResource.size();
				app.setInstCount(instanceSize);
				resourceList.addAll(resourceListForResource);
			} else if(isResourceAnEC2Instance(resource)){
				resourceList.add(getEC2InstanceDetails(resource, resource.getPhysicalResourceId()));
			}else if (isResourceALoadBalancer(resource)) {
				Resource inst = new Resource();
				inst.setPhysicalResourceId(resource.getPhysicalResourceId());
				inst.setLogicalResourceId(resource.getLogicalResourceId());
				inst.setDescription(resource.getDescription());
				inst.setStatusReason(resource.getResourceStatusReason());
				inst.setResourceType(resource.getResourceType());
				inst.setStatus(resource.getResourceStatus());
				inst.setInstanceType("LOAD_BALANCER");
				LoadBalancer lb = EC2Util.getInstance().getELBDetails(inst.getPhysicalResourceId());
				if (lb != null) {
					inst.setPublicDns(lb.getDNSName());
					inst.setInstanceId(lb.getLoadBalancerName());
				}
				resourceList.add(inst);
			} else {
				Resource inst = new Resource();
				inst.setPhysicalResourceId(resource.getPhysicalResourceId());
				inst.setLogicalResourceId(resource.getLogicalResourceId());
				inst.setDescription(resource.getDescription());
				inst.setStatusReason(resource.getResourceStatusReason());
				inst.setResourceType(resource.getResourceType());
				inst.setStatus(resource.getResourceStatus());
				resourceList.add(inst);
			}
		}
		app.setInstances(resourceList);
	}
	
	private boolean isResourceAnEC2Instance(StackResource resource) {
		return StackResourceType.INSTANCE.toString().equals(resource.getResourceType());
	}

	private List<Resource> getInstanceDetailsFromEC2(StackResource resource) {
		List<com.amazonaws.services.autoscaling.model.Instance> instanceList = EC2Util.getInstance().getInstanceDetailOfASG(resource.getPhysicalResourceId());
		List<Resource> resourceList = new ArrayList<>();
		for (com.amazonaws.services.autoscaling.model.Instance instance : instanceList) {
			resourceList.add(getEC2InstanceDetails(resource, instance.getInstanceId()));
		}
		return resourceList;
	}

	private Resource getEC2InstanceDetails(StackResource resource, String instanceId) {
		Resource inst = new Resource();
		inst.setPhysicalResourceId(resource.getPhysicalResourceId());
		inst.setLogicalResourceId(resource.getLogicalResourceId());
		inst.setDescription(resource.getDescription());
		inst.setStatusReason(resource.getResourceStatusReason());
		inst.setResourceType(resource.getResourceType());
		inst.setInstCount(1);
		
		Instance ec2Instance = EC2Util.getInstance().getInstanceDetailFromEC2(instanceId);
		inst.setInstanceId(ec2Instance.getInstanceId());
		inst.setImageId(ec2Instance.getImageId());
		inst.setStatus(ec2Instance.getState().getName().toString());
		inst.setInstSize(ec2Instance.getInstanceType());
		inst.setInstanceType(ec2Instance.getInstanceType());
		inst.setPrivateIp(ec2Instance.getPrivateIpAddress());
		inst.setPrivateDns(ec2Instance.getPrivateDnsName());
		inst.setPublicIp(ec2Instance.getPublicIpAddress());
		inst.setPublicDns(ec2Instance.getPublicDnsName());
		return inst;
	}

	public HashMap<String, String> getStackOutputs(String name) {
		List<Stack> stackList = getStackDetails(name);
		HashMap<String, String> appUrl = new LinkedHashMap<String, String>();
		if(!stackList.isEmpty()){
			stackList.get(0).getOutputs().stream().filter(out -> out.getOutputKey().endsWith("AppURL")).collect(Collectors.toList()).forEach(out ->{
				appUrl.put(out.getOutputKey(), out.getOutputValue());
			});;
		}
		return appUrl;
	}

	
}
