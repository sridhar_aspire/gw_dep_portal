package com.gw.util.aws;

import org.apache.commons.httpclient.Credentials;

import com.amazonaws.auth.AWSCredentials;
import com.gw.util.GenericCredentials;

public class AmazonCredentials extends GenericCredentials implements AWSCredentials {
	
	private String awsSecretKey;
	private String awsAccessKeyId;
	
	public AmazonCredentials() {
	}
	
	public AmazonCredentials(String awsAccessKeyId, String awsSecretKey) {
		this.awsAccessKeyId = awsAccessKeyId;
		this.awsSecretKey = awsSecretKey;
	}

	@Override
	public Credentials getCredentials() {
		return null;
	}
	@Override
	public String getAWSAccessKeyId() {
		//TODO Encrypt 
		return this.awsAccessKeyId;
	}
	@Override
	public String getAWSSecretKey() {
		//TODO Encrypt 
		return this.awsSecretKey;
	}
	
	

}
