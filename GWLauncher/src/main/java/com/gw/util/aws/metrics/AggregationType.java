package com.gw.util.aws.metrics;

public enum AggregationType {

	AVERAGE("AVERAGE"), 
	MAXIMUM("MAXIMUM"), 
	MINIMUM("MINIMUM");
	
	private String aggregationType;

	private AggregationType(String aggregationType) {
		this.aggregationType = aggregationType;
	}

	public String getAggregationType() {
		return aggregationType;
	}
}
