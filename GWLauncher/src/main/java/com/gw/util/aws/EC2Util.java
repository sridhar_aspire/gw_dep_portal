package com.gw.util.aws;

import java.util.List;

import org.springframework.stereotype.Component;

import com.amazonaws.services.autoscaling.AmazonAutoScalingClient;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult;
import com.amazonaws.services.autoscaling.model.Instance;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.InstanceState;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.InstanceStateName;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancingClient;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancersRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancersResult;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancer;

@Component
public class EC2Util {

	private AmazonEC2Client amazonEc2Client;
	private AmazonAutoScalingClient amazonASClient;
	private AmazonElasticLoadBalancingClient amazonELBClient;

	private static EC2Util instance = new EC2Util();

	private EC2Util() {
		init();
	}
	
	public static EC2Util getInstance() {
		return instance;
	}

	public void init(){
		if(AwsUtil.getInstance().getAWSCredential() != null){
			amazonEc2Client = new AmazonEC2Client(AwsUtil.getInstance().getAWSCredential()).withRegion(AwsUtil.getInstance().getRegion());
			amazonASClient = new AmazonAutoScalingClient(AwsUtil.getInstance().getAWSCredential()).withRegion(AwsUtil.getInstance().getRegion());
			amazonELBClient = new AmazonElasticLoadBalancingClient(AwsUtil.getInstance().getAWSCredential()).withRegion(AwsUtil.getInstance().getRegion());
		}
	}
	
	public static AmazonEC2Client getEC2Client() {
		instance.init();
		return instance.amazonEc2Client;
	}
	
	public static AmazonAutoScalingClient getASClient() {
		instance.init();
		return instance.amazonASClient;
	}
	
	public static AmazonElasticLoadBalancingClient getELBClient() {
		instance.init();
		return instance.amazonELBClient;
	}

	public List<Instance> getInstanceDetailOfASG(String autoScalingGroupName) {
		DescribeAutoScalingGroupsRequest describeASGGroupsReq = new DescribeAutoScalingGroupsRequest();
		describeASGGroupsReq.withAutoScalingGroupNames(autoScalingGroupName);
		DescribeAutoScalingGroupsResult asgResult = getASClient().describeAutoScalingGroups(describeASGGroupsReq);
		if (asgResult != null && asgResult.getAutoScalingGroups() != null && asgResult.getAutoScalingGroups().size() > 0) {
			return asgResult.getAutoScalingGroups().get(0).getInstances();
		}
		return null;
	}

	public com.amazonaws.services.ec2.model.Instance getInstanceDetailFromEC2(String instanceId) {
		DescribeInstancesRequest  describeInstanceReq = new DescribeInstancesRequest();
		describeInstanceReq.withInstanceIds(instanceId);
		DescribeInstancesResult result = getEC2Client().describeInstances(describeInstanceReq);
		List<Reservation> reservationList = result.getReservations();
		if (reservationList != null) {
			Reservation res = reservationList.get(0);
			List<com.amazonaws.services.ec2.model.Instance> instances = res.getInstances();
			if (instances != null) {
				return instances.get(0);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public void stopInstance(String instanceId) {
		StopInstancesRequest stopInstanceReq = new StopInstancesRequest().withInstanceIds(instanceId);;
		getEC2Client().stopInstances(stopInstanceReq);
	}
	
	public void startInstance(String instanceId) {
		StartInstancesRequest stopInstanceReq = new StartInstancesRequest().withInstanceIds(instanceId);;
		getEC2Client().startInstances(stopInstanceReq);
	}

	public LoadBalancer getELBDetails(String ARNString) {
		DescribeLoadBalancersRequest request = new DescribeLoadBalancersRequest().
		        withLoadBalancerArns(ARNString);
		DescribeLoadBalancersResult response = getELBClient()
		        .describeLoadBalancers(request);
		List<LoadBalancer> lbList = response.getLoadBalancers();
		if (lbList != null && lbList.size() > 0) {
			return lbList.get(0);
		}
		return null;
	}

}
