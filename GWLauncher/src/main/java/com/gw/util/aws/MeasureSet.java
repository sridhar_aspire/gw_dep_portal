package com.gw.util.aws;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;

public class MeasureSet implements Comparable<MeasureSet> {
	public Calendar timestamp;
	public HashMap<String, Double> measures = new HashMap<String, Double>();

	@Override
	public int compareTo(MeasureSet compare) {
		return (int) (timestamp.getTimeInMillis() - compare.timestamp.getTimeInMillis());
	}

	public void setMeasure(String measureName, double value) {
		measures.put(measureName, value);
	}

	public Set<String> getMeasureNames() {
		return measures.keySet();
	}

	public double getMeasure(String measureName) {
		return measures.get(measureName);
	}
}
