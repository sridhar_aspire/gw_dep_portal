package com.gw.util.aws;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.IOUtils;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.gw.util.AppConstants;

public class S3Util {
	
	private AmazonS3Client s3Client;
	private static Logger logger = Logger.getLogger(S3Util.class.getName());
	private static S3Util instance = new S3Util();
	
	private S3Util(){
		init();
	}
	
	public static S3Util getInstance() {
		return instance;
	}
	
	public void init() {
		if(AwsUtil.getInstance().getAWSCredential() != null){
			s3Client = new AmazonS3Client(AwsUtil.getInstance().getAWSCredential());
		}
	}
	
	public static AmazonS3Client getS3Client() {
		instance.init();
		return instance.s3Client;
	}

	public String updateCFTemplate(String cfFileName){
		GetObjectRequest getObjectRequest =  new GetObjectRequest(AppConstants.CF_BUCKET_NAME, cfFileName);
		S3Object s3Object = getS3Client().getObject(getObjectRequest);
		if(s3Object != null && s3Object.getObjectContent() != null){
			try(InputStream fileStream = s3Object.getObjectContent()) {
				if(s3Object.getObjectMetadata().getContentLength() <= AppConstants.CF_TEMPLATE_MAX_SIZE){
					String cfTemplateContent = IOUtils.toString(fileStream, "UTF-8"); 
//					FileUtil.writeInputStreamIntoFile(fileStream, cfLocalPath);
					logger.log(Level.INFO, "Cloud formation script file retrieved successfully. " + " last modified : " + 
							AppConstants.dateFormat.format(s3Object.getObjectMetadata().getLastModified()));
					return cfTemplateContent;
				}else{
					logger.log(Level.INFO, "File size : " + s3Object.getObjectMetadata().getContentLength() + 
							" is more than the defualt size(bytes): " + AppConstants.CF_TEMPLATE_MAX_SIZE);
				}
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Error occured while updaing CF template from S3");
			}
		}
		return null;
	}
}
