package com.gw.util.workflow;

import java.util.concurrent.Executors;

public abstract class Task implements Runnable {

	private Workflow flow = null;
	private long prevWaitTime = 10;
	private Task task;
	
	public Task(Workflow flow) {
		super();
		this.flow = flow;
	}
	
	public Task() {
		super();
	}
	
	public void setTask(Task task){
		this.task = task;
	}
	
	public void setWorkflow(Workflow flow) {
		this.flow = flow;
	}

	@Override
	public void run() {
		persistStartStatus();
		perform();
		while (!isDone()) {
			try {
				Thread.sleep(prevWaitTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			prevWaitTime = getExponentialWaitTime(prevWaitTime);
		}
		persistEndStatus();
	}

	public abstract void perform();
	
	public abstract boolean isDone();
	
	public abstract boolean hasSucceeded();

	private void persistEndStatus() {
		// TODO add code to persist state of the task as started
	}

	private void persistStartStatus() {
		// TODO Auto-generated method stub
		
	}
	
	private long getExponentialWaitTime(long prevWaitTime) {
		if (prevWaitTime >= (1*60*1000)) {
			return prevWaitTime;
		} else {
			return prevWaitTime + prevWaitTime;
		}
	}
	
	public void addInput(Object obj) {
		flow.addInput(obj);
	}
	
	public Object getVariable(String className) {
		return flow.getVariable(className);
	}
	
	public void setVariable(Object obj) {
		flow.setVariable(obj);
	}

	public void startTask(){
		Executors.newFixedThreadPool(1).execute(task);
	}
}
