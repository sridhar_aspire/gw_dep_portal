package com.gw.util.workflow;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class Workflow {
	
	Task currentTask = null;
	
	HashMap<String, Object> variables = new HashMap<>();
	LinkedList<Task> taskSequence = new LinkedList<>();
	
	public void addInput(String key, Object obj) {
		this.variables.put(key, obj);
	}
	
	public void addInput(Object obj) {
		this.variables.put(obj.getClass().getName(), obj);
	}
	
	public Object getVariable(String className) {
		return this.variables.get(className);
	}
	
	public void setVariable(Object obj) {
		this.variables.put(obj.getClass().getName(), obj);
	}
	
	// Add Tasks (Added by default in the order of addition)
	public void addTask(Task task) {
		taskSequence.add(task);
		task.setWorkflow(this);
	}
	
	public void startWorkflow() {
		// TODO : persistInputs
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		for (Task task:taskSequence) {
			executorService.execute(task);
		}
		executorService.shutdown();
		// TODO : delete persisted inputs and remove record.
		
	}

}
