package com.gw.util.workflow;

import java.util.concurrent.LinkedBlockingQueue;

public class WorkflowThreadPool {
	
	private static final int THREAD_POOL_MAX_SIZE = 20;

	private static WorkflowThreadPool wfThreadPool = null;

	private final int nThreads;
    private final PoolWorker[] threads;
    private final LinkedBlockingQueue<Runnable> queue;

    public static WorkflowThreadPool getInstance() {
		if (wfThreadPool == null) {
			synchronized (WorkflowThreadPool.class) {
				if (wfThreadPool == null) {
					wfThreadPool = new WorkflowThreadPool();
				}
			}
		}
		return wfThreadPool;
	}

	private WorkflowThreadPool() {
		this.nThreads = THREAD_POOL_MAX_SIZE;
        queue = new LinkedBlockingQueue<Runnable>();
        threads = new PoolWorker[nThreads];
        for (int i = 0; i < nThreads; i++) {
            threads[i] = new PoolWorker();
            threads[i].start();
        }
	}

	public void submitTask(Runnable task) {
        synchronized (queue) {
            queue.add(task);
            queue.notify();
        }
    }
	
	private class PoolWorker extends Thread {
        public void run() {
            Runnable task;

            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            System.out.println("An error occurred while queue is waiting: " + e.getMessage());
                        }
                    }
                    task = queue.poll();
                }

                // If we don't catch RuntimeException,
                // the pool could leak threads
                try {
                    task.run();
                } catch (RuntimeException e) {
                    System.out.println("Thread pool is interrupted due to an issue: " + e.getMessage());
                }
            }
        }
    }
}
