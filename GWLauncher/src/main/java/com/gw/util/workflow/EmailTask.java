package com.gw.util.workflow;

public abstract class EmailTask implements Runnable {

	private Workflow flow = null;
	private long prevWaitTime = 10;
	
	public EmailTask(Workflow flow) {
		super();
		this.flow = flow;
	}
	
	public EmailTask() {
		super();
	}
	
	public void setWorkflow(Workflow flow) {
		this.flow = flow;
	}

	@Override
	public void run() {
		persistStartStatus();
		perform();
		while (!isDone()) {
			try {
				Thread.sleep(prevWaitTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			prevWaitTime = getExponentialWaitTime(prevWaitTime);
		}
		persistEndStatus();
	}

	public abstract void perform();
	
	public abstract boolean isDone();
	
	public abstract boolean hasSucceeded();

	private void persistEndStatus() {
		// TODO add code to persist state of the task as started
	}

	private void persistStartStatus() {
		// TODO Auto-generated method stub
		
	}
	
	private long getExponentialWaitTime(long prevWaitTime) {
		if (prevWaitTime >= (5*60*1000)) {
			return prevWaitTime;
		} else {
			return prevWaitTime + prevWaitTime;
		}
	}
	
	public void addInput(Object obj) {
		flow.addInput(obj);
	}
	
	public Object getVariable(String className) {
		return flow.getVariable(className);
	}
	
	public void setVariable(Object obj) {
		flow.setVariable(obj);
	}

}
