package com.gw.util.jenkins;

import org.apache.commons.httpclient.UsernamePasswordCredentials;

import com.gw.util.GenericCredentials;

public class JenkinsCredentials extends GenericCredentials {

	private String userName;
	private String password;
	private String buildTriggerToken;

	public JenkinsCredentials() {
		super();
	}
	
	public JenkinsCredentials(String userName, String password, String buildTriggerToken) {
		super();
		this.userName = userName;
		this.password = password;
		this.buildTriggerToken = buildTriggerToken;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBuildTriggerToken() {
		return buildTriggerToken;
	}

	public void setBuildTriggerToken(String buildTriggerToken) {
		this.buildTriggerToken = buildTriggerToken;
	}

	@Override
	public UsernamePasswordCredentials getCredentials() {
		return new UsernamePasswordCredentials(getUserName(), getPassword());
	}

}
