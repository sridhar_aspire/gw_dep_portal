package com.gw.util.jenkins;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.Header;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gw.model.Build;
import com.gw.model.status.BuildStatus;
import com.gw.util.PropertiesReader;
import com.gw.util.RestApiClient;
import com.gw.util.RestResponse;

public class JenkinsUtility {
	
	String url;
	JenkinsCredentials creds;
	
	private static JenkinsUtility instance = new JenkinsUtility();
	
	private static final String URL_TEMPLATE_FOR_REMOTE_BUILD = "%s/job/%s/build?token=%s";
	private static final String URL_TEMPLATE_FOR_GET_BUILD = "%s/job/%s/%s/api/json";
	private static final String URL_TEMPLATE_FOR_REMOTE_BUILD_WITH_PARAM = "%s/job/%s/buildWithParameters?token=%s";
	private static final String URL_TEMPLATE_FOR_CHECKING_STATUS_BY_BUILD_NUMBER = "%s/job/%s/%s/api/json";
	private static final String URL_TEMPLATE_FOR_GETTING_ALL_BUILD_NUMBERS = "%s/job/%s/api/json";
	private static final String URL_TEMPLATE_FOR_FETCHING_CONSOLE_OUTPUT_BY_BUILD_NUMBER = "%s/job/%s/%s/consoleText";
	Logger logger = Logger.getLogger(JenkinsUtility.class.getClass());
	
	private JenkinsUtility() {
		String ip = PropertiesReader.getInstance().getValue("jenkins.ip");
		String port = PropertiesReader.getInstance().getValue("jenkins.port");
		String username = PropertiesReader.getInstance().getValue("jenkins.username");
		String password = PropertiesReader.getInstance().getValue("jenkins.password");
		String buildTriggerToken = PropertiesReader.getInstance().getValue("jenkins.build.trigger.token");
		this.url = "https://" + ip ;
		if (port != null && port.trim().length() > 0) {
			this.url += ":" + port;
		}
		this.creds = new JenkinsCredentials(username, password, buildTriggerToken);
	}
	
	public static JenkinsUtility getInstance() {
		return instance;
	}
	
	public Map<String, ArrayList<String>> getBuilds(String projectName) {
		Map<String, ArrayList<String>> buildMap = new HashMap<String,  ArrayList<String>>();
		try {
			BuildResponse buildResponse = new BuildResponse();
			List<Build> buildList = new ArrayList<Build>();
			JSONObject buildDetails;
			String buildUrl = getUrlForBuildList(this.url, projectName);
			RestResponse restResponse = RestApiClient.post(buildUrl, creds, null, null);
			buildResponse.setStatusCode(restResponse.getStatusCode());
			buildResponse.setResponseBody(restResponse.getResponseBody());
			if (restResponse.getHeaders() != null) {
				for (Header header : restResponse.getHeaders()) {
					if ("Location".equals(header.getName())) {
						buildResponse.setLocation(header.getValue());
						break;
					}
				}
			}
			System.out.println(buildResponse.getResponseBody());
			buildDetails = new JSONObject(buildResponse.getResponseBody());
			String[] keys = JSONObject.getNames(buildDetails);
			ObjectMapper om = new ObjectMapper();
			for (String key : keys)
			{ 
				if(key.equals("builds")){
					System.out.println(buildDetails.get(key));
					buildList = om.readValue(buildDetails.get(key).toString(), om.getTypeFactory().constructCollectionType(List.class, Build.class));
					mapBuildWithApp(buildMap, buildList, projectName);
				}
			}
		} catch (IOException e) {
			logger.error(e);
		}
		return buildMap;
	}

	private Map<String, ArrayList<String>> mapBuildWithApp(Map<String, ArrayList<String>> buildMap, List<Build> buildList, String projectName) {
		
		for(Build build : buildList) {
			BuildResponse buildResponse = new BuildResponse();
			String buildUrl = String.format(URL_TEMPLATE_FOR_GET_BUILD, this.url, projectName, build.getNumber());
			JSONObject buildobj = new JSONObject(runJob(buildResponse, buildUrl).getResponseBody());
			String appName = buildobj.getJSONArray("actions").getJSONObject(0).getJSONArray("parameters").getJSONObject(1).getString("value");
			String buildStatus = buildobj.getString("result");
			
			if(buildStatus.equalsIgnoreCase(BuildStatus.SUCCESS.getStatus())){
				if(buildMap.get(appName) != null){
					if(buildMap.get(appName).size() <= 20){
						buildMap.get(appName).add(build.getNumber());
					}
				}else{
					buildMap.put(appName, new ArrayList<String>());
					buildMap.get(appName).add(build.getNumber());
				}
			}
		}
		return buildMap;
	}

	public BuildResponse triggerJob(String projectName) {
		BuildResponse buildResponse = new BuildResponse();
		String buildUrl = getUrlForRemoteBuildAction(this.url, projectName, creds.getBuildTriggerToken());
		return runJob(buildResponse, buildUrl);
	}

	private BuildResponse runJob(BuildResponse buildResponse, String buildUrl) {
		try {
			RestResponse restResponse = RestApiClient.post(buildUrl, creds, null, null);
			buildResponse.setStatusCode(restResponse.getStatusCode());
			buildResponse.setResponseBody(restResponse.getResponseBody());
			if (restResponse.getHeaders() != null) {
				for (Header header : restResponse.getHeaders()) {
					if ("Location".equals(header.getName())) {
						buildResponse.setLocation(header.getValue());
						break;
					}
				}
			}
		} catch (IOException e) {
			logger.error(e);
		}
		return buildResponse;
	}
	
	public String triggerJobWithParams(String projectName, HashMap<String, String> variables) {
		BuildResponse buildResponse = new BuildResponse();
		String buildUrl = getUrlForRemoteBuildActionWithParams(this.url, projectName, creds.getBuildTriggerToken(), variables);
		try {
			JSONObject triggerResp = new JSONObject(runJob(buildResponse, buildUrl)); 
			if (triggerResp != null) {
				return triggerResp.getString("location");
			}
		} catch(JSONException ex) {
//			return null;
			// Do thing. Just log exception
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		return null;
	}
	

	public BuildResponse checkBuildStatus(String projectName, String buildNumber) {
		BuildResponse buildResponse = new BuildResponse();
		String url = getUrlForBuildStatus(this.url, projectName, buildNumber);
		try {
			RestResponse restResponse = RestApiClient.post(url, creds, new HashMap<String, String>(),null);
			buildResponse.setStatusCode(restResponse.getStatusCode());
			buildResponse.setResponseBody(restResponse.getResponseBody());
		} catch (IOException e) {
			logger.error(e);
		}
		return buildResponse;
	}
	
	public BuildResponse fetchConsoleOutputForBuild(String projectName, String buildNumber) {
		BuildResponse buildResponse = new BuildResponse();
		String url = getUrlForFetchingConsoleOutput(this.url, projectName, buildNumber);
		try {
			RestResponse restResponse = RestApiClient.post(url, creds, new HashMap<String, String>(), null);
			buildResponse.setStatusCode(restResponse.getStatusCode());
			buildResponse.setResponseBody(restResponse.getResponseBody());
		} catch (IOException e) {
			logger.error(e);
		}
		return buildResponse;
	}
	
	public InputStream fetchConsoleOutputForBuildAsStream(String projectName, String buildNumber) {
		String url = getUrlForFetchingConsoleOutput(this.url, projectName, buildNumber);
		InputStream responseStream = null;
		try {
			responseStream = RestApiClient.postToGetstream(url, creds, new HashMap<String, String>(), null);
		} catch (IOException e) {
			logger.error(e);
		}
		return responseStream;
	}

	public static String getUrlForRemoteBuildAction(String baseUrl, String projectName, String token) {
		return String.format(URL_TEMPLATE_FOR_REMOTE_BUILD, baseUrl, projectName, token);
	}
	
	public static String getUrlForRemoteBuildActionWithParams(String baseUrl, String projectName, String token, HashMap<String, String> variables) {
		String url = String.format(URL_TEMPLATE_FOR_REMOTE_BUILD_WITH_PARAM, baseUrl, projectName, token);
		if (variables != null && variables.size() > 0) {
			for (String key : variables.keySet()) {
				url += "&" + key + "=" + variables.get(key);
			}
		}
		return url;
	}

	public static String getUrlForBuildStatus(String baseUrl, String projectName, String buildNumber) {
		return String.format(URL_TEMPLATE_FOR_CHECKING_STATUS_BY_BUILD_NUMBER, baseUrl, projectName, buildNumber);
	}
	
	public static String getUrlForBuildList(String baseUrl, String projectName) {
		return String.format(URL_TEMPLATE_FOR_GETTING_ALL_BUILD_NUMBERS, baseUrl, projectName);
	}

	public static String getUrlForFetchingConsoleOutput(String baseUrl, String projectName, String buildNumber) {
		return String.format(URL_TEMPLATE_FOR_FETCHING_CONSOLE_OUTPUT_BY_BUILD_NUMBER, baseUrl, projectName,
				buildNumber);
	}

	public String getNextBuild(String UCD) {
		String buildUrl = getUrlForBuildList(this.url, UCD);
		String nextBuild = null;
		try {
			RestResponse restResponse = RestApiClient.post(buildUrl, creds, null, null);
			JSONObject buildDetails = new JSONObject(restResponse.getResponseBody());
			nextBuild = String.valueOf(buildDetails.getInt("nextBuildNumber"));
		} catch (IOException e) {
			logger.error(e);
		}
		return nextBuild;
	}

	public BuildResponse checkQueueBuild(String queueItemUrl) {
		BuildResponse buildResponse = new BuildResponse();
		return runJob(buildResponse, queueItemUrl + "/api/json");
	}

//	private String getUrlForQueueBuild(String url, String queueItem) {
//		return String.format(URL_TEMPLATE_FOR_GET_QUEUE_BUILD, queueItem);
//	}

}
