package com.gw.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

/**
 * RestApiClient class to execute rest apiactions
 */
public class RestApiClient {

//	private static boolean proxyEnabled = true;
	private static Logger logger = Logger.getLogger(RestApiClient.class.getName());

	/**
	 * Method to restrict creating new object for this class.
	 */
	private RestApiClient() {

	}

	/**
	 * Method to execute rest api post method actions
	 * 
	 * @param url - to get access
	 * @param params - no of parameter to attach i post method
	 * @return response as string
	 */
	public static RestResponse post(String url,  GenericCredentials creds, Map<String, String> params, RestRequest content)
			throws IOException {
		RestResponse restResponse = new RestResponse();
		try {
			PostMethod postMethod = new PostMethod(url);
			if (params != null && !params.isEmpty()) {
				Set<String> keys = params.keySet();
				for (String key : keys) {
					postMethod.addParameter(key, params.get(key));
				}
			}
			if (content != null) {
				postMethod.setRequestEntity(content.getRequestEntity());
			}
			HttpClient hc = getHttpClient(creds);
			
			hc.executeMethod(postMethod);
			logger.info("POST METHOD CALL RESPONSE STATUS : " + postMethod.getStatusCode());
			restResponse.setHeaders(postMethod.getResponseHeaders());
			restResponse.setStatusCode(postMethod.getStatusCode());
			restResponse.setResponseBody(getStringFromInputStream(postMethod.getResponseBodyAsStream()));
			logger.info("POST METHOD CALL RESPONSE BODY : " + restResponse.getResponseBody());
		} catch (IOException e) {
			throw e;
		}
		return restResponse;
	}
	
	public static InputStream postToGetstream(String url,  GenericCredentials creds, Map<String, String> params, RestRequest content)
			throws IOException {
		RestResponse restResponse = new RestResponse();
		PostMethod postMethod = new PostMethod(url);
		try {
			if (params != null && !params.isEmpty()) {
				Set<String> keys = params.keySet();
				for (String key : keys) {
					postMethod.addParameter(key, params.get(key));
				}
			}
			if (content != null) {
				postMethod.setRequestEntity(content.getRequestEntity());
			}
			HttpClient hc = getHttpClient(creds);
			
			hc.executeMethod(postMethod);
			logger.info("POST METHOD CALL RESPONSE STATUS : " + postMethod.getStatusCode());
			logger.info("POST METHOD CALL RESPONSE BODY : " + restResponse.getResponseBody());
		} catch (IOException e) {
			throw e;
		}
		return postMethod.getResponseBodyAsStream();
	}

	private static HttpClient getHttpClient(GenericCredentials creds) {
		HttpClient hc = new HttpClient();
		//setProxy(hc);
		if (creds != null) { 
			hc.getState().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
					creds.getCredentials());
			hc.getParams().setAuthenticationPreemptive(true);
		}
		return hc;
	}

	// TODO GET PROXY SETTING FROM ENV VARIABLES
//	private static void setProxy(HttpClient hc) {
//		if (proxyEnabled) {
//			String proxyHost = "eproxy.aspiresys.com";
//			int proxyPort = 3128;
//			hc.getHostConfiguration().setProxy(proxyHost, proxyPort);
//		}
//	}

	/**
	 * Method to convert input stream to string
	 * @param is input stream object
	 * @return
	 */
	private static String getStringFromInputStream(InputStream is) {
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}

}
