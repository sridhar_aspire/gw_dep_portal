package com.gw.util;

import org.apache.commons.httpclient.Credentials;

public abstract class GenericCredentials {
//  private String host;
//  private int port;
//  private String realm;
  
//  public String getHost() {
//    return host;
//  }
//  public void setHost(String host) {
//    this.host = host;
//  }
//  public int getPort() {
//    return port;
//  }
//  public void setPort(int port) {
//    this.port = port;
//  }
//  public String getRealm() {
//    return realm;
//  }
//  public void setRealm(String realm) {
//    this.realm = realm;
//  }
  
  public abstract Credentials getCredentials();
  
}
