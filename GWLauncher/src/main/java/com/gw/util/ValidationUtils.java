package com.gw.util;

import com.gw.dao.ValidationDAO;
import com.gw.exception.DuplicateDataFoundException;
import com.gw.exception.ServerException;
import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.repository.RoleRepository;
import com.gw.repository.SettingRepository;
import com.gw.repository.StackRepository;

public class ValidationUtils {
	
	public static boolean validateStackParams(StackRepository stackRepo, RoleRepository roleRepo, SettingRepository settingRepo, Stack stack, User user) throws DuplicateDataFoundException, ServerException{
		if(roleRepo.getDefaultRole().getId() == user.getRoleId() && ValidationDAO.getInstance().getStackLimit(user, stackRepo, settingRepo)){
			throw new ServerException("Stack Limit Exceeded");
		}else if(ValidationDAO.getInstance().isStackExist(stackRepo, stack.getName())){
			throw new DuplicateDataFoundException(stack.getName() + " Already Exists");
		}else if(stack.getAwsStackid() == null && !ValidationDAO.getInstance().isValidTemplate(stack)){
			throw new ServerException("Exception while validating template");
		}else{
			return true;
		}
	}
}
