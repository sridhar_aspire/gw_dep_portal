package com.gw.util;

import java.util.Comparator;
import java.util.HashMap;

import com.gw.model.AppMetaData;


public class AppMetaDataComparator implements Comparator<AppMetaData> {

	private static HashMap<String, Integer> applicationOrderMap = new HashMap<>();
	
	static {
		applicationOrderMap.put("AB", 1);
		applicationOrderMap.put("BC", 2);
		applicationOrderMap.put("CC", 3);
		applicationOrderMap.put("PC", 4);
		applicationOrderMap.put("Portals", 5);
	}
	
	@Override
	public int compare(AppMetaData o1, AppMetaData o2) {
		return applicationOrderMap.get(o1.getId()).compareTo(applicationOrderMap.get(o2.getId()));
	}

	
}
