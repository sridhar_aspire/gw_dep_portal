package com.gw.util;

import java.io.InputStream;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PropertiesReader {
	
	private Properties appProperties = new Properties();
	private String propFile = "application.properties";
	private Logger logger = Logger.getLogger(PropertiesReader.class.getName());
	
	private static PropertiesReader instance = new PropertiesReader();
	
	private PropertiesReader() {
		try (InputStream in = PropertiesReader.class.getClassLoader().getResourceAsStream(propFile)) {
			System.out.println("Loaded Property file : " + propFile);
			appProperties.load(in);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error occured while loading " + propFile);
		}
	}
	
	public static PropertiesReader getInstance() {
		return instance;
	}

	public Properties getAppProperties() {
		return appProperties;
	}
	
	public String getValue(String key) {
		return appProperties.getProperty(key);
	}
	
	public Set<Object> getKeySet(String key) {
		return appProperties.keySet();
	}
}
