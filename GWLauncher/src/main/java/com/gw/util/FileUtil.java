package com.gw.util;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.tomcat.util.http.fileupload.IOUtils;

public class FileUtil {
	
	public static String readFileContent(String fileName) throws IOException {
		InputStream inputStream = null;
        try {
            inputStream = FileUtil.class.getClassLoader().getResourceAsStream(fileName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String content = readAllContentFromBufferedReader(bufferedReader);
            return content;
        } finally {
            if (inputStream != null) {
               inputStream.close();
            }
        }
	}
	
	private static String readAllContentFromBufferedReader(BufferedReader buffIn) throws IOException {
	    StringBuilder everything = new StringBuilder();
	    String line;
	    while( (line = buffIn.readLine()) != null) {
	       everything.append(line);
	    }
	    return everything.toString();
	}

	public static Map<String, String> readSpecificStringFromStream(InputStream inStream) {
		BufferedReader br = null;
		String line = null;
		Map<String, String> values = new HashMap<String, String>();
		try {
			br = new BufferedReader(new InputStreamReader(inStream));
			while ((line = br.readLine()) != null) {
				if(line.contains("ETag:") && line.contains("}")){
					values.put("etag", line.split("ETag:")[1].split("}")[0].trim());
				} if( line.contains(AppConstants.DEP_UCD + "-") && line.contains(".zip")){
					values.put("RevisionId", AppConstants.DEP_UCD + "-" + line.split(AppConstants.DEP_UCD + "-")[1].split(".zip")[0].trim() + ".zip");
				}
			}
		} catch (IOException | NullPointerException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return values;
	}

	public static void writeInputStreamIntoFile(InputStream fileContent, String cfLocalPath) throws IOException{
		try(OutputStream output = new FileOutputStream(FileUtil.class.getClassLoader().getResource(cfLocalPath).getFile());) {
			IOUtils.copy(fileContent, output);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
