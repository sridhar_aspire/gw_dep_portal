package com.gw.util;

import java.text.SimpleDateFormat;

public class AppConstants {
	
	public static final String DEP_UCD = "DEP-Universal-CodeDeploy";
	public static final String DEP_UCDP = "DEP-Universal-CodeDeploy-Promotion";
	public static final String PROJECT_ID = "green";
	public static final String SUB_PROJECT = "internal_dev_environment";
	public static final String CF_BUCKET_NAME = "dep-cf-template";
	public static final String CF_FILE_NAME = "insurancesuite-green-dev-min.json";
	public static final String CF_SINGLE_INS_FILE_NAME = "insurancesuite-green-dev-single-instance.min.json";
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
	public static final String CF_LOCAL_PATH = "cfTemplate.json";
	public static final String CF_SINGLE_INS_LOCAL_PATH = "cfTemplateSingleInstance.json";
	public static final Integer DEFAULT_STACK_LIMIT = 200;
	public static final Integer CF_TEMPLATE_MAX_SIZE = 51200;
	public static final String DEFAULT_INSTANCE_SIZE = "DEFAULT_INSTANCE_SIZE";
	public static final String[] MONTH_NAMES = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	public static final String[] APP_NAMES = {"ab","bc","cc","pc"};
	public static final String BUILD_GREEN_DEV_JDBC_URL = "oracle2.gwre-green-dev.net:orcl";
	public static final String BUILD_DATA_GUARD_JDBC_URL = "oradev.dataguard-env:oradev";
	public static final int SESSION_RENEWAL_TIME = 40;
	public static final String LIVE_STACK_LIMIT_PER_USER  =  "LIVE STACK LIMIT PER USER";
}
