package com.gw.util;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class StringUtil {

	public static String datePattern = "MM/dd/yyyy hh:mm:ss";
	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();
	
	public static String getUUID() {
		return UUID.randomUUID().toString();
	}
	
	public static String getCurrentDateString() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat(datePattern);
		return format.format(date);
	}
	
	public static String generateAlphanumericStr(int len) {
		StringBuilder sb = new StringBuilder( len );
		for( int i = 0; i < len; i++ ) 
			sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		return sb.toString();
	}
	
	public static int getMonthsBetween(Date endDate, Date beginDate) {
		Calendar startCalendar = Calendar.getInstance();
		startCalendar.setTime(beginDate);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);
		int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		return diffMonth;
	}
	
	public static long getMinutesBetween(Date endDate, Date beginDate) {
		long diffMilliseconds = 0;
		if(endDate != null && beginDate != null){
			diffMilliseconds = (endDate.getTime() - beginDate.getTime()) / 60000;
		}
		return diffMilliseconds;
	}
	
	
	
}
