package com.gw.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "APPLICATION")
public class Application {

	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;
	
	@Column(name = "stack_id")
	private String stackId ;
	
	@Column(name = "app_id")
	private String appId = null;

	@Column(name = "new_db")
	private boolean newDB = true;
	
	@Column(name = "instance_size")
	private String instSize = null;
	
	@Max(value = 5)
	@Min(value = 1)
	@Column(name = "instance_count")
	private Integer instCount = 0;
	
	@Column(name = "new_build")
	private boolean newBuild = false;
	
	@Column(name = "build_number")
	private String buildId = null;
	
	@Column(name = "status")
	private String status = null;
	
	@Column(name = "deployment_group")
	private String deploymentGroup;
	
	@Column(name = "instance_name")
	private String instanceName;
	
	@Column(name = "dns_suffix")
	private String dnsSuffix;
	
	@Column(name = "created_date")
	private Date createdDate = new Date();
	
	@Column(name = "updated_date")
	private Date updatedDate = new Date();
	
	@Column(name = "terminated_date")
	private Date terminatedDate = null;
	
	@Column(name = "app_url")
	private String appURL;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="appId")
	private List<Resource> instances;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	public boolean isNewDB() {
		return newDB;
	}
	public void setNewDB(boolean newDB) {
		this.newDB = newDB;
	}
	
	public String getBuildId() {
		return buildId;
	}
	public void setBuildId(String buildId) {
		this.buildId = buildId;
	}

	public boolean isNewBuild() {
		return newBuild;
	}
	public void setNewBuild(boolean newBuild) {
		this.newBuild = newBuild;
	}
	
	public String getInstSize() {
		return instSize;
	}
	public void setInstSize(String instSize) {
		this.instSize = instSize;
	}
	
	public Integer getInstCount() {
		return instCount;
	}
	public void setInstCount(Integer instCount) {
		this.instCount = instCount;
	}
	
	public String getDeploymentGroup() {
		return deploymentGroup;
	}
	public void setDeploymentGroup(String deploymentGroup) {
		this.deploymentGroup = deploymentGroup;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getTerminatedDate() {
		return terminatedDate;
	}
	public void setTerminatedDate(Date terminatedDate) {
		this.terminatedDate = terminatedDate;
	}

	public List<Resource> getInstances() {
		return instances;
	}
	public void setInstances(List<Resource> instances) {
		this.instances = instances;
	}
	
	public String getDnsSuffix() {
		return dnsSuffix;
	}
	public void setDnsSuffix(String dnsSuffix) {
		this.dnsSuffix = dnsSuffix;
	}
	
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getStackId() {
		return stackId;
	}
	public void setStackId(String stackId) {
		this.stackId = stackId;
	}
	public String getAppURL() {
		return appURL;
	}
	public void setAppURL(String appURL) {
		this.appURL = appURL;
	}
}