package com.gw.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "RESOURCE")
public class Resource {

	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;
	
	@Column(name = "application_id")
	private String appId;
	
	@Column(name = "app_name")
	private String appName = null;
	
	@Column(name = "aws_stack_id")
	private String awsStackId = null;
	
	@Column(name = "instance_size")
	private String instSize;
	
	@Column(name = "instance_count")
	private Integer instCount;
	
	@Column(name = "instance_id")
	private String instanceId;
	
	@Column(name = "instance_type")
	private String instanceType;
	
	@Column(name = "image_id")
	private String imageId;
	
	@Column(name = "public_ip")
	private String publicIp;
	
	@Column(name = "public_dns")
	private String publicDns;
	
	@Column(name = "private_ip")
	private String privateIp;
	
	@Column(name = "private_dns")
	private String privateDns;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "physical_resource_id")
	private String physicalResourceId = "";
	
	@Column(name = "logical_resource_id")
	private String logicalResourceId = "";
	
	@Column(name = "description")
	private String description = "";
	
	@Column(name = "status_reason")
	private String statusReason = "";
	
	@Column(name = "resource_type")
	private String resourceType = "";
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "updated_date")
	private Date updatedDate;
	
	@Column(name = "terminated_date")
	private Date terminatedDate = null;
	
	@Column(name = "running_hours")
	private long runningHours = 0;
	
	@Column(name = "unused")
	private boolean unused = false;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	public String getAwsStackId() {
		return awsStackId;
	}
	public void setAwsStackId(String awsStackId) {
		this.awsStackId = awsStackId;
	}

	public String getInstSize() {
		return instSize;
	}
	public void setInstSize(String instSize) {
		this.instSize = instSize;
	}

	public Integer getInstCount() {
		return instCount;
	}
	public void setInstCount(Integer instCount) {
		this.instCount = instCount;
	}

	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getPublicIp() {
		return publicIp;
	}
	public void setPublicIp(String publicIp) {
		this.publicIp = publicIp;
	}

	public String getPrivateIp() {
		return privateIp;
	}
	public void setPrivateIp(String privateIp) {
		this.privateIp = privateIp;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getPhysicalResourceId() {
		return physicalResourceId;
	}
	public void setPhysicalResourceId(String physicalResourceId) {
		this.physicalResourceId = physicalResourceId;
	}

	public String getLogicalResourceId() {
		return logicalResourceId;
	}
	public void setLogicalResourceId(String logicalResourceId) {
		this.logicalResourceId = logicalResourceId;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatusReason() {
		return statusReason;
	}
	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getInstanceType() {
		return instanceType;
	}
	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getPublicDns() {
		return publicDns;
	}
	public void setPublicDns(String publicDns) {
		this.publicDns = publicDns;
	}

	public String getPrivateDns() {
		return privateDns;
	}
	public void setPrivateDns(String privateDns) {
		this.privateDns = privateDns;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getTerminatedDate() {
		return terminatedDate;
	}
	public void setTerminatedDate(Date terminatedDate) {
		this.terminatedDate = terminatedDate;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public long getRunningHours() {
		return runningHours;
	}
	public void setRunningHours(long runningHours) {
		this.runningHours = runningHours;
	}
	public boolean getUnused() {
		return unused;
	}
	public void setUnused(boolean unused) {
		this.unused = unused;
	}
}
