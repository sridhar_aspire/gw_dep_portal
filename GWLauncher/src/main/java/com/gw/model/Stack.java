package com.gw.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "STACK")
public class Stack {

	@Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	private String id;
	
	@Column(name = "aws_stack_id")
	private String awsStackid;
	
	@Column(name = "name")
	@Pattern(regexp = "^[a-zA-Z0-9]*$")
	private String name;
	
	@Max(value = 90)
	@Min(value = 10)
	@Column(name = "planned_usage_days")
	private Integer plannedUseDays;
	
	@Column(name = "auto_shutdown")
	private boolean autoShutdown;
	
	@Column(name = "status")
	private String status;
	
    @Column(name="user_id")
    private Integer userId;
	
	@Column(name = "notified_date")
	private Date notifiedDate;
	
	@Column(name = "created_date")
	private Date createdDate = null;
	
	@Column(name = "updated_date")
	private Date updatedDate = null;
	
	@Column(name = "terminated_date")
	private Date terminatedDate = null;
	
	@Column(name = "env_type")
	private boolean envType;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy= "stackId")
	private List<Application> applications;
	
	@ManyToMany(targetEntity=User.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "SHARED_STACK_USER", joinColumns = @JoinColumn(name = "stack_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private Set<User> sharedUsers;
	
	@ManyToMany(targetEntity=Group.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "SHARED_STACK_GROUP", joinColumns = @JoinColumn(name = "stack_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"))
    private Set<Group> sharedGroup;
	
	@Transient
	private int instanceCount;
	
	@Transient
	private double estimatedMonthlyCost = 0.0;
	
	@Transient
	private String username;
	
	@Transient
	private int sharedToGroup;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAutoShutdown() {
		return autoShutdown;
	}

	public void setAutoShutdown(boolean autoShutdown) {
		this.autoShutdown = autoShutdown;
	}

	public Integer getPlannedUseDays() {
		return plannedUseDays;
	}

	public void setPlannedUseDays(Integer plannedUseDays) {
		this.plannedUseDays = plannedUseDays;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getNotifiedDate() {
		return notifiedDate;
	}

	public void setNotifiedDate(Date notifiedDate) {
		this.notifiedDate = notifiedDate;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public Date getTerminatedDate() {
		return terminatedDate;
	}
	public void setTerminatedDate(Date terminatedDate) {
		this.terminatedDate = terminatedDate;
	}
	
	public int getInstanceCount() {
		return instanceCount;
	}
	public void setInstanceCount(int instanceCount) {
		this.instanceCount = instanceCount;
	}
	public double getEstimatedMonthlyCost() {
		return estimatedMonthlyCost;
	}
	public void setEstimatedMonthlyCost(double estimatedMonthlyCost) {
		this.estimatedMonthlyCost = estimatedMonthlyCost;
	}
	public String getAwsStackid() {
		return awsStackid;
	}
	public void setAwsStackid(String awsStackid) {
		this.awsStackid = awsStackid;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public List<Application> getApplications() {
		return applications;
	}
	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}
	public Set<User> getSharedUsers() {
		return sharedUsers;
	}
	public void setSharedUsers(Set<User> sharedUsers) {
		this.sharedUsers = sharedUsers;
	}
	public boolean getEnvType() {
		return envType;
	}
	public void setEnvType(boolean envType) {
		this.envType = envType;
	}

	public Set<Group> getSharedGroup() {
		return sharedGroup;
	}

	public void setSharedGroup(Set<Group> sharedGroup) {
		this.sharedGroup = sharedGroup;
	}

	public int getSharedToGroup() {
		return sharedToGroup;
	}

	public void setSharedToGroup(int dataObj) {
		this.sharedToGroup = dataObj;
	}
	
}
 