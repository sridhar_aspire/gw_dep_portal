package com.gw.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gw.model.converter.StringListConverter;

@Entity
@Table(name="APPLICATION_META_DATA")
public class AppMetaData {
	
	@Id
	@Column(name = "id")
	private String id; 
	
	@Column(name = "build_info")
	@Convert(converter = StringListConverter.class)
	private List<String> buildInfo;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public List<String> getBuildInfo() {
		return buildInfo;
	}
	public void setBuildInfo(List<String> buildInfo) {
		this.buildInfo = buildInfo;
	}

}
