package com.gw.model.status;

public enum StackResourceType {
	DEPLOYMENT_GROUP("AWS::CodeDeploy::DeploymentGroup"),
	LOAD_BALANCER("AWS::ElasticLoadBalancingV2::LoadBalancer"),
	LAUNCH_CONFIGURATION("AWS::AutoScaling::LaunchConfiguration"),
	LISTENER("AWS::ElasticLoadBalancingV2::Listener"),
	TARGET_GROUP("AWS::ElasticLoadBalancingV2::TargetGroup"),
	AUTO_SCALING_GROUP("AWS::AutoScaling::AutoScalingGroup"),
	RECORD_SET("AWS::Route53::RecordSet"),
	APPLICATION("AWS::CodeDeploy::Application"),
	INSTANCE("AWS::EC2::Instance");
	
	private String resourceType;
	
	private StackResourceType(String resourceType){
		this.resourceType = resourceType;
	}
	
	@Override
	public String toString() {
		return resourceType.toString();
	}
}
