package com.gw.model.status;

public enum StackStatus {

	INITIALIZING("INITIALIZING"), 
	CREATE_IN_PROGRESS("CREATE_IN_PROGRESS"), 
	CREATE_COMPLETE("CREATE_COMPLETE"), 
	UPDATE_IN_PROGRESS("UPDATE_IN_PROGRESS"), 
	UPDATE_COMPLETE("UPDATE_COMPLETE"), 
	CREATE_FAILED("CREATE_FAILED"), 
	UPDATE_FAILED("UPDATE_FAILED"),
	DELETE_IN_PROGRESS("DELETE_IN_PROGRESS"),
    DELETE_FAILED("DELETE_FAILED"),
    DELETE_COMPLETE("DELETE_COMPLETE"),
	TERMINATION_STARTED("TERMINATION_STARTED");
	
	private String stackStatus;

	private StackStatus(String stackStatus) {
		this.stackStatus = stackStatus;
	}

	public String getStatus() {
		return stackStatus;
	}
}
