package com.gw.model.status;

public enum SettingTypes {

	SETTING("SETTING"), 
	APP_SETTING("APP_SETTING");
	
	private String settingType;

	private SettingTypes(String settingType) {
		this.settingType = settingType;
	}

	public String getSettingType() {
		return settingType;
	}
}
