package com.gw.model.status;

public enum BuildStatus {
	
	SUCCESS("SUCCESS"),
	FAILURE("FAILURE"),
	ABORTED("ABORTED"),
	UNSTABLE("UNSTABLE"),
	NOT_BUILD("NOT_BUILD");
	
	private String buildStatus;

	private BuildStatus(String buildStatus) {
		this.buildStatus = buildStatus;
	}

	public String getStatus() {
		return buildStatus;
	}
}
