package com.gw.model.status;

public enum InstanceStatus {

	INITIALIZING("INITIALIZING"), 
	CREATE_IN_PROGRESS("CREATE_IN_PROGRESS"), 
	CREATE_COMPLETE("CREATE_COMPLETE"),
	RUNNING("RUNNING"),
	STOPPED("STOPPED"),
	TERMINATED("TERMINATED"),
	UPDATE_IN_PROGRESS("UPDATE_IN_PROGRESS"), 
	UPDATE_COMPLETE("UPDATE_COMPLETE"), 
	CREATE_FAILED("CREATE_FAILED"), 
	UPDATE_FAILED("UPDATE_FAILED"),
	DELETE_IN_PROGRESS("DELETE_IN_PROGRESS"),
    DELETE_FAILED("DELETE_FAILED"),
    DELETE_COMPLETE("DELETE_COMPLETE");
	
	private String stackStatus;

	private InstanceStatus(String stackStatus) {
		this.stackStatus = stackStatus;
	}

	public String getStatus() {
		return stackStatus;
	}
}
