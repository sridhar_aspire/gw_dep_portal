package com.gw.model;

import java.util.StringTokenizer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gw.model.status.SettingTypes;

@Entity
@Table(name = "SETTING")
public class Setting {
	
	@Id
	@Column(name = "var_name")
	private String key;
	
	@Column(name = "description")
	private String desc;
	
	@Column(name = "value")
	private String value;
	
	@Transient
	private String appId;
	
	@Column(name="type")
	private String type;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAppId() {
		if (type.equals(SettingTypes.APP_SETTING)) {
			if (appId == null) {
				StringTokenizer tokenizer = new StringTokenizer(type, "|");
				if (tokenizer.countTokens() == 2) {
					appId = tokenizer.nextToken();
				}
				return appId;
			}
		}
		return null;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}
