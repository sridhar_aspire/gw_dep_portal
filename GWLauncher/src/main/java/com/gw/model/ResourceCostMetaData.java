package com.gw.model; 

import javax.persistence.*;

@Entity
@Table(name="RESOURCE_COST_META_DATA")
public class ResourceCostMetaData {
	
	@Id
	@Column(name = "id")
	private String id;
	
	@Column(name = "type", nullable = false)
	private String type;
	
	@Column(name = "size", nullable = false)
	private String size;
	
	@Column(name = "cost", nullable = false)
	private double cost;
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}

	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}

}
