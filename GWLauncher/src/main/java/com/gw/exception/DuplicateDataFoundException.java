package com.gw.exception;

public class DuplicateDataFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public DuplicateDataFoundException() {
		super();
	}

	public DuplicateDataFoundException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
