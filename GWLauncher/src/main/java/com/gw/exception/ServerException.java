package com.gw.exception;

public class ServerException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public ServerException() {
		super();
	}

	public ServerException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
