package com.gw.exception;

public class NoDataFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public NoDataFoundException() {
		super();
	}

	public NoDataFoundException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
