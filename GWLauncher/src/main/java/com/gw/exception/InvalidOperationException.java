package com.gw.exception;

public class InvalidOperationException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public InvalidOperationException() {
		super();
	}

	public InvalidOperationException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
