package com.gw.exception;

public class NotAuthorizedException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public NotAuthorizedException() {
		super();
	}

	public NotAuthorizedException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = "NOT AUTHORIZED : " + errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
