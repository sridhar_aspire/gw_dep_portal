package com.gw;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@ComponentScan("com.gw")
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class Application {
	
	private static ApplicationContext context = null;

    public static void main(String[] args) {
        context = SpringApplication.run(Application.class, args);
        System.out.println(context.getDisplayName());
    }

    
    
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
//            printBeans(ctx);
        };
    }

//	private void printBeans(ApplicationContext ctx) {
//		System.out.println("Let's inspect the beans provided by Spring Boot:");
//		String[] beanNames = ctx.getBeanDefinitionNames();
//		Arrays.sort(beanNames);
//		for (String beanName : beanNames) {
//		    System.out.println(beanName);
//		}
//	}

}
