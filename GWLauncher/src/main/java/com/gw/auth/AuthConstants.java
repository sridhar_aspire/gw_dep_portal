package com.gw.auth;

public class AuthConstants {
	
	public static final String USER_INFO_SESSION_KEY = "USER_INFO";
	public static final String AUTH_VALUE_KEY = "gwAuth";

}
