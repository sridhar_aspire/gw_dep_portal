package com.gw.auth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.gw.controller.BaseController;
import com.gw.controller.UserController;
import com.gw.model.User;
import com.gw.repository.RoleRepository;
import com.gw.repository.UserRepository;

@Component
public class AuthFilter extends OncePerRequestFilter {
	
	private static final String ROLE_DEP_PORTAL_ADMINISTRATOR = "ROLE_DEP-PORTAL-ADMINISTRATOR";
	private static final String ROLE_DEP_PORTAL_USER = "ROLE_DEP-PORTAL-USER";

	private static Logger logger = Logger.getLogger(UserController.class.getName());
	
	@Autowired
	protected UserRepository userRepo;
	
	@Autowired
	protected RoleRepository roleRepo;

	@Override
	protected  void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		logger.log(Level.INFO, "Inside Auth Filter : " + request.getRequestURI());
		if (request.getSession() == null || RequestContextHolder.currentRequestAttributes().getAttribute(AuthConstants.USER_INFO_SESSION_KEY, 1) == null) {
			logger.log(Level.INFO, "User Not found in session");
			String authToken = getAuthValue(request);
			if (authToken == null || authToken.trim().length() == 0) {
				logger.log(Level.INFO, "Auth Token not found");
				chain.doFilter(request, response);
			} else {
				logger.log(Level.INFO, "Auth Token Found.");
				User userObj = getUserFromToken(authToken, request);
				logger.log(Level.INFO, "Getting user info from Auth Token " + authToken);
				if (userObj == null) {
					logger.log(Level.INFO, "User Not found");
					chain.doFilter(request, response);
				} else {
					logger.log(Level.INFO, "Valid User Object found on the server side : " + userObj.toString());
//					request.getSession().setAttribute(AuthConstants.USER_INFO_SESSION_KEY, userObj);
					RequestContextHolder.currentRequestAttributes().setAttribute(AuthConstants.USER_INFO_SESSION_KEY, userObj, 1);
					AuthToken auth = new AuthToken(userObj, authToken, null);
					SecurityContextHolder.getContext().setAuthentication(auth);
					chain.doFilter(request, response);
				}
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	private User getUserFromToken(String authToken, HttpServletRequest hsr) {
		try {
			logger.log(Level.INFO, "Getting user info : " + authToken);
			String url = BaseController.getUserInfoURL(hsr);
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(url);
			request.setHeader("Cookie", AuthConstants.AUTH_VALUE_KEY + "=" + authToken);
			
			HttpResponse response = client.execute(request);
			System.out.println("Response Code : "
			                + response.getStatusLine().getStatusCode());
	
			BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
	
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			User user = null;
			logger.log(Level.INFO, "User Repo not null. Getting user info from the DB");
			user = convertToUser(result.toString());
			userRepo.save(user);
			return user;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	private User convertToUser(String result) throws ParseException {
		logger.log(Level.INFO, "User Info from IDP : " + result);
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(result);
		JSONObject extraObj = (JSONObject) json.get("extra");
		String username = json.get("username").toString();
		JSONArray authoritiesArray = (JSONArray) json.get("authorities");
		ArrayList<String> roles = new ArrayList<>();
		for (int authCount=0; authCount < authoritiesArray.size(); authCount++) {
			JSONObject authObject = (JSONObject) authoritiesArray.get(authCount);
			String authority = authObject.get("authority").toString();
			roles.add(authority);
		}
		
		User user = null;
		if (userRepo == null) {
			user = new User();
		} else {
			user = userRepo.findByUserName(username);
			if (user == null) {
				user = new User();
				user.setCreatedDate(Calendar.getInstance().getTime());
			}
			boolean isAdmin = false;
			boolean isUser = false;
			for (int authCount=0; authCount < roles.size(); authCount++) {
				if (ROLE_DEP_PORTAL_ADMINISTRATOR.equals(roles.get(authCount))) {
					isAdmin = true;
				}
				if (ROLE_DEP_PORTAL_USER.equals(roles.get(authCount))) {
					isUser = true;
				}
			}
			if (isAdmin) {
				user.setRole(roleRepo.getAdminRole());
				user.setRoleId(user.getRole().getId());
			} else if (isUser) {
				user.setRole(roleRepo.getDefaultRole());
				user.setRoleId(user.getRole().getId());
			}
		}
		if (user.getRole() != null) {
			user.setUserName(username);
			user.setEmail(username);
			user.setUserUid(extraObj.get("userUid").toString());
			user.setCompanyUid(extraObj.get("companyUid").toString());
			user.setCompanyId(extraObj.get("companyId").toString());
			user.setFirstName(extraObj.get("firstName").toString());
			user.setLastName(extraObj.get("lastName").toString());
			user.setUpdatedDate(Calendar.getInstance().getTime());
			return user;
		}
		return null;
	}

	private String getAuthValue(HttpServletRequest hsr) {
		logger.log(Level.INFO, "INSIDE getAuthValue()");
		Cookie[] cookies = hsr.getCookies();
		Enumeration<String> headerNames = hsr.getHeaderNames();
		if (hsr.getParameterMap().containsKey("x")) {
			String authToken = hsr.getParameter("x");
			return authToken;
		}
		if (cookies != null) {
			logger.log(Level.INFO, "Cookies not null");
			for (Cookie cookie : cookies) {
				logger.log(Level.INFO, cookie.getName() + " - " + cookie.getValue() + " / " + cookie.getDomain());
				if (cookie.getName().equals(AuthConstants.AUTH_VALUE_KEY)) {
					logger.log(Level.INFO, "TOKEN = " + cookie.getValue());
					return cookie.getValue();
				}
			}
		}
		if (headerNames != null && headerNames.hasMoreElements()) {
			logger.log(Level.INFO, "Auth Token not found in cookies. But Searching in headers");
			while(headerNames.hasMoreElements()) {
				String headerName = headerNames.nextElement();
				String value = hsr.getHeader(headerName);
				logger.log(Level.INFO, "HEADER : " + headerName + " - " + value);
				if (headerName.equals(AuthConstants.AUTH_VALUE_KEY)) {
					logger.log(Level.INFO, "TOKEN = " + value);
					return value;
				}
			}
		} else {
			logger.log(Level.INFO, "both cookie and headers are empty");
		}
		return null;
	}

}
