package com.gw.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.gw.model.User;
import com.gw.repository.UserRepository;

@Component
public class AuthProvider implements AuthenticationProvider {

	@Autowired
	UserRepository userRepo;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		System.out.println("INSIDE AUTH PROVIDER : " + authentication.toString());
		AuthToken auth = (AuthToken) authentication;
        User user = userRepo.findByUserName(auth.getName());
        
        if(user == null){
            throw new UnknownUserException("Could not find user with ID: " + auth.getName());
        }
        return authentication;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return AuthToken.class.isAssignableFrom(authentication);
	}

}
