package com.gw.auth;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gw.model.Permission;
import com.gw.model.Role;
import com.gw.model.User;
import com.gw.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {
	
	@Autowired
    private UserRepository userRepository;
	
	@Override
    @Transactional(readOnly = true)
	public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
        System.out.println("Token = " + token);
		User user = userRepository.findByUserName(token.getName());
		
		
        Role role = user.getRole();
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Permission permission : role.getPermissions()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(permission.getPermissionName()));
        }
        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPwd(), grantedAuthorities);
    }

}
