package com.gw.workflow;

import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.util.workflow.Workflow;
import com.gw.workflow.task.SendAutoShutDownReminder;

public class AutoShutDownReminder extends Workflow{
	public AutoShutDownReminder initWorkFlow(Stack stack, User user) {
		addInput(stack);		
		SendAutoShutDownReminder sendEmailTask = new SendAutoShutDownReminder(stack, user);
		addTask(sendEmailTask);
		return this;
	}
}
