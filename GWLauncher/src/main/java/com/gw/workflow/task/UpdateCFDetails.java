package com.gw.workflow.task;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.services.cloudformation.model.StackResource;
import com.amazonaws.services.cloudformation.model.StackStatus;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancer;
import com.gw.model.Application;
import com.gw.model.Resource;
import com.gw.model.Stack;
import com.gw.model.status.ApplicationStatus;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.util.aws.EC2Util;
import com.gw.util.aws.StackUtil;
import com.gw.util.workflow.Task;
import com.gw.util.workflow.Workflow;

public class UpdateCFDetails extends Task {
	
	Application app;
	Stack stack;
	ApplicationRepository appRepo;
	ResourceRepository resourceRepo;
	
	private boolean succeeded = true;
	boolean isDone = false; 
	private Logger logger = Logger.getLogger(UpdateCFDetails.class.getName());
	
	public void init(Workflow workflow, Application app, ApplicationRepository appRepo, ResourceRepository resourceRepo){
		setWorkflow(workflow);
		stack = (Stack) getVariable(Stack.class.getName());
		this.app = app;
		this.appRepo = appRepo;
		this.resourceRepo = resourceRepo;
	}
	
	@Override
	public void perform() {
		// Do nothing
	}

	@Override
	public boolean isDone() {
		List<com.amazonaws.services.cloudformation.model.Stack> awsStacks = StackUtil.getInstance().getStackDetails(stack.getAwsStackid());
		if (awsStacks == null || awsStacks.size() == 0) {
			succeeded = false;
			app.setStatus(ApplicationStatus.INSTANCE_CREATION_FAILED.getStatus());
			isDone = true;
			logger.log(Level.INFO, "stack creation failed for " + app.getAppId());
		} else {
			com.amazonaws.services.cloudformation.model.Stack awsStack = awsStacks.get(0);
			if (awsStack.getStackStatus().equalsIgnoreCase(StackStatus.CREATE_COMPLETE.toString())) {
				List<StackResource> resources = StackUtil.getInstance().getStackInstances(stack.getName() + "-" + app.getAppId());
				List<Resource> resourceList = new ArrayList<>();
				for (StackResource resource : resources) {
					if (resource.getLogicalResourceId().equalsIgnoreCase("WebServerGroup")) {
						List<Resource> resourceListForResource = getInstanceDetailsFromEC2(resource);
						int instanceSize = resourceListForResource.size();
						app.setInstCount(instanceSize);
						resourceList.addAll(resourceListForResource);
					} else if ("EC2ElasticLoadBalancer".equalsIgnoreCase(resource.getLogicalResourceId())) {
						Resource inst = new Resource();
						inst.setPhysicalResourceId(resource.getPhysicalResourceId());
						inst.setLogicalResourceId(resource.getLogicalResourceId());
						inst.setDescription(resource.getDescription());
						inst.setStatusReason(resource.getResourceStatusReason());
						inst.setResourceType(resource.getResourceType());
						inst.setStatus(resource.getResourceStatus());
						inst.setInstanceType("LOAD_BALANCER");
						LoadBalancer lb = EC2Util.getInstance().getELBDetails(inst.getPhysicalResourceId());
						if (lb != null) {
							inst.setPublicDns(lb.getDNSName());
							inst.setInstanceId(lb.getLoadBalancerName());
						}
						resourceList.add(inst);
					} else {
						Resource inst = new Resource();
						inst.setPhysicalResourceId(resource.getPhysicalResourceId());
						inst.setLogicalResourceId(resource.getLogicalResourceId());
						inst.setDescription(resource.getDescription());
						inst.setStatusReason(resource.getResourceStatusReason());
						inst.setResourceType(resource.getResourceType());
						inst.setStatus(resource.getResourceStatus());
						resourceList.add(inst);
					}
				}
				app.setInstances(resourceList);
				succeeded = true;
				app.setStatus(ApplicationStatus.INSTANCE_LAUNCHED.getStatus());
				isDone = true;
				logger.log(Level.INFO, "stack created successfully for " + app.getAppId());
			} else if (awsStack.getStackStatus().equals(StackStatus.CREATE_FAILED.toString()) || awsStack.getStackStatus().equals(StackStatus.ROLLBACK_COMPLETE.toString()) ||
					awsStack.getStackStatus().equals(StackStatus.ROLLBACK_FAILED.toString()) ) {
				succeeded = false;
				app.setStatus(ApplicationStatus.INSTANCE_CREATION_FAILED.getStatus());
				isDone = true;
				logger.log(Level.INFO, "stack creation failed for " + app.getAppId());
			} else {
				isDone = false;
			}
		}
		if(isDone){
			appRepo.save(app);
			List<Resource> resources = app.getInstances();
			if (resources != null && resources.size() > 0) {
				for(Resource res : resources) {
					res.setAppName(app.getAppId());
					resourceRepo.save(res);
				}
			}
			return true;
		} else{
			return false;
		}
	}

	private List<Resource> getInstanceDetailsFromEC2(StackResource resource) {
		List<com.amazonaws.services.autoscaling.model.Instance> instanceList = EC2Util.getInstance().getInstanceDetailOfASG(resource.getPhysicalResourceId());
		List<Resource> resourceList = new ArrayList<>();
		for (com.amazonaws.services.autoscaling.model.Instance instance : instanceList) {
			Resource inst = new Resource();
			inst.setPhysicalResourceId(resource.getPhysicalResourceId());
			inst.setLogicalResourceId(resource.getLogicalResourceId());
			inst.setDescription(resource.getDescription());
			inst.setStatusReason(resource.getResourceStatusReason());
			inst.setResourceType(resource.getResourceType());
			inst.setInstCount(1);
			
			Instance ec2Instance = EC2Util.getInstance().getInstanceDetailFromEC2(instance.getInstanceId());
			inst.setInstanceId(ec2Instance.getInstanceId());
			inst.setImageId(ec2Instance.getImageId());
			inst.setStatus(ec2Instance.getState().toString());
			inst.setInstSize(ec2Instance.getInstanceType());
			inst.setInstanceType(ec2Instance.getInstanceType());
			inst.setPrivateIp(ec2Instance.getPrivateIpAddress());
			inst.setPrivateDns(ec2Instance.getPrivateDnsName());
			inst.setPublicIp(ec2Instance.getPublicIpAddress());
			inst.setPublicDns(ec2Instance.getPublicDnsName());
			
			resourceList.add(inst);
		}
		return resourceList;
	}

	@Override
	public boolean hasSucceeded() {
		return succeeded;
	}

}
