package com.gw.workflow.task;

import java.io.StringWriter;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.util.AppConstants;
import com.gw.util.aws.AmazonSESUtil;
import com.gw.util.workflow.Task;

public class SendAutoShutDownNotification extends Task {
	
	private static Logger logger = Logger.getLogger(SendUserPasswordNotification.class.getName());
	private boolean isDone = false;
	private boolean succeeded = true;
	private Stack stack = null;
	private User user = null;

	public SendAutoShutDownNotification(Stack stack, User user) {
		this.user = user;
		this.stack = stack;
	
	}
	
	public void NotifyEmail() {
		AmazonSESUtil ses = new AmazonSESUtil();
		if(ses.sendEmail(getBody(stack, user), user.getEmail(), "Your stack has been autoshutdown")){
			logger.log(Level.INFO,"Email has been triggered");
		} else {
			logger.log(Level.INFO,"Email has not been sent");
		}
		isDone = true;
	}
	
	/*
	 * Calls the VM file to implement the email template for SES
	 */
	public static String getBody(Stack stack,User user) {
		VelocityEngine velocityEngine = new VelocityEngine();
		Properties p = new Properties();
		p.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		p.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		velocityEngine.init(p);
		try{
			VelocityContext velocityContext = new VelocityContext();
			velocityContext.put("stack", stack);
			velocityContext.put("user", user);
			velocityContext.put("date",AppConstants.dateFormat.format(Calendar.getInstance().getTime()));
			velocityContext.put("stackCreatedDate", AppConstants.dateFormat.format(stack.getCreatedDate()));
			Template template = velocityEngine.getTemplate("templates/AutoShutDownNotifier.vm");
			StringWriter stringWriter = new StringWriter();
			template.merge(velocityContext, stringWriter);
			return stringWriter.toString();
		}catch(Exception e){
			e.printStackTrace();
			logger.log(Level.INFO," Message body not built");
			return null;
		}
	}

	@Override
	public void perform() {
		NotifyEmail();		
	}
	
	@Override
	public boolean isDone() {
		return isDone;
	}

	@Override
	public boolean hasSucceeded() {
		return succeeded;
	}

}