package com.gw.workflow.task;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import com.gw.model.AppMetaData;
import com.gw.model.Application;
import com.gw.model.Stack;
import com.gw.model.status.ApplicationStatus;
import com.gw.model.status.BuildStatus;
import com.gw.repository.AppMetaDataRepository;
import com.gw.repository.ApplicationRepository;
import com.gw.util.AppConstants;
import com.gw.util.FileUtil;
import com.gw.util.jenkins.BuildResponse;
import com.gw.util.jenkins.JenkinsUtility;
import com.gw.util.workflow.Task;
import com.gw.util.workflow.Workflow;

public class TriggerJenkinsCodeDeployBuild extends Task {

	private Application application;
	private Stack stack;
	private String queueItemUrl = null;
	private String nextBuild = null;
	private String jobTriggered = null;
	private boolean isDone = false;
	private boolean isSucceded = false;
	private Logger logger = Logger.getLogger(TriggerJenkinsCodeDeployBuild.class.getName());
	
	AppMetaDataRepository appMetaDataDao;
	ApplicationRepository appRepo;
	
	public TriggerJenkinsCodeDeployBuild() {
		super();
	}
	
	public void init(Workflow flow, Application app, AppMetaDataRepository appMetaDataDao, ApplicationRepository appRepo, Stack stack) {
		setWorkflow(flow);
		this.application = app;
		this.appMetaDataDao = appMetaDataDao;
		this.appRepo = appRepo;
		this.stack = stack;
	}
	
	@Override
	public void perform() {
		HashMap<String, String> variables = new HashMap<>();
		variables.put("AppCode", application.getAppId());
		variables.put("CodeDeployApplication", stack.getName());
		variables.put("recreateDb", application.isNewDB() ? "yes" : "no");
		if(application.isNewBuild() || application.getBuildId() == null){
			jobTriggered = AppConstants.DEP_UCD;
			nextBuild = JenkinsUtility.getInstance().getNextBuild(AppConstants.DEP_UCD);
			queueItemUrl = JenkinsUtility.getInstance().triggerJobWithParams(AppConstants.DEP_UCD, variables);
			logger.log(Level.INFO, "Build trigger initiated with the queue no. " + queueItemUrl + " for " + application.getAppId());
		}else{
			variables.put("JDBCUrl", AppConstants.BUILD_GREEN_DEV_JDBC_URL);
			Map<String, String> values = FileUtil.readSpecificStringFromStream(JenkinsUtility.getInstance().fetchConsoleOutputForBuildAsStream(AppConstants.DEP_UCD, application.getBuildId()));
			if(values.get("RevisionId") != null && values.get("etag") != null ){
				variables.put("etag", values.get("etag"));
				variables.put("RevisionId", values.get("RevisionId"));
				jobTriggered = AppConstants.DEP_UCDP;
				nextBuild = JenkinsUtility.getInstance().getNextBuild(AppConstants.DEP_UCDP);
				queueItemUrl = JenkinsUtility.getInstance().triggerJobWithParams(AppConstants.DEP_UCDP, variables);
				logger.log(Level.INFO, "Build-" + application.getBuildId() + " trigger initiated for " + application.getAppId());
			}
		}
	}

	@Override
	public boolean isDone() {
		try{
			BuildResponse queueResp = JenkinsUtility.getInstance().checkQueueBuild(queueItemUrl);
			if(queueResp.getStatusCode() != 404 && new JSONObject(queueResp.getResponseBody()).getBoolean("buildable")){
				application.setStatus(ApplicationStatus.BUILD_IN_QUEUE.getStatus());
			}else{
				BuildResponse buildResp = JenkinsUtility.getInstance().checkBuildStatus(jobTriggered, nextBuild);
				if(buildResp.getStatusCode() != 404){
					JSONObject buildObj = new JSONObject(buildResp.getResponseBody());
					if(buildObj != null && buildObj.getBoolean("building")){
						application.setStatus(ApplicationStatus.BUILD_IN_PROGRESS.getStatus());
					}else{
						if(buildObj != null && buildObj.getString("result") != null ){
							isDone = true;
							isSucceded = true;
							if(application.isNewBuild()){
								application.setBuildId(nextBuild);
							}
							application.setStatus("BUILD_" + buildObj.getString("result"));
							if(BuildStatus.SUCCESS.getStatus().equals(buildObj.getString("result"))){
								if(application.isNewBuild()){
									Iterator<AppMetaData> iter = appMetaDataDao.findAll().iterator();
									while(iter.hasNext()) {
										AppMetaData appMetaData = iter.next();
										if (appMetaData.getId().equals(application.getAppId())) {
											List<String> buildList = appMetaData.getBuildInfo();
											buildList.add(nextBuild);
											appMetaData.setBuildInfo(buildList);
											appMetaDataDao.save(appMetaData);
										}
									}
								}
								application.setStatus(ApplicationStatus.BUILD_SUCCEDED.getStatus());
								logger.log(Level.INFO, "Build-" + application.getBuildId() + " triggered succesfully for " + application.getAppId());
							}
						}
					}
				}else{
					application.setStatus(ApplicationStatus.BUILD_IN_QUEUE.getStatus());
				}
			}
			appRepo.save(application);
		}catch(JSONException e){
			logger.log(Level.INFO, "No response yet for the triggered build");
		}
		return isDone;
	}

	@Override
	public boolean hasSucceeded() {
		return isSucceded;
	}

}
