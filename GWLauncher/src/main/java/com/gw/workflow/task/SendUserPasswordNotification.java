package com.gw.workflow.task;

import java.io.StringWriter;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;

import com.gw.model.User;
import com.gw.repository.StackRepository;
import com.gw.util.aws.AmazonSESUtil;
import com.gw.util.workflow.Task;

public class SendUserPasswordNotification extends Task {
	
	private Logger logger = Logger.getLogger(SendUserPasswordNotification.class.getName());
	private boolean isDone = false;
	private boolean succeeded = true;
	private User user = null;
	private String pwd = null;
	
	@Autowired
	StackRepository stackRepo;
	
	public SendUserPasswordNotification(User user, String pwd) {
		this.user = user;
		this.pwd = pwd;
	}
	
	public void NotifyEmail() {
		AmazonSESUtil ses = new AmazonSESUtil();
		if(ses.sendEmail(getBody(), user.getEmail(), "Password reset")){
			logger.log(Level.INFO,"Email has been triggered");
		} else {
			logger.log(Level.INFO,"Email has not been sent");
		}
		isDone = true;
	}
	
	/*
	 * Calls the VM file to implement the email template for SES
	 */
	public String getBody() {
		VelocityEngine velocityEngine = new VelocityEngine();
		Properties p = new Properties();
		p.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		p.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		velocityEngine.init(p);
		VelocityContext velocityContext = new VelocityContext();
		velocityContext.put("user", this.user);
		velocityContext.put("pwd", this.pwd);
		Template template = velocityEngine.getTemplate("templates/UserCreateNotification.vm");
		StringWriter stringWriter = new StringWriter();
		template.merge(velocityContext, stringWriter);
		return stringWriter.toString();
	}

	@Override
	public void perform() {
		NotifyEmail();		
	}
	
	@Override
	public boolean isDone() {
		// TODO Auto-generated method stub
		return isDone;
	}

	@Override
	public boolean hasSucceeded() {
		// TODO Auto-generated method stub
		return succeeded;
	}

}