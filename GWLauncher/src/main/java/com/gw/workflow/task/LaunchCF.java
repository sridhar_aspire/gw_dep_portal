package com.gw.workflow.task;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.amazonaws.services.cloudformation.model.StackResource;
import com.amazonaws.services.cloudformation.model.StackStatus;
import com.gw.model.Application;
import com.gw.model.Stack;
import com.gw.model.status.ApplicationStatus;
import com.gw.model.status.StackResourceType;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.SettingRepository;
import com.gw.repository.StackRepository;
import com.gw.util.AppConstants;
import com.gw.util.aws.S3Util;
import com.gw.util.aws.StackUtil;
import com.gw.util.workflow.Task;
import com.gw.util.workflow.Workflow;

public class LaunchCF extends Task {
	
	private Stack stack;
	private ApplicationRepository appRepo;
	private SettingRepository settingRepo; 
	private StackRepository stackRepo;
	private boolean succeeded = true;
	private HashMap<String, String> stackParams = new HashMap<String, String>();
	boolean isDone = false; 
	private Logger logger = Logger.getLogger(LaunchCF.class.getName());
	private ResourceRepository resourceRepo;
	
	public void init(Workflow workflow, Stack stack, ApplicationRepository appRepo, ResourceRepository resourceRepo, StackRepository stackRepo, SettingRepository settingRepo){
		setWorkflow(workflow);
		this.stack = stack;
		this.appRepo = appRepo;
		this.stackRepo = stackRepo;
		this.settingRepo = settingRepo;
		this.resourceRepo = resourceRepo;
	}
	
	@Override
	public void perform() {
		try {
			File currentDirectory = new File(".");
			logger.log(Level.SEVERE, "CURRENT PATH : " + currentDirectory.getAbsolutePath());
			String cfTemplate = stack.getEnvType() ? S3Util.getInstance().updateCFTemplate(AppConstants.CF_FILE_NAME) : 
				S3Util.getInstance().updateCFTemplate(AppConstants.CF_SINGLE_INS_FILE_NAME);
			stackParams.put("Duration", String.valueOf(stack.getPlannedUseDays()));
			double costEstimate = StackUtil.getInstance().getEstimatedMonthlyCost();
			stack.setEstimatedMonthlyCost(costEstimate);
			stack.getApplications().forEach(app -> {
				configureTemplate(app);
				appRepo.save(app);
			});
			String stackId = StackUtil.getInstance().createStack(cfTemplate, stack.getName(), stackParams);
			stack.setAwsStackid(stackId);
			stackRepo.save(stack);
		} catch (Exception e) {
			e.printStackTrace();
			isDone = true;
			succeeded = false;
			updateStackStatus(StackStatus.CREATE_FAILED.toString(), ApplicationStatus.INSTANCE_CREATION_FAILED.getStatus());
			logger.log(Level.SEVERE, "stack creation failed");
		}
	}

	private void updateStackStatus(String stackStatus, String appStatus) {
		stack.setStatus(stackStatus);
		stack.setUpdatedDate(Calendar.getInstance().getTime());
		stack.getApplications().forEach(app -> {
			app.setStatus(appStatus);
			app.setUpdatedDate(Calendar.getInstance().getTime());
			appRepo.save(app);
		});
		stackRepo.save(stack);
	}

	private void configureTemplate(Application app){
		app.setInstSize(settingRepo.getDefaultInstanceSizeByAppId(app.getAppId().toUpperCase() + "|" + AppConstants.DEFAULT_INSTANCE_SIZE).getValue());
		stackParams.put(app.getAppId().toUpperCase() + "AppName", app.getAppId());
		if(stack.getEnvType()){
			stackParams.put(app.getAppId().toUpperCase() + "InstanceCount", String.valueOf(app.getInstCount()));
		}
		app.setDeploymentGroup(stack.getName() + "-" + app.getAppId());
		app.setInstanceName(stack.getName() + "_" + app.getAppId());
		app.setDnsSuffix(stack.getName() + "-" + app.getAppId());
	}

	/*private String getAppPort(Application app) {
		if (app.getAppId().equalsIgnoreCase("ab")){
			return "8280";
		}else if(app.getAppId().equalsIgnoreCase("bc")){
			return "8580";
		}else if(app.getAppId().equalsIgnoreCase("cc")){
			return "8080";
		}else if(app.getAppId().equalsIgnoreCase("pc")){
			return "8180";
		}else{
			return "8280";
		}
	}*/

	@Override
	public boolean isDone() {
		List<com.amazonaws.services.cloudformation.model.Stack> awsStacks = StackUtil.getInstance().getStackDetails(stack.getAwsStackid());
		if (awsStacks == null || awsStacks.size() == 0) {
			succeeded = false;
			isDone = true;
			updateStackStatus(StackStatus.CREATE_FAILED.toString(), ApplicationStatus.INSTANCE_CREATION_FAILED.getStatus());
			logger.log(Level.INFO, "stack creation failed");
		} else {
			com.amazonaws.services.cloudformation.model.Stack awsStack = awsStacks.get(0);
			if (StackStatus.CREATE_COMPLETE.toString().equals(awsStack.getStackStatus())) {
				List<StackResource> resources = StackUtil.getInstance().getStackInstances(stack.getName());
				stack.getApplications().forEach(app -> {
					StackUtil.getInstance().mapResourceWithApp(app, resources.stream().filter(r ->  r.getLogicalResourceId().startsWith(app.getAppId().toUpperCase()) || 
							r.getResourceType().equals(StackResourceType.APPLICATION.toString())).collect(Collectors.toList()));
					app.setStatus(ApplicationStatus.INSTANCE_LAUNCHED.getStatus());
					app.setUpdatedDate(Calendar.getInstance().getTime());
					HashMap<String, String> urlMap = StackUtil.getInstance().getStackOutputs(stack.getName());
                    for (String appName : urlMap.keySet()) {
                          if(appName.contains(app.getAppId().toUpperCase())){
                                app.setAppURL(urlMap.get(appName));
                          }
                    }
					appRepo.save(app);
					app.getInstances().forEach(res -> {
						res.setAwsStackId(stack.getAwsStackid());
						res.setAppId(app.getId());
						res.setCreatedDate(Calendar.getInstance().getTime());
						res.setUpdatedDate(Calendar.getInstance().getTime());
						resourceRepo.save(res);
					});
				});
				stack.setUpdatedDate(Calendar.getInstance().getTime());
				stack.setStatus(StackStatus.CREATE_COMPLETE.toString());
				stackRepo.save(stack);
				isDone = true;
				succeeded = true;
				logger.log(Level.INFO, "stack created successfully ");
			} else if (awsStack.getStackStatus().equals(StackStatus.CREATE_FAILED.toString()) || awsStack.getStackStatus().equals(StackStatus.ROLLBACK_COMPLETE.toString()) ||
					awsStack.getStackStatus().equals(StackStatus.ROLLBACK_FAILED.toString()) ) {
				succeeded = false;
				isDone = true;
				updateStackStatus(StackStatus.CREATE_FAILED.toString(), ApplicationStatus.INSTANCE_CREATION_FAILED.getStatus());
				logger.log(Level.INFO, "stack creation failed");
			} else {
				isDone = false;
			}
		}
		if(isDone){
			return true;
		} else{
			return false;
		}
	}

	@Override
	public boolean hasSucceeded() {
		return succeeded;
	}

}
