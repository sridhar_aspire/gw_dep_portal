package com.gw.workflow.task;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.gw.dao.StackDAO;
import com.gw.model.Application;
import com.gw.model.Stack;
import com.gw.model.status.ApplicationStatus;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.StackRepository;
import com.gw.util.workflow.Task;

public class UpdateStackStatus extends Task {
	
	private boolean isDone = false;
	private boolean succeeded = true;
	
	StackRepository stackRepo;
	ApplicationRepository appRepo;
	ResourceRepository resourceRepo;
	
	private Logger logger = Logger.getLogger(UpdateStackStatus.class.getName());
	@Override
	public void perform() {
		setStackStatus();
	}
	
	public void init(StackRepository stackRepo, ApplicationRepository appRepo, ResourceRepository resourceRepo) {
		this.stackRepo = stackRepo;
		this.appRepo = appRepo;
		this.resourceRepo = resourceRepo;
	}

	private void setStackStatus() {
		Stack stack = (Stack) getVariable(Stack.class.getName());
		StackDAO stackDao = new StackDAO();
		stack = stackDao.getStackDetail(stack.getId(), stackRepo);
		logger.log(Level.INFO, "stack status check initiated for " + stack.getId());
		boolean appFailed = false;
		boolean appTerminated = false;
		int failureCount = 0;
		int terminatedCount = 0;
		int totalLiveInstanceCount = 0;
		for (Application app : stack.getApplications()) {
			if (app.getStatus().equals(ApplicationStatus.INSTANCE_CREATION_FAILED.getStatus())) {
				appFailed = true;
				failureCount++;
			} else if (app.getStatus().equals(ApplicationStatus.TERMINATED.getStatus())) {
				appTerminated = true;
				terminatedCount++;
			} else if (app.getStatus().equalsIgnoreCase(ApplicationStatus.BUILD_IN_PROGRESS.getStatus())
					|| app.getStatus().equalsIgnoreCase(ApplicationStatus.BUILD_SUCCEDED.getStatus())
					|| app.getStatus().equalsIgnoreCase(ApplicationStatus.INSTANCE_LAUNCHED.getStatus())
					) {
				totalLiveInstanceCount += app.getInstCount();
			}
		}
		stack.setInstanceCount(totalLiveInstanceCount);
		if (appFailed && (failureCount == stack.getApplications().size())) {
			stack.setStatus("FAILED");
		} else if (appFailed && (failureCount < stack.getApplications().size())) {
			stack.setStatus("PARTIAL_FAILURE");
		} else if (appTerminated && (terminatedCount == stack.getApplications().size())) {
			stack.setStatus("TERMINATED");
		} else if (appTerminated && (terminatedCount < stack.getApplications().size())) {
			stack.setStatus("PARTIALLY_TERMINATED");
		} else {
			stack.setStatus("LAUNCHED");
		}
		stackDao.updateStackStatus(stackRepo, appRepo, resourceRepo, stack, null, null);
		setVariable(stack);
		isDone = true;
	}

	@Override
	public boolean isDone() {
		return isDone;
	}

	@Override
	public boolean hasSucceeded() {
		return succeeded;
	}

}
