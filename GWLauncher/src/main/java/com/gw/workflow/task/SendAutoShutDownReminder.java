package com.gw.workflow.task;

import java.io.StringWriter;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;

import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.repository.StackRepository;
import com.gw.util.aws.AmazonSESUtil;
import com.gw.util.workflow.Task;

public class SendAutoShutDownReminder extends Task {
	
	private static Logger logger = Logger.getLogger(SendAutoShutDownReminder.class.getName());
	private boolean isDone = false;
	private boolean succeeded = true;
	private Stack stack = null;
	private User user = null;
	
	@Autowired
	StackRepository stackRepo;
	
	public SendAutoShutDownReminder(Stack stack, User user) {
		this.stack = stack;
		this.user = user;
	}

	public void NotifyEmail() {
		AmazonSESUtil ses = new AmazonSESUtil();
		if(ses.sendEmail(getBody(stack, user), user.getEmail(), "Reminder for Auto shutdown")){
			logger.log(Level.INFO,"Email has been triggered");
		} else {
			logger.log(Level.INFO,"Email has not been sent");
		}
		isDone = true;
	}
	
	/*
	 * Calls the VM file to implement the email template for SES
	 */
	public static String getBody(Stack stack,User user) {
		VelocityEngine velocityEngine = new VelocityEngine();
		Properties p = new Properties();
		p.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		p.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		velocityEngine.init(p);
		VelocityContext velocityContext = new VelocityContext();
		velocityContext.put("stack", stack);
		velocityContext.put("user", user);
		try{
			Date current_date = new Date();
			long count = (current_date.getTime() - stack.getCreatedDate().getTime())/(24*60*60*1000);
			velocityContext.put("count", count);
			long tillTime = ((stack.getCreatedDate().getTime())+(count* 24 * 60 * 60 * 1000));
			Date tillDate = new Date(tillTime);
		    Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		    format.format(tillDate);
			velocityContext.put("date", tillDate);
			Template template = velocityEngine.getTemplate("templates/AutoShutDownReminder.vm");
			StringWriter stringWriter = new StringWriter();
			template.merge(velocityContext, stringWriter);
			return stringWriter.toString();
		}catch(Exception e){
			e.printStackTrace();
			logger.log(Level.INFO, "Template not created");
			return null;
		}
	}

	@Override
	public void perform() {
		NotifyEmail();		
	}
	
	@Override
	public boolean isDone() {
		// TODO Auto-generated method stub
		return isDone;
	}

	@Override
	public boolean hasSucceeded() {
		// TODO Auto-generated method stub
		return succeeded;
	}

}