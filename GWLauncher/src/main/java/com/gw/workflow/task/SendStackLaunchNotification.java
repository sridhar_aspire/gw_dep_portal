package com.gw.workflow.task;

import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.gw.model.Application;
import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.util.aws.AmazonSESUtil;
import com.gw.util.aws.StackUtil;
import com.gw.util.workflow.Task;

public class SendStackLaunchNotification extends Task {
	
	private Logger logger = Logger.getLogger(SendStackLaunchNotification.class.getName());
	private boolean isDone = false;
	private boolean succeeded = true;
	private Stack stack = null;
	private User user = null;
	private Application app = null;
	public SendStackLaunchNotification(Stack stack, User user, Application app) {
		this.stack = stack;
		this.user = user;
		this.app = app;
	}
	
	public void NotifyEmail() {
		AmazonSESUtil ses = new AmazonSESUtil();
		String status = app == null ? " Launched Successfully" : " Modified Successfully";
		if(ses.sendEmail(getBody(stack, user, status), user.getEmail(), "Stack " + stack.getName() + status )){
			logger.log(Level.INFO,"Email has been triggered");
			stack.setNotifiedDate(new Date());
		} else {
			logger.log(Level.INFO,"Email has nt been sent");
		}
		isDone = true;
	}
	
	/*
	 * Calls the VM file to implement the email template for SES
	 */
	public String getBody(Stack stack,User user, String status) {
		VelocityEngine velocityEngine = new VelocityEngine();
		Properties p = new Properties();
		p.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		p.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		velocityEngine.init(p);
		VelocityContext velocityContext = new VelocityContext();
		velocityContext.put("stack", stack);
		velocityContext.put("status", status);
		velocityContext.put("user", user);
		velocityContext.put("appURL", getAppURL());
		Template template = velocityEngine.getTemplate("templates/SESNotificationMessage.vm");
		StringWriter stringWriter = new StringWriter();
		template.merge(velocityContext, stringWriter);
		return stringWriter.toString();
	}

	private String getAppURL() {
		StringBuilder urlContent = new StringBuilder("");
		HashMap<String, String> urlMap = StackUtil.getInstance().getStackOutputs(stack.getName());
		for (String appName : urlMap.keySet()) {
			urlContent.append("<tr><th>");
			urlContent.append(appName + ": ");
			urlContent.append("</th><td>");
			urlContent.append(urlMap.get(appName));
			urlContent.append("</td></tr>");
		}
		return urlContent.toString();
	}

	@Override
	public void perform() {
		logger.log(Level.INFO, "The send email started: ");
		NotifyEmail();		
	}
	
	@Override
	public boolean isDone() {
		// TODO Auto-generated method stub
		return isDone;
	}

	@Override
	public boolean hasSucceeded() {
		// TODO Auto-generated method stub
		return succeeded;
	}

}