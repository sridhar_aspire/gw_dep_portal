package com.gw.workflow.task;

import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.services.cloudformation.model.StackStatus;
import com.gw.dao.StackDAO;
import com.gw.model.Stack;
import com.gw.model.status.ApplicationStatus;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.StackRepository;
import com.gw.util.aws.StackUtil;
import com.gw.util.workflow.Task;
import com.gw.workflow.TerminateStackWorkFlow;

public class TerminateCF extends Task {
	
	private Stack stack;
	private ApplicationRepository appRepo;
	private StackRepository stackRepo;
	private ResourceRepository resourceRepo;
	private StackDAO stackDAO;
	private boolean isDone = false;
	private boolean succeeded = true;
	private Logger logger = Logger.getLogger(TerminateCF.class.getName());

	public void init(TerminateStackWorkFlow workFlow, ApplicationRepository appRepo, StackRepository stackRepo, ResourceRepository resourceRepo, Stack stack){
		setWorkflow(workFlow);
		this.appRepo = appRepo;
		this.stackRepo = stackRepo;
		this.resourceRepo = resourceRepo;
		this.stack = stack;
		this.stackDAO = new StackDAO();
	}
	
	@Override
	public void perform() {
		terminateStack();
	}

	private void terminateStack() {
		logger.log(Level.INFO, stack.getName() + " Termination started ");
		StackUtil.getInstance().deleteStack(stack.getName());
	}

	@Override
	public boolean isDone() {
		List<com.amazonaws.services.cloudformation.model.Stack> awsStacks = StackUtil.getInstance().getStackDetails(stack.getAwsStackid());
		if (awsStacks == null || awsStacks.size() == 0) {
			succeeded = false;
			stackDAO.updateStackStatus(stackRepo, appRepo, resourceRepo, stack, ApplicationStatus.TERMINATION_FAILED.toString(), null);
			isDone = true;
		} else {
			com.amazonaws.services.cloudformation.model.Stack awsStack = awsStacks.get(0);
			if (StackStatus.DELETE_COMPLETE.toString().equals(awsStack.getStackStatus())) {
				succeeded = true;
				stackDAO.updateStackStatus(stackRepo, appRepo, resourceRepo, stack, ApplicationStatus.TERMINATED.getStatus(), Calendar.getInstance().getTime());
				isDone = true;
				logger.log(Level.INFO, stack.getName() + " deleted successfully");
			} else if (StackStatus.DELETE_FAILED.toString().equals(awsStack.getStackStatus())) {
				succeeded = false;
				stackDAO.updateStackStatus(stackRepo, appRepo, resourceRepo, stack, ApplicationStatus.TERMINATION_FAILED.toString(), null);
				isDone = true;
			} else {
				isDone = false;
			}
		}
		if(isDone){
			return true;
		} else{
			return false;
		}
	}

	@Override
	public boolean hasSucceeded() {
		return succeeded;
	}

}
