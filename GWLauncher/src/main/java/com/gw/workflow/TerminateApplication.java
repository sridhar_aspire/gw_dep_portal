package com.gw.workflow;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.services.cloudformation.model.StackStatus;
import com.gw.model.Application;
import com.gw.model.Stack;
import com.gw.model.status.ApplicationStatus;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.StackRepository;
import com.gw.util.aws.StackUtil;
import com.gw.util.workflow.Task;

public class TerminateApplication extends Task{
	
	private Application app;
	private Stack stack;
	private ApplicationRepository appRepo;
	private ResourceRepository resourceRepo;
	private StackRepository stackRepo;
	private boolean succeeded = true;
	boolean isDone = false; 
	private HashMap<String, String> stackParams = new HashMap<String, String>();;
	private Logger logger = Logger.getLogger(TerminateApplication.class.getName());
	
	public void init(TerminateAppWorkFlow workFlow, Stack stack, Application app, ApplicationRepository appRepo,
			ResourceRepository resourceRepo, StackRepository stackRepo) {
		setWorkflow(workFlow);
		this.app = app;
		this.stack = stack;
		this.appRepo = appRepo;
		this.stackRepo = stackRepo;
		this.resourceRepo = resourceRepo;
	}
	
	@Override
	public void perform() {
		try{
			String cfTemplateContent = StackUtil.getInstance().getExistingCFTemplate(stack.getName());
			StackUtil.getInstance().getStackDetails(stack.getName()).get(0).getParameters().forEach(p ->{
				stackParams.put(p.getParameterKey(), p.getParameterValue());
			});
			stackParams.put(app.getAppId().toUpperCase() + "AppName", "");
			StackUtil.getInstance().updateStack(cfTemplateContent, stack.getName(), stackParams);
		} catch (Exception e) {
			//TODO after this catch thread should terminate and stop other sequence of tasks.
			e.printStackTrace();
			isDone = true;
			succeeded = false;
			updateStackStatus(com.gw.model.status.StackStatus.UPDATE_FAILED.getStatus(), ApplicationStatus.TERMINATION_FAILED.getStatus());
			logger.log(Level.SEVERE, "stack update failed");
		}
	}

	private void updateStackStatus(String stackStatus, String appStatus) {
		stack.setStatus(stackStatus);
		stack.setUpdatedDate(Calendar.getInstance().getTime());
		stackRepo.save(stack);
		app.setUpdatedDate(Calendar.getInstance().getTime());
		app.setStatus(appStatus);
		appRepo.save(app);
	}

	@Override
	public boolean isDone() {
		//TODO need to handle exception in the done part.
		List<com.amazonaws.services.cloudformation.model.Stack> awsStacks = StackUtil.getInstance().getStackDetails(stack.getAwsStackid());
		if (awsStacks == null || awsStacks.size() == 0) {
			succeeded = false;
			isDone = true;
			updateStackStatus(com.gw.model.status.StackStatus.UPDATE_FAILED.getStatus(), ApplicationStatus.TERMINATION_FAILED.getStatus());
			logger.log(Level.INFO, "stack update failed");
		} else {
			com.amazonaws.services.cloudformation.model.Stack awsStack = awsStacks.get(0);
			if (StackStatus.UPDATE_COMPLETE.toString().equals(awsStack.getStackStatus())) {
				app.setStatus(ApplicationStatus.TERMINATED.getStatus());
				app.setTerminatedDate(Calendar.getInstance().getTime());
				app.setUpdatedDate(Calendar.getInstance().getTime());
				appRepo.save(app);
				app.getInstances().forEach(res -> {
					res.setTerminatedDate(Calendar.getInstance().getTime());
					res.setUpdatedDate(Calendar.getInstance().getTime());
					res.setStatus(ApplicationStatus.TERMINATED.getStatus());
					resourceRepo.save(res);
				});
				stack.setUpdatedDate(Calendar.getInstance().getTime());
				stack.setStatus(StackStatus.UPDATE_COMPLETE.toString());
				stackRepo.save(stack);
				isDone = true;
				succeeded = true;
				logger.log(Level.INFO, "stack updated successfully ");
			} else if (awsStack.getStackStatus().equals(StackStatus.UPDATE_ROLLBACK_COMPLETE.toString()) || 
					awsStack.getStackStatus().equals(StackStatus.UPDATE_ROLLBACK_FAILED.toString())) {
				succeeded = false;
				isDone = true;
				updateStackStatus(com.gw.model.status.StackStatus.UPDATE_FAILED.getStatus(), ApplicationStatus.TERMINATION_FAILED.getStatus());
				logger.log(Level.INFO, "stack update failed");
			} else {
				isDone = false;
			}
		}
		if(isDone){
			return true;
		} else{
			return false;
		}
	}

	@Override
	public boolean hasSucceeded() {
		return succeeded;
	}

}
