package com.gw.workflow;

import com.gw.model.Application;
import com.gw.model.Stack;
import com.gw.repository.AppMetaDataRepository;
import com.gw.repository.ApplicationRepository;
import com.gw.util.workflow.Workflow;
import com.gw.workflow.task.TriggerJenkinsCodeDeployBuild;

public class UpdateBuildWorkflow extends Workflow{
	
	public UpdateBuildWorkflow() {
		super();
	}
	
	public void initWorkflow(ApplicationRepository appRepo, AppMetaDataRepository appMetaDataRepo, Application application, Stack stack){
		TriggerJenkinsCodeDeployBuild codeDeployBuild = new TriggerJenkinsCodeDeployBuild();
		codeDeployBuild.init(this, application, appMetaDataRepo, appRepo, stack);
		addTask(codeDeployBuild);
	}
}
