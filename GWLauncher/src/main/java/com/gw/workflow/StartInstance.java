package com.gw.workflow;

import java.util.Calendar;

import com.amazonaws.services.ec2.model.InstanceStateName;
import com.gw.model.Resource;
import com.gw.repository.ResourceRepository;
import com.gw.util.aws.EC2Util;
import com.gw.util.workflow.Task;

public class StartInstance extends Task{

	private String instId;
	private ResourceRepository resRepo;
	private String stackId;
	private String appId;
	
	public StartInstance(String instanceId, ResourceRepository resourceRepo, String stackId, String appId) {
		setTask(this);
		this.instId = instanceId;
		this.appId = appId;
		this.stackId = stackId;
		this.resRepo = resourceRepo;
	}

	@Override
	public void perform() {
		EC2Util.getInstance().startInstance(instId);
	}

	@Override
	public boolean isDone() {
		if(InstanceStateName.Running.toString().equals(EC2Util.getInstance().getInstanceDetailFromEC2(instId).getState().getName())){
			Resource resource = resRepo.getEC2Instance(stackId, appId, instId);
			resource.setStatus(InstanceStateName.Running.toString());
			resource.setUpdatedDate(Calendar.getInstance().getTime());
			resRepo.save(resource);
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean hasSucceeded() {
		return true;
	}

}
