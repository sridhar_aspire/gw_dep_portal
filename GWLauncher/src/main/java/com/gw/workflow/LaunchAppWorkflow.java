package com.gw.workflow;

import com.gw.model.Application;
import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.repository.AppMetaDataRepository;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.SettingRepository;
import com.gw.repository.StackRepository;
import com.gw.util.workflow.Workflow;
import com.gw.workflow.task.SendStackLaunchNotification;
import com.gw.workflow.task.TriggerJenkinsCodeDeployBuild;

public class LaunchAppWorkflow extends Workflow {
	
	public LaunchAppWorkflow() {
		super();
	}
	
	public LaunchAppWorkflow initWorkFlow(Stack stack, Application app, AppMetaDataRepository appMetaDataRepository, StackRepository stackRepo, ApplicationRepository appRepo, ResourceRepository resourceRepo, SettingRepository settingRepo, User user) {
		ModifyCF modifyCF = new ModifyCF();
		modifyCF.init(this, stack, app, appRepo, resourceRepo, stackRepo, settingRepo);
		addTask(modifyCF);
		TriggerJenkinsCodeDeployBuild codeDeployBuild = new TriggerJenkinsCodeDeployBuild();
		codeDeployBuild.init(this, app, appMetaDataRepository, appRepo, stack);
		addTask(codeDeployBuild);
		SendStackLaunchNotification sendEmailTask = new SendStackLaunchNotification(stack, user, app);
		addTask(sendEmailTask);
		return this;
	}

}
