package com.gw.workflow;

import org.springframework.stereotype.Component;

import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.StackRepository;
import com.gw.util.workflow.Workflow;
import com.gw.workflow.task.SendAutoShutDownNotification;
import com.gw.workflow.task.TerminateCF;

@Component
public class TerminateStackWorkFlow extends Workflow {
	
	public TerminateStackWorkFlow() {
		super();
	}
	
	public TerminateStackWorkFlow initWorkFlow(Stack stack, StackRepository stackRepo, ApplicationRepository appRepo, ResourceRepository resourceRepo, User user, boolean isAutoShutdown) {
		TerminateCF terminateCF = new TerminateCF();
		terminateCF.init(this, appRepo, stackRepo, resourceRepo, stack);
		addTask(terminateCF);
		if(isAutoShutdown){
			SendAutoShutDownNotification notification = new SendAutoShutDownNotification(stack, user);
			addTask(notification);
		}
		return this;
	}

}
