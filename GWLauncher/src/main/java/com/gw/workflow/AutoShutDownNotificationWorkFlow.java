package com.gw.workflow;

import org.springframework.stereotype.Component;

import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.util.workflow.Workflow;
import com.gw.workflow.task.SendAutoShutDownNotification;

@Component
public class AutoShutDownNotificationWorkFlow extends Workflow {
	
	public AutoShutDownNotificationWorkFlow initWorkFlow(Stack stack, User user) {
		addInput(stack);
		SendAutoShutDownNotification sendEmailTask = new SendAutoShutDownNotification(stack, user);
		addTask(sendEmailTask);
		return this;
	}

}