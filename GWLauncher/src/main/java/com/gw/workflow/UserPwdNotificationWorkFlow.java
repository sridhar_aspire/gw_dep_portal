package com.gw.workflow;

import org.springframework.stereotype.Component;

import com.gw.model.User;
import com.gw.util.workflow.Workflow;
import com.gw.workflow.task.SendUserPasswordNotification;

@Component
public class UserPwdNotificationWorkFlow extends Workflow {
	
	public UserPwdNotificationWorkFlow initWorkFlow(User user, String pwd) {
		addInput(user);
		SendUserPasswordNotification sendEmailTask = new SendUserPasswordNotification(user, pwd);
		addTask(sendEmailTask);
		return this;
	}

}