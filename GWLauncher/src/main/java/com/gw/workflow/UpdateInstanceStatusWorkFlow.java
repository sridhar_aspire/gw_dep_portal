package com.gw.workflow;

import java.util.Collections;

import org.springframework.stereotype.Component;

import com.gw.model.Application;
import com.gw.model.Stack;
import com.gw.repository.AppMetaDataRepository;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.StackRepository;
import com.gw.util.ApplicationComparator;
import com.gw.util.workflow.Workflow;
import com.gw.workflow.task.UpdateCFDetails;
import com.gw.workflow.task.UpdateStackStatus;

@Component
public class UpdateInstanceStatusWorkFlow extends Workflow {
	
	public UpdateInstanceStatusWorkFlow() {
		super();
	}
	
	public UpdateInstanceStatusWorkFlow initWorkFlow(Stack stack, AppMetaDataRepository appMetaDataRepository, StackRepository stackRepo, ApplicationRepository appRepo, ResourceRepository resourceRepo, String userId) {
		addInput(stack);
		addInput("USER_ID", userId);
		Collections.sort(stack.getApplications(),new ApplicationComparator());
		for (Application app : stack.getApplications()) {
			UpdateCFDetails updateCFDetailsTask = new UpdateCFDetails();
			updateCFDetailsTask.init(this, app, appRepo, resourceRepo);
			addTask(updateCFDetailsTask);
		}
		UpdateStackStatus updateStackStatus = new UpdateStackStatus();
		updateStackStatus.init(stackRepo, appRepo, resourceRepo);
		updateStackStatus.setWorkflow(this);
		addTask(updateStackStatus);
		return this;
	}

}
