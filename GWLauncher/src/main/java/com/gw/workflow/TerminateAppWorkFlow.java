package com.gw.workflow;

import org.springframework.stereotype.Component;

import com.gw.model.Application;
import com.gw.model.Stack;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.StackRepository;
import com.gw.util.workflow.Workflow;

@Component
public class TerminateAppWorkFlow extends Workflow {
	
	public TerminateAppWorkFlow() {
		super();
	}
	
	public TerminateAppWorkFlow initWorkFlow(Stack stack, Application application, StackRepository stackRepo, ApplicationRepository appRepo, ResourceRepository resourceRepo) {
		TerminateApplication terminateApp = new TerminateApplication();
		terminateApp.init(this, stack, application, appRepo, resourceRepo, stackRepo);
		addTask(terminateApp);
		return this;
	}

}
