package com.gw.workflow;

import java.util.Collections;

import org.springframework.stereotype.Component;

import com.gw.model.Application;
import com.gw.model.Stack;
import com.gw.model.User;
import com.gw.repository.AppMetaDataRepository;
import com.gw.repository.ApplicationRepository;
import com.gw.repository.ResourceRepository;
import com.gw.repository.SettingRepository;
import com.gw.repository.StackRepository;
import com.gw.util.ApplicationComparator;
import com.gw.util.workflow.Workflow;
import com.gw.workflow.task.LaunchCF;
import com.gw.workflow.task.SendStackLaunchNotification;
import com.gw.workflow.task.TriggerJenkinsCodeDeployBuild;

@Component
public class LaunchStackWorkFlow extends Workflow {
	
	public LaunchStackWorkFlow initWorkFlow(Stack stack, AppMetaDataRepository appMetaDataRepository, StackRepository stackRepo, ApplicationRepository appRepo, ResourceRepository resourceRepo, SettingRepository settingRepo, User user) {
		addInput(user);
		Collections.sort(stack.getApplications(),new ApplicationComparator());
		LaunchCF launchCF = new LaunchCF();
		launchCF.init(this, stack, appRepo, resourceRepo, stackRepo, settingRepo);
		addTask(launchCF);
		for (Application app : stack.getApplications()) {
			TriggerJenkinsCodeDeployBuild codeDeployBuild = new TriggerJenkinsCodeDeployBuild();
			codeDeployBuild.init(this, app, appMetaDataRepository, appRepo, stack);
			addTask(codeDeployBuild);
		}
		SendStackLaunchNotification sendEmailTask = new SendStackLaunchNotification(stack, user, null);
		addTask(sendEmailTask);
		return this;
	}

}