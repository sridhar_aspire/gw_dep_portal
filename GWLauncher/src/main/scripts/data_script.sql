
insert into RESOURCE_COST_META_DATA values ("1", "EC2_INSTANCE", "m3.large", 0.12);
insert into RESOURCE_COST_META_DATA values ("2", "EC2_INSTANCE", "m4.large", 0.15);
insert into RESOURCE_COST_META_DATA values ("3", "EC2_INSTANCE", "m4.xlarge", 0.18);
insert into RESOURCE_COST_META_DATA values ("4", "LOAD_BALANCER", "regular", 0.22);

insert into APPLICATION_META_DATA values ("bc", "1004, 1002, 996");
insert into APPLICATION_META_DATA values ("ab", "1007, 1003, 1001, 999, 997, 995");
insert into APPLICATION_META_DATA values ("cc", "1005");
insert into APPLICATION_META_DATA values ("pc", "1006, 998");


insert into PERMISSION values (1, "USER_CREATE", "USER_CREATE");
insert into PERMISSION values (2, "USER_PWD_RESET", "USER_PWD_RESET");
insert into PERMISSION values (3, "STACK_CREATE", "STACK_CREATE");
insert into PERMISSION values (4, "MY_STACK_MODIFY", "MY_STACK_MODIFY");
insert into PERMISSION values (5, "MY_STACK_TERMINATE", "MY_STACK_TERMINATE");
insert into PERMISSION values (6, "ALL_STACK_MODIFY", "ALL_STACK_MODIFY");
insert into PERMISSION values (7, "ALL_STACK_TERMINATE", "ALL_STACK_TERMINATE");
insert into PERMISSION values (8, "STACK_REALLOCATION", "STACK_REALLOCATION");
insert into PERMISSION values (9, "COST_GROUP_BY_USER", "COST_GROUP_BY_USER");
insert into PERMISSION values (10, "COST_GROUP_BY_STACK", "COST_GROUP_BY_STACK");
insert into PERMISSION values (11, "ASSIGN_ROLE", "ASSIGN_ROLE");
insert into PERMISSION values (12, "ASSIGN_ADMIN_ROLE", "ASSIGN_ADMIN_ROLE");
insert into PERMISSION values (13, "ROLE_LISTING", "ROLE_LISTING");
insert into PERMISSION values (14, "USER_LISTING", "USER_LISTING");
insert into PERMISSION values (15, "METADATA_LISTING", "METADATA_LISTING");
insert into PERMISSION values (16, "MY_STACK_LISTING", "MY_STACK_LISTING");
insert into PERMISSION values (17, "ALL_STACK_LISTING", "ALL_STACK_LISTING");
insert into PERMISSION values (18, "COST_GROUP_BY_APP", "COST_GROUP_BY_APP");
insert into PERMISSION VALUES (19, "EDIT_SETTING", "EDIT_SETTING");
insert into PERMISSION VALUES (20, "MY_USER_LISTING", "MY_USER_LISTING");

insert into ROLE values(1, "USER", "A regular user who can manage his own stacks");
insert into ROLE values(2, "ADMIN", "An admin user who can manage his own stacks and stacks of other users");

insert into ROLE_HAS_PERMISSION values (1, 3);
insert into ROLE_HAS_PERMISSION values (1, 4);
insert into ROLE_HAS_PERMISSION values (1, 5);
insert into ROLE_HAS_PERMISSION values (1, 10);
insert into ROLE_HAS_PERMISSION values (1, 15);
insert into ROLE_HAS_PERMISSION values (1, 16);
insert into ROLE_HAS_PERMISSION values (2, 1);
insert into ROLE_HAS_PERMISSION values (2, 2);
insert into ROLE_HAS_PERMISSION values (2, 3);
insert into ROLE_HAS_PERMISSION values (2, 4);
insert into ROLE_HAS_PERMISSION values (2, 5);
insert into ROLE_HAS_PERMISSION values (2, 6);
insert into ROLE_HAS_PERMISSION values (2, 7);
insert into ROLE_HAS_PERMISSION values (2, 8);
insert into ROLE_HAS_PERMISSION values (2, 9);
insert into ROLE_HAS_PERMISSION values (2, 10);
insert into ROLE_HAS_PERMISSION values (2, 11);
insert into ROLE_HAS_PERMISSION values (2, 12);
insert into ROLE_HAS_PERMISSION values (2, 13);
insert into ROLE_HAS_PERMISSION values (2, 14);
insert into ROLE_HAS_PERMISSION values (2, 15);
insert into ROLE_HAS_PERMISSION values (2, 16);
insert into ROLE_HAS_PERMISSION values (2, 17);
insert into ROLE_HAS_PERMISSION values (2, 18);
insert into ROLE_HAS_PERMISSION values (2, 19);
insert into ROLE_HAS_PERMISSION values (1, 20);


insert into USER values (1, "user1", "$2a$10$Qxs9mHrlT3zZ43U4ZW8uMOHrMgSQao9BtCbLIl4SeK3ZCrmy0aZ12", "Sridhar", "Prabhakar", "sridhar.prabhakar@aspiresys.com", NOW(), NOW(), null, null, 1);
insert into USER values (2, "admin", "$2a$10$Qxs9mHrlT3zZ43U4ZW8uMOHrMgSQao9BtCbLIl4SeK3ZCrmy0aZ12", "admin", "admin", "sridhar.prabhakar@aspiresys.com", NOW(), NOW(), null, null, 2);

INSERT INTO SETTING values ("LIVE_STACK_LIMIT_PER_USER", "Live stack limit per user", "10", "SETTING");
INSERT INTO SETTING values ("AUTO_SHUTDOWN_WAIT_DAYS", "Auto shutdown wait time (Days)", "2", "SETTING");
INSERT INTO SETTING values ("AUTO_SHUTDOWN_ALERT_DAYS", "Auto shutdown alert time (Days)", "1", "SETTING");
INSERT INTO SETTING values ("AB|DEFAULT_INSTANCE_SIZE", "AB default instance size", "m4.large", "APP_SETTING");
INSERT INTO SETTING values ("BC|DEFAULT_INSTANCE_SIZE", "BC default instance size", "m4.xlarge", "APP_SETTING");
INSERT INTO SETTING values ("CC|DEFAULT_INSTANCE_SIZE", "CC default instance size", "m4.xlarge", "APP_SETTING");
INSERT INTO SETTING values ("PC|DEFAULT_INSTANCE_SIZE", "PC default instance size", "m4.xlarge", "APP_SETTING");